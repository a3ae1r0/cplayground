---
Title: C Programming - A Modern Approach, 2nd Edition
Type: Notes
Date: 2020-08-13
---

# Chapter 11 | Pointers

>The 11th commandment was "Thou Shalt Compute" or "Thou Shalt Not Compute" -I forget which.

## Table of Content
- [Chapter 11 | Pointers](#chapter-11-pointers)
  - [Table of Content](#table-of-content)
  - [11.1 | Pointer Variables](#111-pointer-variables)
    - [Declaring Pointer Variables](#declaring-pointer-variables)
  - [11.2 | The Address and Indirection Operators](#112-the-address-and-indirection-operators)
    - [The Address Operator](#the-address-operator)
    - [The Indirection Operator](#the-indirection-operator)
  - [11.3 | Pointer Assignment](#113-pointer-assignment)
  - [11.4 | Pointers as Arguments](#114-pointers-as-arguments)


## 11.1 | Pointer Variables
* In most modern computers, main memory is divided into bytes
  * Each byte is capable of storing eight bits of information
* **Each byte** has a <u>**unique address**</u> to distinguish it from other bytes in memory
* An executable program consists of both:
  * Code: machine instructions corresponding to statements in the original C program
  * Data: variables in the original program
* Each variable in the program occupies one or more bytes of memory
  * The address of the **first byte** is said to be the <u>**address of the variable**</u>
    * E.g. If variable `i` occupies the bytes at addresses 2000 and 2001, the `i` address is 2000
* We **store addresses in pointer variables**
  * So the address of a variable `i` is stored in the pointer variable `p`
    * We say that `p` **"points"** to `i`
* In resume:
  * **Pointer** is nothing more than an <u>**address**</u>
  * **Pointer variable** is just a variable the can <u>**store an address**</u>
* **Pointer notation**
  * p --> i (an arrow from p to i)
    * This indicate that a pointer variable `p` store the address of the variable `i`

### Declaring Pointer Variables
* Declared very similar to an ordinary variable
  * Only difference is the presence of an asterisk before the name of the pointer variable
    ```c
    int *p;
    ```
* This declaration states that `p` is a **pointer variable** capable of pointing to <u>**objects of the type int**</u>
    * Can point to an area of memory that doesn't belong to a variable. See information in [here](../ch17/README.md)
* Pointers can appear in declarations along with other variables
    ```c
    int i, j, a[10], b[20], *p, *q;
    ```
* **Referenced type**: every pointer variables can <u>**only point to objects of the same data type**</u>
    ```c
    int *p;          /* points only to integers   */
    double *q;       /* points only to doubles    */
    char *r;         /* points only to characters */
    ```
* A pointer variable can also **point to another pointer**


## 11.2 The Address and Indirection Operators
* Exclusive operators **only for pointers use**:
* To <u>**find the address of a variable**</u>, we sue the **`&` (address) operator**
  * If `x` is a variable, then `&x` is the address of `x` in memory
* To gain <u>**access to the object**</u> that a pointer points to, we use the **`*` (indirection) operator**
  * If `p` is a pointer, the `*p` represents the object to which `p` currently points

### The Address Operator
* **Declaring** a pointer variable <u>**sets aside space for the pointer**</u>, but doesn't make it point to an object
  ```c
  int *p;       /* points nowhere in particular */
  ```
* It's crucial to **initialize `p`** <u>**before use it**</u>
* To initialize a pointer variable, *assign it the address** of some variable -- more generally, lvalue -- using the <u>**`&` operator**</u>
  ```c
  int i, *p;
  ...
  p = &i;
  /* Or declare and initialize at the same time */
  int i;
  int *p = &i;
  /* Or combine the declaration of both variables */
  int i, *p = &i;
  ```

### The Indirection Operator
* Use the **`*` (indirection) operator** to access what's <u>**is stored in the object**</u>
* If `p` points to `i`, we can print the value of `i` as follows:
  ```c
  printf("$d\n", *p)
  ```
  * `printf` will display the value of `i`, not the address of `i`
* Relation between `*` and `&`
  * **`&` :** produces a pointer to a variable (address/value)
  * **`*` :** takes us back to the original variable
  ```c
  j = *&i;        /* same as j = i */
  ```
* As long as `p` points to `i`, <u>**`*p` is an alias for `i`**</u>
  * So changing the value of `*p` also changes the value of `i`
    * `*p` is an lvalue, so assignment to it is legal
  ```c
  int i = i, p = &i;
  printf("%d\n", i);         /* prints 1 */
  printf("%d\n", *p);        /* prints 1 */
  *p = 2
  printf("%d\n", i);         /* prints 2 */
  printf("%d\n", *p);        /* prints 2 */
  ```

**Be careful**:
* Never apply the indirection operator to an uninitialized pointer variable
  * Attempting to access the value of `p` will cause undefined behavior
  ```c
  int *p;
  printf("%d", *p);       /*** WRONG!! ***/
  ```
* Attempting to assign a value to `*p` is particularly dangerous
  * If `p` happens to contain a valid memory address, then he will attempt to modify the data stored in that address
  ```c
  int *p;
  *p = i;       /*** WRONG!! ***/
  ```
  * If the location address belongs to:
    * Program: the execution of this program may be act erratically
    * Operating System: the program will most likely crash


## 11.3 | Pointer Assignment
* **Assignment operators** allow the <u>**copy of pointers**</u>
* Example:
  ```c
  int i, j, *p, *q;           /* declaration of multiple variables and pointers */
  p = &i;                     /* pointer assignment: the address of i is copied into p */
  q = p;                      /* pointer assignment: copies the contents of p (address of i) into q */

  /* q point to the same place as p
  p --> ?(value) i
  q --> ?(value) i
  */

  /* changing *p or *q will change the value of i */
  *p = 1;                     /* i == 1 */
  *q = 2;                     /* i == 2 */
  ```
* **Any number** of pointers can point to the <u>**same object**</u>
* `q = p;` is not the same as `*q = *p`
  * The first is a pointer assignment; the second isn't
  * Example:
    ```c
    p = &i;
    q = &j;
    i = 1;

    /* Meaning 
    p --> 1(value) i
    q --> ?(value) j
    */

    *q = *p;

    /* Meaning: the assignment *q = *p copies the value that p points to (the value of i)
                into the object that q points to (the variable j)
    p --> 1(value) i
    q --> 1(value) j
    */
    ```


## 11.4 | Pointers as Arguments
* **Advantage :** allows the <u>**modification of a variable**</u> that is passed as a function argument
* **How it works:**
  * Instead of passing a variable `x` as the argument to a function, we'll supply `&x`, a pointer to `x`
  * Declare the corresponding parameter `p` to be a pointer
  * When the function is called, `p` will have the value `&x`, hence `*p` (the object that `p` points to) will be an alias for `x`
  * Each appearance of `*p` in the function's body will be an indirect reference to `x`
  * This allow the function both to read and modify `x`
* [Example](./sample_decompose_pointers.c) modifying the `decompose` function from [Chapter 9](../ch09/README.md).
* `scanf` function already makes use of pointers as arguments
  * Is also possible to pass a pointer variable to `scanf`:
    ```c
    int i, *p;
    ...
    p = &i;
    scanf("%d", p);
    ```

**Be careful:**
* Failing to pass a pointer to a function - when one is expected - can have disastrous results
* Example:
  ```c
  decompose(3.14159, i, d);
  ```
  * `decompose` has no way to tell the difference between `&i` and `&d`, and `i` and `d`
  * `decompose` will then trying to  change unknown memory locations (values of `i` and `d`)
* Prototyping a function, will allow the compiler let is know that the passed arguments have a wrong type
  * This don't happen with `scanf`, making this an error-prone function


### Using `const` to Protect Arguments
* When passing a pointer to a variable in a function call -- `f(&X)` -- the function may only need to examine the value o `x`, and not change it
* Advantage of **examine a variable** through a pointer:
  * Is <u>**faster**</u> to access
  * <u>**Don't waste unnecessary space**</u> required to hold the value
* To make sure we <u>**don't accidentally change a value**</u> and to document that intention, we can use the **keyword `cons`**:
  ```c
  void f(const int *p)      /* declare the p is pointer to a "constant integer" */
  {
      *p = 0;               /*** WRONG ***/
  }
  ```
  * Attempting to modify `*p` will result in an error detected by the compiler


## 11.5 | Pointers as Return Values
>**See ::** [Chapter 13](../ch13/README.md) for more functions regarding this topic
* Functions can also <u>**return pointers**</u>
* Example:
  ```c
  ...
  int *p, i, j;
  ...
  p = max(&i, &j);              /* after the call, p points either to i or j */
  ...
  /* return --> pointer , *a --> alias for i , *b --> alias for j */
  int *max(int *a, int *b)
  {
      if (*a > *b)                /* i > j        */
          return a;               /* address of i */
      else
          return b;               /* address of j */
  }
  ```
* A **function** could also <u>**return a pointer to an external variable**</u> or to a <u>**local variable (`static`)**</u>
* Pointers can also point to array elements
  * Useful for the function to return a pointer to one for the elements of the array
  ```c
  int *find_middle(int a[], int b)
  {
      return &a[n/2];
  }
  ```
>**See ::** [Chapter 12](../ch12/README.md) explores the relationship between pointers and arrays

**Be careful**
* Never return a pointer to an automatic local variable
  ```c
  int *f(void)
  {
      int i;
      ...
      return &i;    /* pointer invalid: i don't exist once f returns */
  }
  ```
* Some compilers issue a warning such `function returns address of a local variable`
