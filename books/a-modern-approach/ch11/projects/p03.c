/******************************************************************************
 * Modify Programming Project 3 from Chapter 6 so that includes the following
 * function:
 * void reduce (int numerator, int denominator, int *reduced_numerator,
 *             int *reduced_denominator);
 * numerator and denominator are the numerator and denominator of a fraction.
 * reduced_numerator and reduced_denominator are pointers to variables in
 * which the function will store the numerator and denominator of the fraction
 * once it has been reduced to the lowest terms.
 *****************************************************************************/
#include <stdio.h>

void reduce(int numerator, int denominator, int *reduced_numerator,
            int *reduced_denominator);

int main(void)
{
    int numerator, denominator, reduced_numerator, reduced_denominator;

    printf("Enter a fraction: ");
    scanf("%d/%d", &numerator, &denominator);

    reduce(numerator, denominator, &reduced_numerator, &reduced_denominator);
    printf("In lowest terms: %d/%d\n", reduced_numerator, reduced_denominator);

    return 0;
}

void reduce(int numerator, int denominator, int *reduced_numerator,
            int *reduced_denominator)
{
    /*
     * GCD Method:
     * 1st --> GCD numerator + denominator formula
     * 2nd --> divide numerator and denominator by GCD
     * Online calculator:
     * https://www.calculatorsoup.com/calculators/math/fractionssimplify.php
     */
    int a = numerator, b = denominator, r = 0;      /* GCD temp variables */

    while (b != 0) {
        r = a % b;
        a = b;
        b = r;
    }

    *reduced_numerator = numerator / a;
    *reduced_denominator = denominator / a;

}
