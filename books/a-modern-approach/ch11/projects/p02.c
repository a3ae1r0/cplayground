/******************************************************************************
 * Modify Programming Project 8 from Chapter 5 so that it includes the
 * following function:
 * void find_closest_flight(int desired time, int *departure_time,
 *                          int *arrival_time)
 *
 * This function will find the flight whose departure time is closest to
 * desired_time (expressed in minutes since midnight) in the variables pointed
 * to by departure_time and arrival_time, respectively
 *
 * Note: Code from scratch, without using - or seeing - old code from CH05.
 *****************************************************************************/
#include <stdio.h>

/*** PROTYPES ***/
void find_closest_flight(int desired_time, int *departure_time,
                         int *arrival_time);

/*** MACROS ***/
#define NUM_FLIGHTS 8

int main(void)
{
    int minutes, hours, desired_time, departure_time, arrival_time;

    /* Get and calculate desired_time */
    printf("Enter a 24-hour time (hh:mm): ");
    scanf("%d:%d", &hours, &minutes);
    desired_time = (hours * 60) + minutes;

    find_closest_flight(desired_time, &departure_time, &arrival_time);

    /* Departure */
    printf("Closest departure time is ");
    hours = departure_time/60;
    if (hours < 12)
        printf("%d:%.2d a.m.", hours, (departure_time - hours * 60));
    else
        printf("%d:%.2d p.m.", hours-12, (departure_time - hours * 60));
    /* Arrival */
    printf(", arriving at ");
    hours = arrival_time/60;
    if (hours < 12)
        printf("%d:%.2d a.m.\n", hours, (arrival_time - hours * 60));
    else
        printf("%d:%.2d p.m.\n", hours-12, (arrival_time - hours * 60));

    return 0;
}

void find_closest_flight(int desired_time, int *departure_time,
                         int *arrival_time)
{
    /* Departure and arrival times expressed in minutes since midnight */
    int i, departure_in_min[NUM_FLIGHTS] = {480, 583, 679, 767,
                                            840, 945, 1020, 1305},
             arrival_in_min[NUM_FLIGHTS] = {616, 112, 811, 900,
                                            968, 1075, 1280, 1438};

    *departure_time = departure_in_min[0];              /* Set default value */
    *arrival_time   = arrival_in_min[0];

    if (desired_time < departure_in_min[0])
        return;                 /* With that desired_time its the 1st flight */

    for (i=1; i < NUM_FLIGHTS; i++) {
        if ( (desired_time % departure_in_min[i]) < *departure_time ) {
            *departure_time = departure_in_min[i];
            *arrival_time   = arrival_in_min[i];
        }
    }
}
