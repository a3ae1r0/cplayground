/******************************************************************************
 * Modify Programming Project 7 from Chapter 2 so that it includes the
 * following function:
 * void pay_amount (int dollars, int *twenties, int *tens, int *fives,
 *                  int *ones);
 *
 * The function determines the smallest number of $20, $10, $5, and $1 bills
 * necessary to pay the amount represent by the dollars parameter. The twenties
 * parameter points t a variable in which the function store the number of $20
 * bills required. The tens, fives, an ones parameters are similar
 *
 * Note: Code from scratch, without using - or seeing - old code from CH02.
 *****************************************************************************/
#include <stdio.h>

void pay_amount(int dollars, int *twenties, int *tens, int *fives, int *ones);

int main(void)
{
    int dollars = 133, twenties, tens, fives, ones;

    printf("Insert amount of dollars: ");
    scanf("%d", &dollars);

    pay_amount(dollars, &twenties, &tens, &fives, &ones);
    printf("::Dollars::\n$%d\n::Bills::\n$20: %d  $10: %d  $5: %d  $1: %d\n",
            dollars, twenties, tens, fives, ones);
    return 0;
}

void pay_amount(int dollars, int *twenties, int *tens, int *fives, int *ones)
{
    *twenties = dollars / 20;
             /* |-- REMAINING $ AFTER $20--| */
    *tens     = (dollars - (*twenties * 20)) / 10;
    *fives    = (dollars - (*twenties * 20) - (*tens * 10)) / 5;
    *ones     = (dollars - (*twenties * 20) - (*tens * 10) - (*fives * 5)) / 1;
}
