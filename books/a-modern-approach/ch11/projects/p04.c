/*****************************************************************************
 * Modify the poker.c program from Section 10.5 by moving all external
 * variables into main and modifying functions so that they communicate by
 * passing arguments. The analyze_hands function needs to change the straight,
 * flush, four, three, and pairs variables, so it will have to passed pointers
 * to those variables.
 ****************************************************************************/
/* Headers */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

/* Macros */
#define NUM_RANKS 13
#define NUM_SUITS 4
#define NUM_CARDS 5

/* Prototypes */
void read_cards(int num_in_rank[], int num_in_suit[]);
void analyze_hand(int num_in_rank[], int num_in_suit[], int *pairs,
                  int *royal, bool *straight, bool *flush, bool *four,
                  bool *three);
void print_result(int *pairs, int *royal, bool *straight, bool *flush,
                  bool *four, bool *three);

/***********************************************************
 * main: Calls reads_cards, analyze_hand, and print_result *
 *       repeatedly. Pass pointers to variables in main.   *
 ***********************************************************/
int main(void)
{
    /* each card rank/suit value will take his corresponding place on array */
    int num_in_rank[NUM_RANKS], num_in_suit[NUM_SUITS], royal, pairs;
    bool straight, flush, four, three;

    for(;;) {
        read_cards(num_in_rank, num_in_suit);
        analyze_hand(num_in_rank, num_in_suit, &pairs, &royal, &straight,
                     &flush, &four, &three);
        print_result(&pairs, &royal, &straight, &flush, &four, &three);
    }

    return 0;
}

/***********************************************************
 * read_cards: Reads the cards into the pointers to the    *
 *             variables passed by main: num_in_rank and   *
 *             num_in_suit; Checks for bad cards and       *
 *             duplicated cards.                           *
 ***********************************************************/
void read_cards(int num_in_rank[], int num_in_suit[])
{
    int rank, suit, cards_read = 0;
    char ch, rank_ch, suit_ch;
    bool card_exists[NUM_RANKS][NUM_SUITS], bad_card;

    /* set default initial rank value and card_exists */
    for (rank = 0; rank < NUM_RANKS; rank++) {
        num_in_rank[rank] = 0;
        for (suit = 0; suit < NUM_SUITS; suit++) 
            card_exists[rank][suit] = false;
    }

    /* set default initial suit value */
    for (suit = 0; suit < NUM_SUITS; suit++)
        num_in_suit[suit] = 0;

    while (cards_read < NUM_CARDS) {
        bad_card = false;           /* reset bad_card status */

        printf("Enter a card: ");
        /*** get rank from user ***/
        rank_ch = getchar();
        switch (rank_ch) {
            case '0':           exit(EXIT_SUCCESS); /* terminate program */
            case '2':           rank = 0; break;
            case '3':           rank = 1; break;
            case '4':           rank = 2; break;
            case '5':           rank = 3; break;
            case '6':           rank = 4; break;
            case '7':           rank = 5; break;
            case '8':           rank = 6; break;
            case '9':           rank = 7; break;
            case 't': case 'T': rank = 8; break;    /* also accept upper case */
            case 'j': case 'J': rank = 9; break;
            case 'q': case 'Q': rank = 10; break;
            case 'k': case 'K': rank = 11; break;
            case 'a': case 'A': rank = 12; break;
            default:            bad_card = true;    /* found a bad card */
        }

        /*** get suit from user ***/
        suit_ch = getchar();
        switch (suit_ch) {
            case 'c': case 'C': suit = 0; break;
            case 'd': case 'D': suit = 1; break;
            case 'h': case 'H': suit = 2; break;
            case 's': case 'S': suit = 3; break;
            default:            bad_card = true;
        }

        /* escape new line char */
        while ((ch = getchar()) != '\n')
            if (ch != ' ') bad_card = true;

        if (bad_card)
            printf("Bad card; ignored.\n");
        else if (card_exists[rank][suit])
            printf("Duplicate card; ignored.\n");
        else {
            num_in_rank[rank]++;    /* increment corresponding array element */
            num_in_suit[suit]++;
            card_exists[rank][suit] = true;
            cards_read++;
        }
    }
}

/***********************************************************
 * analyze_cards: Determines whether the hand contains a   *
 *                straight, a flush, four-of-a-kind,       *
 *                and/or three-of-a-kind; determines the   *
 *                number of pairs; stores the results into *
 *                the pointers to the variables royal,     *
 *                straight, flush, four, three, and pairs. *
 ***********************************************************/
void analyze_hand(int num_in_rank[], int num_in_suit[], int *pairs,
                  int *royal, bool *straight, bool *flush, bool *four,
                  bool *three)
{
    int rank, suit, num_consec = 0; 
    *straight = false, *flush = false, *four = false,
    *three  = false;
    *royal  = 0, *pairs = 0;

    /*** check for flush : five cards of the same suit ***/
    for (suit = 0; suit < NUM_SUITS; suit++)
        if (num_in_suit[suit] == NUM_CARDS)
            *flush = true;

    /*** check for royal or straight : five cards with consecutive ranks ***/
    rank = 0;
    /* this loop (while) to skip non consecutives (for) */
    while (num_in_rank[rank] == 0) rank++;
    /* now starting counting consecutive cards */
    for (;rank < NUM_RANKS && num_in_rank[rank] > 0; rank++)
        num_consec++;
    if (num_consec == NUM_CARDS) {
        /* royal */
        for (rank = 8; rank < NUM_RANKS; rank++) {
            if (num_in_rank[rank] == 1)
                ++(*royal);
        }

        /* straight */
        *straight = true;
        return;         /* dunno why we need this... */
    }

    /*** check for 4-of-a-kind, 3-of-a-kind, and pairs ***/
    for (rank = 0; rank < NUM_RANKS; rank++) {
        if (num_in_rank[rank] == 4) *four = true;
        if (num_in_rank[rank] == 3) *three = true;
        /* Changed variable increment.
         * See https://stackoverflow.com/a/8208106 */
        if (num_in_rank[rank] == 2) ++(*pairs);
    }
}

/***********************************************************
 * print_result: Prints the classification of the hand,    *
 *               based on the values of the variables      *
 *               (pointers) straight, flush, four, three,  *
 *               and pairs.                                *
 ***********************************************************/
void print_result(int *pairs, int *royal, bool *straight, bool *flush,
                  bool *four, bool *three)
{
    if (*royal == 5 && *flush)      printf("Royal flush");
    else if (*straight && *flush)   printf("Straight flush");
    else if (*four)                 printf("Four of a kind");
    else if (*three && *pairs == 1) printf("Full house");
    else if (*flush)                printf("Flush");
    else if (*straight)             printf("Straight");
    else if (*three)                printf("Three of a kind");
    else if (*pairs == 2)           printf("Two pairs");
    else if (*pairs == 1)           printf("Pair");
    else                            printf("High card");
    printf("\n\n");
}
