/******************************************************************************
 * Write the following function:
 * void split_time(long total_sec, int *hr, int *min, int *sec);
 *
 * total_sec is a time represented as the number of seconds since midnigth. hr,
 * min, and sec are pointers to variables in which the function will stoare the
 * equivalent time in hours (0-23), minutes (0-59), and seconds (0-59),
 * respectively.
 *****************************************************************************/
#include <stdio.h>

void split_time(long total_sec, int *hr, int *min, int *sec);

int main(void)
{
    long total_sec;
    int hr, min, sec;

    printf("Enter number of seconds since midnight: ");
    scanf("%ld", &total_sec);

    split_time(total_sec, &hr, &min, &sec);
    printf("Equivalent time in:\nHours (0-23): %d \tMinutes (0-59): %d\t"
           "Seconds (0-59): %d \n", hr, min, sec);
    return 0;
}

void split_time(long total_sec, int *hr, int *min, int *sec)
{
    int sec_in_hour = 3600, sec_in_min = 60;
    *hr   = (total_sec - (total_sec % sec_in_hour)) / sec_in_hour;
    *min  = ((total_sec % sec_in_hour) -
            ((total_sec % sec_in_hour) % sec_in_min))/ sec_in_min;
    *sec  = total_sec - (*hr * sec_in_hour) - (*min * sec_in_min);
}
