/******************************************************************************
 * 1. If i is a variable and p points to i, which of the following are aliases
 * for i?
 *
 * (a) *p           (c) *&p         (e) *i          (g) *&i
 * (b) &p           (c) &*p         (f) &i          (h) &*i
 ******************************************************************************/
#include <stdio.h>

int main(void)
{
    int i = 3, *p = &i;

    printf("i : %d\n", i);              /* 3 */

    printf("(a) *p  : %d\n", *p);       /* 3 */
    printf("(b) &p  : %d\n", &p);       /* 2002040064 (random) -  mem address? */
    printf("(c) *&p : %d\n", *&p);      /*      "           "       (similar)  */
    printf("(d) &*p : %d\n", &*p);      /*      "           "       (similar)  */
    printf("(e) *i  : %d\n", *i);       /* error: invalid type argument of unary
                                                  ‘*’ (have ‘int’) */
    printf("(f) &i  : %d\n", &p);       /* similiar to values above            */
    printf("(g) *&i : %d\n", *&i);      /* 3 */
    printf("(h) &*i : %d\n", &*i);      /* error: invalid type argument of unary
                                              ‘*’ (have ‘int’) */
}

/*** ANSWER 
 * (a) *p
 * (g) *&i
 *
 * Code proves above.
 */
