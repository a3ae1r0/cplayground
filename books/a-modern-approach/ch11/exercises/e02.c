/******************************************************************************
 * 2. If i is an int variable and p and q are pointers to int, which of the
 * following assignments are legal?
 *
 * (a) p  = i;              (d) p = &q;               (g) p  = *p;
 * (b) *p = &i;             (e) p = *&q;              (h) *p = q;
 * (c) &p = q;              (f) p = q;                (i) *p = *p;
 *****************************************************************************/
int main(void)
{
    int i = 23, *p = &i, *q = &i;

    /* a */
    p = i;          /* warning: assignment to ‘int *’ from ‘int’ makes pointer
                      from integer without a cast [-Werror=int-conversion] */
    /* b */
    *p = &i;        /* warning: assignment to ‘int’ from ‘int *’ makes integer
                       from pointer without a cast [-Wint-conversion] */
    /* c */
    &p = q;         /* error: lvalue required as left operand of assignment */
    /* d */
    p = &q;         /* warning: assignment to ‘int *’ from incompatible
                       pointer type ‘int **’ [-Wincompatible-pointer-types] */
    /* e */
    p = *&p;        /* OK */
    /* f */
    p = q;          /* OK */
    /* g */
    *p = *q;        /* warning: assignment to ‘int *’ from ‘int’ makes pointer
                       from integer without a cast [-Wint-conversion] */
    /* h */
    *p = q;         /* warning: assignment to ‘int *’ from ‘int’ makes pointer
                       from integer without  a cast [-Wint-conversion]*/
    /* i */
    *p = *q;        /* OK */

    return 0;
}





/*** ANSWERS
 *
 * (e)
 * (f)
 * (i)
 *
 */
