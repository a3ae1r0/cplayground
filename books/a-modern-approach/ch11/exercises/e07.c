/******************************************************************************
 * Write the following function:
 * void split_date(int day_of_year, int year, int *month, int *day);
 *
 * day_of_year is an integer between 1 and 366, specifying a particular day
 * within the year designated by year. month, day point to variables in which
 * the function will store equivalent month (1-12) and day within that month
 * (1-31).
 *****************************************************************************/
#include <stdio.h>

void split_date(int day_of_year, int year, int *month, int *day);

int main(void)
{
    int day_of_year, year, month, day;

    /* User input */
    printf("Enter day_of_year (1-366): ");
    scanf("%d", &day_of_year);
    printf("Enter year: ");
    scanf("%d", &year);

    split_date(day_of_year, year, &month, &day);
    printf("\nMonth (1-12): %d | Day (1-31): %d\n", month, day);

    return 0;
}

void split_date(int day_of_year, int year, int *month, int *day)
{
    /* Non-leap year. For leap years, the additional day is added at the end */
    int days_in_month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int i  = 0;
    *month = 1;             /* Starting month */

    for (;;) {
        /* The day we are looking for (day_of_year), it's the current month */
        if (day_of_year < days_in_month[i]) {
            *day = day_of_year;
            break;
        /* Needs to move for the next month */
        } else
            /* Remove days of current month */
            day_of_year -= days_in_month[i];

        /* Inc month: both in the array of the days and also the desired var */
        i++;
        *month += 1;
    }

    /** Leap Year
     * Add +1 day (remove -1 in *day) to the year.
     */
    /* Find if year is a leap year */
    if ((year % 4 == 0 && year % 100 != 0)
        || ((year % 4 == 0 && year % 100 == 0) && year % 400 == 0)) {
        /* Check if day is equal 1. If it's change day (and month) for the last
         * day of previous month.
         * Remove -1 if is not. */
        if (*day == 1) {
            *day    = days_in_month[*month-1];
            *month -= 1;
        } else
            *day -= 1;
    }
}
