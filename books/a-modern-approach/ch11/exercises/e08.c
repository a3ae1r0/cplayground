/******************************************************************************
 * Write the following function:
 * int *find_largest(int a[], int n);
 *
 * When passed and array a of length n, the function will return a pointer to
 * the array's largest element.
 *****************************************************************************/
#include <stdio.h>

int *find_largest(int a[], int n);

int main(void)
{
    int a[100], n, i;

    printf("Insert array length: ");
    scanf("%d", &n);
    printf("Insert array elements (separated by spaces): ");
    for (i = 0; i < n; i++)
        scanf("%d", &a[i]);

    printf("The largest element of array is %d\n", *find_largest(a, n));

    return 0;
}

int *find_largest(int a[], int n)
{
    int i, largest_element = 0;     /* Set 1st array element as largest
                                       (default) */

    for (i = 0; i < n; i++) {
        if (a[i] > a[largest_element])
            largest_element = i;        /* Update index of largest element */
    }

    return &a[largest_element];
}
