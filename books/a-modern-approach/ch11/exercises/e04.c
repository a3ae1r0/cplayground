/******************************************************************************
 * Write the following function:
 * void swap(int *p, int *q);
 *
 * When passed the addresses of two variables should exchange the values of the
 * variables:
 * swap(&i, &j)         // exchanges values of i and j
 *****************************************************************************/
#include <stdio.h>

void swap(int *p, int *q);

int main(void)
{
    int i = 1, j = 3;

    printf("Before Swap\ni: %d  j: %d\n", i, j);
    swap(&i, &j);
    printf("After Swap\ni: %d  j: %d\n", i, j);

    return 0;

}

void swap(int *p, int *q)
{
    int temp;
    temp = *p;
    *p = *q;
    *q = temp;
}
