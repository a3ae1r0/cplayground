/******************************************************************************
 * 3. The following function supposedly computes the sum and average of the
 * numbers in the arary a, which has length n. avg and sum point to variables
 * that the function should modify. Unfortunately, the functions contains
 * several errors; find and correct them.
 *****************************************************************************/
#include <stdio.h>

void avg_sum(double a[], int n, double *avg, double *sum);

int main(void)
{
    double avg, sum, array[4] = {1, 2, 3, 4};

    avg_sum(array, 4, &avg, &sum);
    printf("avg =  %g  sum = %g\n", avg, sum);

    return 0;
}

/*** Fix the function below ***/
void avg_sum(double a[], int n, double *avg, double *sum)
{
    int i;

    /* FIX: The name of the variables needs to have "*" prior the name, since
     *      they're pointers
     */
    /* sum = 0.0; */            /* WROGRG */
    *sum = 0.0;
    for (i = 0; i < n; i++)
        /* sum += a[i]; */
        *sum += a[i];
    /* avg = sum / n; */
    *avg = *sum / n;
}
