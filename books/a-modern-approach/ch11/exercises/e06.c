/******************************************************************************
 * Write the following function:
 * void find_two_largest(int a[], int n, int *largest, int *second_largest);
 *
 * When passed an array a of length n, the function will search a for its
 * largest and second-largest elements, storing them in the variables pointed
 * to by largest and second_largest, respectively.
 *****************************************************************************/
#include <stdio.h>

void find_two_largest(int a[], int n, int *largest, int *second_largest);

int main(void)
{
    int i, n, a[100], largest, second_largest;

    /*** Create Array ***/
    printf("Enter the size of the array: ");
    scanf("%d", &n);

    printf("Enter array elements separated by spaces: ");
    for (i = 0; i < n; i++)
        scanf("%d", &a[i]);

    /*** Function Call ***/
    find_two_largest(a, n, &largest, &second_largest);
    printf("Largest: %d \nSecond Largest: %d\n", largest, second_largest);

    return 0;
}

void find_two_largest(int a[], int n, int *largest, int *second_largest)
{
    int element;

    /* Set default values */
    *largest        = a[0];
    *second_largest = a[0];

    for (element = 0; element < n; element++) {
        if (a[element] > *largest) {
            /* Check if last largest element, could be now second_largest
             * before make any change */
            if (*largest > *second_largest)
                /* Update to new second_largest value */
                *second_largest = *largest;

            *largest = a[element];
        }
        else if (a[element] > *second_largest)
            *second_largest = a[element];
    }
}
