/* Modification of the decompose function example in Chapter 9.3, now using
 * pointers.
 * Added the remaining program - main, headers, etc. - for a more clear example
 */
#include <stdio.h>

void decompose (double x, long *int_part, double *frac_part);
/* OR void decompose (double, long *, double *); */

int main (void)
{
    long i;
    double d;

    /* &i and &d: pointers to i and d - not the values of */
    decompose(3.14159, &i, &d);
    printf("decompose: 3.14159 \ni = %ld \nd = %lf\n", i, d);
    return 0;
}

/* When decompose is called:
 * - value 3.14159 is copied into x;
 * - a pointer to i is stored in int_part
 * - a pointer to d is stored in frac_part */
void decompose (double x, long *int_part, double *frac_part)
{
    *int_part  = (long) x;              /* puts the value 3 in i */
    *frac_part = x - *int_part;         /* 3.14159 - 3 and stores in d */
}
