/******************************************************************************
 * Find the Largest and Smallest Elements in an array -- my version
 *
 * Function:
 * void (max_min a[], int n, int *max, int *min);
 * max_min(b, N, &big, &smal);
 *****************************************************************************/
#include <stdio.h>

/*** MACROS ***/
#define ARR_SIZE 10

/*** PROTOTYPES ***/
void max_min(int a[], int n, int *max, int *min);

int main(void)
{
    int i = 0, arr[ARR_SIZE], big, small;

    /* Get array values */
    printf("Enter %d numbers: ", ARR_SIZE);
    for (i = 0; i < ARR_SIZE; i++)
        scanf("%d", &arr[i]);

    /* Function call to calculare largest and smallest elements */
    max_min(arr, ARR_SIZE, &big, &small);
    /* Print variables, now modified */
    printf("Largest: %d\nSmallest: %d\n", big, small);
    return 0;
}

void max_min(int a[], int n, int *max, int *min)
{
    int i;
    *max = *min = a[0];           /* Set default value (1st element) */

    /* Compare current *max and *min values with element value called by the
     * loop*/
    for(i = 0; i < n; i++) {
        *max = (a[i] > *max) ? a[i] : *max;
        *min = (a[i] < *min) ? a[i] : *min;
    }
}
