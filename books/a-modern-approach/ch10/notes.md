---
Title: C Programming - A Modern Approach, 2nd Edition
Type: Notes
Date: 2020-05-05
---

>As Will Rogers would have said, "There is no such thing as a free variable."

# Chapter 10 | Program Organization

## 10.1 | Local Variables
* A variable declared <u>in the body of a function</u> is said to be **local** to the function
* Local variables properties:
  * **Automatic storage duration**
    * "Automatically" allocated when the <u>enclosing function is called</u> and deallocated when the <u>function returns</u>
    * Doesn't retain its value when its enclosing function returns
  * **Block scope**
    * Portion of the program text in which the variable can be referenced
    * Not "visible" (value) to others functions
    * Since variable don't extend beyond function returns, can be reused for other purposes by other functions

> **C99:** Allow mixing variable declarations and code, creating this way a more small scope.

### Static Local Variables
* **Static storage duration**
  * Word `static` in the variable declaration
  * Variable retains its value throughout the execution of the program - **occupies same memory location**
  * Good to be used (call) by the same function the same during execution
  * Still has [block scope](#10.1-%7C-local-variables)

### Parameters
* Similarity to [local variables](#10.1-%7C-local-variables):
  * Same properties - **automatic storage duration** and **block scope**
* Difference to local variables:
  * Each parameter is **initialized automatically** when function is called - value of corresponding argument

## 10.2 | External Variables
* Variables that are declared **outside the body** of any function
* Also knows as **global variables**
* Properties:
  * **Static storage duration**
    * A value stored in this type of variable will stay there indefinitely
    * Works just like local variables declared with `static`
  * **File scope**
    * Visible from its point of declaration to the end of the enclosing file
    * Can be access - and modified - by **all functions** that follow its declaration

### Example: Using External Variables to Implement a Stack
* Abstract concept that can be implemented in most programming languages
* Can store multiple data items of the same type, just like an array
* Operations are limited:
  * **Push**
    * Add a item to one end of the stack, the stack top
  * **Pop**
    * Remove item from the same end of the stack
  * **Examining** or **modifying** an item that's not at the tp of the stack is <u>forbidden</u>
> See `stack_sample.c`

### Pros and Cons of External Variables
* Convenient when <u>many functions</u> **share a(few) variable(s)**
* **Disadvantages:**
  * When modifying a variable, we'll need to check **any affects in every function** that share this variable
  * If a incorrect value is assigned to an external variable, will be **more difficult to find** the guilty function
  * **Reduce portability**, since a function is not self-contained

## 10.3 | Blocks
* C allows compound statements to contain declarations as well, we call to this **statement block**
```c
if (i > j) {
    /* swap values of i and j */
    int temp = i;
    i = j;
    j = temp;
}
```
* Some of the block properties:
  * **Automatic storage duration**
    * Storage for the variable is <u>allocated when the block is entered</u> and <u>deallocated when the block is exited</u>.
    * A variable in a block can be declared with `static` to give it static storage duration
  * **Block scope**
    * Variable can't be referenced outside the block
* **Advantages**
  * Useful for temporary variables:
    1. It avoids <u>cluttering the declarations</u> at the beginning of the function body with variables that are used only briefly
    2. <u>It reduces name conflicts.</u> Variables are local to the block in which it's declared (can be reused for different purposes)

> **C99:** Allow variables to be declared anywhere within a block - same behaviour as variables within a function

## 10.4 | Scope
* The same idenfifier may have several different meanings (e.g. `i` for multiple loops)
* **Most important scope rule**
  * When a declaraton inside a block names a identifier that's already visible - because it has file scope or is declared in an enclosing block - <u>the declaration "hides" the old one</u>, and the identifier takes on a new meaning  
  At the end of the block, the <u>identifier regains its old meaning</u>  
  Example:
    ```c
    int i;                  /* Declaration #1 */

    void f(int i)
    {
        i = 1;              /* Declaration #2 */
    }

    void g(void)
    {
        int i = 2;          /* Declaration #3 */

        if (i > 0) {
            int i;          /* Declaration #4 */
            i = 3;
        }
        i = 4;
    }

    void h(void)
    {
        i = 5;
    }

    Explanation:
    - The i = 1 assingment refers to the paramenter in Declaration 2, not the variable in #1 -- #2 hides #1
    - The i > 0 test refers to the variable in #3 -- #3 hides #1, and #2 is out of scope
    - The i = 3 assingment refers to the variable in Declaration 4 --  #4 hides #3
    - The i = 3 assingment refers to the variable in Declaration 3 --  #4 is out of scope
    - The i = 3 assingment refers to the variable in Declaration 1
    ```
## 10.5 | Organizing a C Program
* (So far) a may program may contain:
  * Preprocessing directives suchs `#include` and `#define`
  * Type definitions
  * Declarations of external variables
  * Function prototypes
  * Function definitions
* Rules in the order imposed by C:
  * A **preprocessing directive** doesn't take effect until the line on which appears
  * A **type name** can't be used until it's been defined
  * A **variable** can't be used until it's declared
  > **C99:** (Author's recomendation): Every function be defined or declared prior to its first call. This is a requeriment in C99.
* Suggestion for organization:
  * `include` directives
  * `define` directives
  * Type definitions
  * Declaration of external variables
  * Prototypes for functions other than `main`
  * Definition of `main`
  * Definitions of other functions
  * Explanation:
    * `#include` directives brings in information that will likely be neeeded in several places within the programm
    * `#define` directives create macros, which are generally ysed throughout the program
    * **Type definitions** above the declaration of external variables is logical, since the declarations of these variables may refer to the types names defined
    * Declaration of **external variables** next makes them available to alal functions that follow
    * Declaring **all functions** except for `main` avoids problems when calling a function before the compiler has seen its prototype
    * Defining `main` before the others functions makes it easier for a reader to locate the program's starting point
    * Sugestion: Precede each function definition by a boxed commented where - explains its purposes, parameter meaning, return value, and side effects
