/* Remove th num_in_rank, num_in_suit, and card_exists arrays from the poker.c
 * program of Section 10.5.
 * Have the program store the cards in a 5x2 array. Each row of the array will
 * represent a card. For example, if the array is named hand, then hand[0][0]
 * will store the rank of the first card and hand[0][1] will store the suit of
 * the first card.
 */

/***********************************************************
 * NOTE:
 * This is program is incomplete... See p03_v2.c for the
 * author's program/answer
 ***********************************************************/

/***********************************************************
 * TO DO
 * - control check for duplicated cards
 *
 * OK
 * - save cards to hand (array)
 * - clean hand array, in order to allow multiple runs (KEEP THIS?)
 * - control check for bad cards
 ***********************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>

/* #define DIRECTIVES */
#define NUM_RANKS 13
#define NUM_SUITS 4
#define NUM_CARDS 5

/* EXTERNAL VARIABLES */
int num_in_rank[NUM_RANKS];
int num_in_suit[NUM_SUITS];

char hand[NUM_CARDS][2];
bool straight, flush, four, three;
int pairs;              /* can be 0, 1, or 2 */

/* PROTOTYPES */
void read_cards(void);
void analyze_hand(void);
void print_result(void);

/***********************************************************
 * main: Calls reads_cards, analyze_hand, and print_result *
 *       repeatedly.                                       *
 ***********************************************************/
int main(void)
{
    for(;;) {
        read_cards();
        analyze_hand();
        print_result();
    }
    return 0;
}

/***********************************************************
 * read_cards: Reads the cards into the external           *
 *             variables hand; Checks for bad cards and    *
 *             duplicated cards.                           *
 ***********************************************************/
void read_cards(void)
{
    char rank, suit;
    int cards_read = 0, i = 0, j = 0;
    bool duplicate = false;         /* replace cards_exists, dont remove it */

    /* Reset hand cards. We actual dont need this because the values will be
     * overwrite in the scanf call, but is better approach (I think)
     * Check later how this affect the program...
     *
     * EDIT: Comment out now, in order to allow debugging (print) hand results
     * after each run (w/o it it, it shows no values)
     */
    /* for (i = 0; i < NUM_CARDS; i++){ */
    /*     for (j = 0; j < 2; j++) */
    /*         hand[i][j]= ' '; */
    /* } */

    while (cards_read < NUM_CARDS) {
        printf("Enter a card: ");
        duplicate = false;              /* initial variable state */

        /* save cards (input) to array */
        scanf(" %c%c", &rank, &suit);
        hand[i][0] = (rank = tolower(rank));
        hand[i][1] = (suit = tolower(suit));

        /* check for duplicated card */
        for (j = 0; j < i; j++) {
            if ((hand[j][0] == rank) && (hand[j][1] == suit)) {
                duplicate = true;
            }
        }

        if ( ((rank < '2' && rank > '9' && rank != 't' && rank != 'j' &&
                rank != 'q' && rank != 'k' && rank != 'a') || (suit != 'c' &&
                suit != 'd' && suit != 'h' && suit != 's') ))
            printf("Bad card; ignored.\n");
        else if (duplicate)
            printf("Duplicate card; ignored.\n");
        else {
            cards_read++;
            i++;                        /* in order no write in a new card */
        }
    }
}

/***********************************************************
 * analyze_cards: Determines whether the hand contains a   *
 *                straight, a flush, four-of-a-kind,       *
 *                and/or three-of-a-kind; determines the   *
 *                number of pairs; stores the results into *
 *                the external variables straight, flush,  *
 *                four, three, and pairs.                  *
 ***********************************************************/
void analyze_hand(void)
{
    int num_consec = 0;
    int rank, suit, i, count = 0;
    straight = false;
    flush = false;
    four = false;
    three = false;
    pairs = 0;

    /*** check for flush : five cards of the same suit ***/
    for (i = 0; i < NUM_CARDS-1; i++) {
        if (hand[i][1] == hand[i+1][1])
            count++;
    }
    if (count == NUM_CARDS - 1)
        flush = true;

    /*** check for straight : five cards with consecutive ranks ***/
    rank = 0;
    /* this loop (while) to skip non consecutives (for) */
    while (num_in_rank[rank] == 0) rank++;
    /* now starting counting consecutive cards */
    for (;rank < NUM_RANKS && num_in_rank[rank] > 0; rank++)
        num_consec++;
    if (num_consec == NUM_CARDS) {
        straight = true;
        return;         /* dunno why we need this... */
    }

    /*** check for 4-of-a-kind, 3-of-a-kind, and pairs ***/
    for (rank = 0; rank < NUM_RANKS; rank++) {
        if (num_in_rank[rank] == 4) four = true;
        if (num_in_rank[rank] == 3) three = true;
        if (num_in_rank[rank] == 2) pairs++;
    }
}

/***********************************************************
 * print_result: Prints the classification of the hand,    *
 *               based on the values of the external       *
 *               variables straight, flush, four, three,   *
 *               and pairs.                                *
 ***********************************************************/
void print_result(void)
{
    if (straight && flush)        printf("Straight flush");
    else if (four)                printf("Four of a kind");
    else if (three && pairs == 1) printf("Full house");
    else if (flush)               printf("Flush");
    else if (straight)            printf("Straight");
    else if (three)               printf("Three of a kind");
    else if (pairs == 2)          printf("Two pairs");
    else if (pairs == 1)          printf("Pair");
    else                          printf("High card");

   /*******************************
   ** Debugging
   *******************************/
   int i, j;
   /* print hand array/hand cards */
   printf("\n\n***************\n");
   for (i = 0, j =1; i < NUM_CARDS; i++){
       printf("Card %d : %c%c\n", i+1, hand[i][0], hand[i][j]);
   }
   /*******************************/

    printf("\n\n");
}
