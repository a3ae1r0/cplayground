/**********************************************************
 * Complete program will help from:
 *
 **********************************************************/
#include <stdio.h>
#include <ctype.h>              /* isdigit() */

/**********************************************************
 * MACROS
 **********************************************************/
#define MAX_DIGITS 10
/* book says 4 digits, which doesn't make sense. 3 x 3 */
#define DIGIT_SIZE 3
/* space between each digit representation */
#define DIGIT_SPACING 1
/* add 1 space per digit, except the last one */
#define MAX_DIGITS_SPACE (MAX_DIGITS * (DIGIT_SIZE + DIGIT_SPACING) \
        - DIGIT_SPACING)
/* to convert char from user input to int */
#define CHAR_TO_INT 48
/* on and off bits */
#define MAX_SEGMENTS 7

/**********************************************************
 * EXTERNAL VARIABLES
 * _segments: store data represent correspondence between:
 *           digits AND segments (e06@ch08); each digit (10)
 *           as predefined segment (7) representation
 *                             0
 *                            ___
 *                         5 |   | 1
 *                           |_6_|
 *                         4 |   | 2
 *                           |___|
 *                             3
 * _digits: array of chars with: (i) 4 rows: segmenent digit
 *          height; (ii) 4 columns: three chars wide digits
 *          AND a space between digits
 **********************************************************/
                           /* |-- even numbers --|  |--- odd numbers ---| */
const int segments[10][7] = {{1, 1, 1, 1, 1, 1, 0}, {0, 1, 1, 0, 0, 0, 0},
                             {1, 1, 0, 1, 1, 0, 1}, {1, 1, 1, 1, 0, 0, 1},
                             {0, 1, 1, 0, 0, 1, 1}, {1, 0, 1, 1, 0, 1, 1},
                             {1, 0, 1, 1, 1, 1, 1}, {1, 1, 1, 0, 0, 0, 0},
                             {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 0, 1, 1}};
const int segment_grid_pos[MAX_SEGMENTS][2] = {
/*  row  col | Segment | */
    {0,   1},  /* 0 */
    {1,   2},  /* 1 */
    {2,   2},  /* 2 */
    {2,   1},  /* 3 */
    {2,   0},  /* 4 */
    {1,   0},  /* 5 */
    {1,   1}   /* 6 */
};
char digits[DIGIT_SIZE][MAX_DIGITS_SPACE];

/**********************************************************
 * PROTOTYPES
 **********************************************************/
void clear_digits_array(void);
void process_digit(int digit, int position);
void print_digits_array(void);

int main(void)
{
    char ch;
    /* position of the representation of each digit */
    int position = 0;

    clear_digits_array();
    printf("Enter a number (up to 10 digits): ");
    /* save into the array only the max amount of chars allowed */
    while (((ch = getchar()) != '\n') && (position < MAX_DIGITS_SPACE))  {
        if (isdigit(ch)) {
            process_digit(ch - '0', position);
            position += DIGIT_SIZE + DIGIT_SPACING;
        }
    }

    print_digits_array();
    return 0;
}

/**********************************************************
 * clear_digits_array: store blank characters into all
 *                     elements of the the digits array
 **********************************************************/
void clear_digits_array(void)
{
    int row, column;
    for (row = 0; row < DIGIT_SIZE; row++) {
        for (column = 0; column < MAX_DIGITS_SPACE; column++)
            digits[row][column] = ' ';
    }
}

/**********************************************************
 * process_digits: store the seven-segment representation
 *                 of digit into a specified position in
 *                 the digits array (positions range from 0
 *                 to MAX_DIGITS - 1)
 **********************************************************/
void process_digit(int digit, int position)
{
    int digit_segment, segment_row_pos, segment_column_pos;
    /* loop through all 7 segments, aka, on/off bits */
    for (digit_segment = 0; digit_segment < MAX_SEGMENTS; digit_segment++) {
        if(segments[digit][digit_segment]){
            segment_row_pos    = segment_grid_pos[digit_segment][0];
            segment_column_pos = segment_grid_pos[digit_segment][1] +
                position;
            digits[segment_row_pos][segment_column_pos] = digit_segment % 3 ==
                0 ? '_' : '|';
        }
    }
}

/**********************************************************
 * print_digits_array: display the rows of the digits array,
 *                     each on a single line, producing the
 *                     output shown in the head of this file
 **********************************************************/
void print_digits_array(void)
{
    int row, column;
    for (row = 0; row < DIGIT_SIZE; row++) {
        for (column = 0; column < MAX_DIGITS_SPACE; column++)
            printf("%c", digits[row][column]);
        printf("\n");
    }
}
