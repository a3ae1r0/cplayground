/**********************************************************
 * Display number entered by user, using the effect of a
 * seven-segment display
 *  _          __
 *  _| |_| |_| _|_
 * |_    |   |  |
 **********************************************************/
#include <stdio.h>

/**********************************************************
 * MACROS
 **********************************************************/
#define MAX_DIGITS 10
#define CHAR_TO_INT 48      /* to convert char from user input to int */

/**********************************************************
 * EXTERNAL VARIABLES
 * _segments: store data represent correspondence between:
 *           digits AND segments (e06@ch08); each digit (10)
 *           as predefined segment (7) representation
 * _digits: array of chars with: (i) 4 rows: segmenent digit
 *          height; (ii) 4 columns: three chars wide digits
 *          AND a space between digits
 **********************************************************/
                           /* |------ even ------|  |------- odd -------| */
const int segments[10][7] = {{1, 1, 1, 1, 1, 1, 0}, {0, 1, 1, 0, 0, 0, 0},
                             {1, 1, 0, 1, 1, 0, 1}, {1, 1, 1, 1, 0, 0, 1},
                             {0, 1, 1, 0, 0, 1, 1}, {1, 0, 1, 1, 0, 1, 1},
                             {1, 0, 1, 1, 1, 1, 1}, {1, 1, 1, 0, 0, 0, 0},
                             {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 0, 1, 1}};
char digits[4][MAX_DIGITS * 4];

/**********************************************************
 * PROTOTYPES
 **********************************************************/
void clear_digits_array(void);
void process_digit(int digit, int position);
void print_digits_array(void);

int main(void)
{
    char ch, digit[MAX_DIGITS] = { };
    int n_digit = 0, i;

    printf("Enter a number: ");
    while (((ch = getchar()) != '\n'))  {
        /* save into the array only the max amount of chars allowed */
        if (n_digit < MAX_DIGITS) {
            digit[n_digit] = ch;
            n_digit++;
        } else
            break;
    }

    /****** TESTING ******/
    /* loop through all elements in digit array; pass each one as function arg */
    for (i = 0; i < n_digit; i++)
        process_digit(digit[i] % CHAR_TO_INT, i);
    /**** TESTING END ****/

    return 0;
}

/**********************************************************
 * clear_digits_array: store blank characters into all
 *                     elements of the the digits array
 **********************************************************/
void clear_digits_array(void)
{
    int height, width;

    for (height = 0; height < 4; height++) {
        for (width = 0; width < (MAX_DIGITS * 4); width++)
            digits[height][width] = ' ';
    }
}

/**********************************************************
 * process_digits: store the seven-segment representation
 *                 of digit into a specified position in
 *                 the digits array (positions range from 0
 *                 to MAX_DIGITS - 1)
 **********************************************************/
void process_digit(int digit, int position)
{
    int column;
    int height, width;

    /* loop for save into the digits array */
    for (height = 0; height < 4; height++)
        for (width = 0; width < (MAX_DIGITS * 4); width++) {
            /* remove corners ? */
            /* if () */
    }

    printf("\n");
}

/**********************************************************
 * print_digits_array: display the rows of the digits array,
 *                     each on a single line, producing the
 *                     output shown in the head of this file
 **********************************************************/
void print_digits_array(void)
{
    int row, column;
    int on_off;

    /* numerical representation of digit using "on" or "off" (segments array) */
    /* for (column = 0; column < 7; column++) */
    /*     printf(" %d", segments[digit][column]); */
    /* printf("\n"); */

    /* testing:
     * (i) get a digit;
     * (ii) check segments array of on and off bits;
     * (iii) convert on and off bits to linues
     * (iv) print lines
     * move something similar later to print_digits_array? */
    for (column = 0; column < 7; column++) {
        /* on_off = segments[digit][column]; */
        if (on_off) {
            printf(" ");
            switch (column) {
                /* horizontal line */
                case 0:
                case 6:
                case 3: printf("_");
                        break;
                /* vertical line */
                default: printf("|");
                         break;
            }
        }
    }

    for (row = 0; row < 4; row++) {
        for (column = 0; column < 4; column++)
            printf("%c", digits[row][column]);
        printf("\n");
    }
}
