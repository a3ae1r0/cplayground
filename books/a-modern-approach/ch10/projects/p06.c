/**********************************************************
 * A program that evaluates RPN (Reverse Polish Notation)
 * expressions
 **********************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>             /* stack overflow and underflow */

/** CONSTANTS **/
#define STACK_SIZE 100
/* '0' (char) == 48 (decimal). Calculate remainder in order to get each int */
#define CHAR_TO_INT 48

/** EXTERNAL VARIABLES **/
int contents[STACK_SIZE];
int top = 0;

/** PROTOTYPES **/
void make_empty(void);
bool is_empty(void);
bool is_full(void);
void push(int i);
int pop(void);
void stack_overflow(void);
void stack_underflow(void);

int main (void)
{
    char ch;
    int first = 0, second = 0;

    printf("Enter an RPN expression: ");
    for (;;) {
        scanf(" %c", &ch);

        switch (ch) {
            /* single-digit integers. Before pushing it, convert char to int */
            case '0': case '1':
            case '2': case '3':
            case '4': case '5':
            case '6': case '7':
            case '8': case '9': push (ch % CHAR_TO_INT);
                                break;
            /* pop operands; perform operation; push the result to the stack */
            case '+': push(pop() + pop());
                      break;
            case '-': second = pop();       /* invert operands popped order */
                      first  = pop();
                      push(first - second);
                      break;
            case '*': push(pop() * pop());
                      break;
            case '/': second = pop();       /* invert operands popped order */
                      first  = pop();
                      push(first / second);
                      break;
            /* display top stack item (last array element) */
            case '=':
                      printf("Value of the expression: %d\n", contents[top-1]);
                      /* run again: clear stack + prompt user again */
                      make_empty();
                      printf("Enter an RPN expression: ");
                      break;
            /* end program if input is not an operator or operand */
            default: return 0;
        }
    }

    return 0;
}

void make_empty(void)
{
    top = 0;
}

bool is_empty(void)
{
    return top == 0;
}

bool is_full(void)
{
    return top == STACK_SIZE;
}

void push(int i)
{
    if (is_full())
        stack_overflow();
    else
        contents[top++] = i;
}

int pop(void)
{
    if (is_empty())
        stack_underflow();
    else
        return contents[--top];
}

/**********************************************************
 * stack_overflow: since the stack full, this function will
 *                 return a message to the user, and
 *                 terminate the program with error.
 **********************************************************/
void stack_overflow(void)
{
    printf("Expression is too complex.\n");
    exit(EXIT_FAILURE);
}

/**********************************************************
 * stack_underflow: since the stack empty, this function
 *                  will return a informative message to
 *                  the user user, and terminate with error.
 **********************************************************/
void stack_underflow(void)
{
    printf("Not enough operands in expression.\n");
    exit(EXIT_FAILURE);
}
