/* Check if parentheses/braces are nested properly (match) */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>         /* needed for exit() function */

#define STACK_SIZE 100

/* EXTERNAL VARIABLES */
char contents[STACK_SIZE];
int top = 0;

/* PROTOTYPES */
void make_empty(void);
bool is_empty(void);
bool is_full(void);
void push(char i);
char pop(void);
void stack_overflow(void);
void stack_underflow(void);

int main(void)
{
    char ch;
    bool match;

    make_empty();           /* reset stack */

    printf("Enter parentheses and/or braces: ");
    while ((ch = getchar()) != '\n') {
        switch(ch) {
            case '(':
            case '{': push(ch);
                      break;
            case ')': if (pop() != '(')
                          match = false;
                      break;
            case '}': if (pop() != '{')
                          match = false;
                      break;
        }
    }

    /* check if array is empty AND not marked as false (not match) */
    if (is_empty() && match == false)
        printf("Parentheses/braces are nested properly\n");
    else
        printf("Parentheses/braces aren't nested properly\n");

    return 0;
}

void make_empty(void)
{
    top = 0;
}

bool is_empty(void)
{
    return top == 0;
}

bool is_full(void)
{
    return top == STACK_SIZE;
}

void push(char i)
{
    if (is_full())
        stack_overflow();
    else
        contents[top++] = i;
}

char pop(void)
{
    if (is_empty())
        stack_underflow();

    /* Comment out else below because of GCC warning when compiling with 
     * C89/C90 compability:
     * error: control reaches end of non-void function [-Werror=return-type]]
     *
     * GCC cannot figure the when the function reach the end.
     *
     * Comment out this line, dont affect the outcome of the program.
     */
    /* else */

    return contents[--top];
}

/* handles overflow call */
void stack_overflow(void)
{
    printf("Stack overflow\n");
    exit(EXIT_FAILURE);
}

/* handles underflow call */
void stack_underflow(void)
{
    printf("Parentheses/braces aren't nested properly\n");
    exit(EXIT_FAILURE);     /* exit immediately, w/o returning to main */
}
