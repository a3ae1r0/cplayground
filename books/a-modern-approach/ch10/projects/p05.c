/**********************************************************
 * Modify the poker.c program of Section 10.5 bu allowing *
 * "ace-low" straights (ace, two, three, four, five)      *
 **********************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

/* #define directives (constants) */
#define NUM_RANKS 13
#define NUM_SUITS 4
#define NUM_CARDS 5

/* global/external variables */
int num_in_rank[NUM_RANKS];
int num_in_suit[NUM_SUITS];
bool straight, flush, four, three, ace_low;
int pairs;              /* can be 0, 1, or 2 */

/* prototypes */
void read_cards(void);
void analyze_hand(void);
void print_result(void);

/***********************************************************
 * main: Calls reads_cards, analyze_hand, and print_result *
 *       repeatedly.                                       *
 ***********************************************************/
int main(void)
{
    for(;;) {
        read_cards();
        analyze_hand();
        print_result();
    }
    return 0;
}

/***********************************************************
 * read_cards: Reads the cards into the external           *
 *             variables num_in_rank and num_in_suit;      *
 *             checks for bad cards and duplicated cards.  *
 ***********************************************************/
void read_cards(void)
{
    bool card_exists[NUM_RANKS][NUM_SUITS];
    char ch, rank_ch, suit_ch;
    int rank, suit;
    bool bad_card;
    int cards_read = 0;

    /* set default initial rank value and card_exists */
    for (rank = 0; rank < NUM_RANKS; rank++) {
        num_in_rank[rank] = 0;
        for (suit = 0; suit < NUM_SUITS; suit++) 
            card_exists[rank][suit] = false;
    }

    /* set default initial suit value */
    for (suit = 0; suit < NUM_SUITS; suit++)
        num_in_suit[suit] = 0;

    while (cards_read < NUM_CARDS) {
        bad_card = false;           /* reset bad_card status */

        printf("Enter a card: ");

        /*** get rank from user ***/
        rank_ch = getchar();
        switch (rank_ch) {
            case '0':           exit(EXIT_SUCCESS); /* terminate program */
            case '2':           rank = 0; break;
            case '3':           rank = 1; break;
            case '4':           rank = 2; break;
            case '5':           rank = 3; break;
            case '6':           rank = 4; break;
            case '7':           rank = 5; break;
            case '8':           rank = 6; break;
            case '9':           rank = 7; break;
            case 't': case 'T': rank = 8; break;    /* also accept upper case */
            case 'j': case 'J': rank = 9; break;
            case 'q': case 'Q': rank = 10; break;
            case 'k': case 'K': rank = 11; break;
            case 'a': case 'A': rank = 12; break;
            default:            bad_card = true;    /* found a bad card */
        }

        /*** get suit from user ***/
        suit_ch = getchar();
        switch (suit_ch) {
            case 'c': case 'C': suit = 0; break;
            case 'd': case 'D': suit = 1; break;
            case 'h': case 'H': suit = 2; break;
            case 's': case 'S': suit = 3; break;
            default:            bad_card = true;
        }

        /* escape new line char */
        while ((ch = getchar()) != '\n')
            if (ch != ' ') bad_card = true;

        if (bad_card)
            printf("Bad card; ignored.\n");
        else if (card_exists[rank][suit])
            printf("Duplicate card; ignored.\n");
        else {
            num_in_rank[rank]++;    /* increment corresponding array element */
            num_in_suit[suit]++;
            card_exists[rank][suit] = true;
            cards_read++;
        }
    }
}

/***********************************************************
 * analyze_cards: Determines whether the hand contains a   *
 *                straight, a flush, four-of-a-kind,       *
 *                and/or three-of-a-kind; determines the   *
 *                number of pairs; stores the results into *
 *                the external variables straight, flush,  *
 *                four, three, pairs, and ace_low.         *
 ***********************************************************/
void analyze_hand(void)
{
    int num_consec = 0;
    int rank, suit;

    straight = false;
    flush    = false;
    four     = false;
    three    = false;
    ace_low  = true;            /* 'true' to allow find any exception next */
    pairs    = 0;

    /*** check for flush : five cards of the same suit ***/
    for (suit = 0; suit < NUM_SUITS; suit++)
        if (num_in_suit[suit] == NUM_CARDS)
            flush = true;

    /*** check for straight : five cards with consecutive ranks ***/
    rank = 0;
    /* this loop (while) to skip non consecutives (for) */
    while (num_in_rank[rank] == 0) rank++;
    /* now starting counting consecutive cards */
    for (;rank < NUM_RANKS && num_in_rank[rank] > 0; rank++)
        num_consec++;
    if (num_consec == NUM_CARDS) {
        straight = true;
        return;         /* dunno why we need this... */
    }

    /*** check for ace-low straights: ace, two, three, four, five ***/
    for (rank = 0; rank < 4; rank++)
        /* catch any exception for when card is not allowed in ace-low */
        if (num_in_rank[rank] != 1 || num_in_rank[12] != 1)
            ace_low = false;

    /*** check for 4-of-a-kind, 3-of-a-kind, and pairs ***/
    for (rank = 0; rank < NUM_RANKS; rank++) {
        if (num_in_rank[rank] == 4) four = true;
        if (num_in_rank[rank] == 3) three = true;
        if (num_in_rank[rank] == 2) pairs++;
    }
}
/***********************************************************
 * print_result: Prints the classification of the hand,    *
 *               based on the values of the external       *
 *               variables straight, flush, four, three,   *
 *               pairs, and ace-low                        *
 ***********************************************************/
void print_result(void)
{
    if (straight && flush)        printf("Straight flush");
    else if (four)                printf("Four of a kind");
    else if (three && pairs == 1) printf("Full house");
    else if (flush)               printf("Flush");
    else if (straight || ace_low) printf("Straight");
    else if (three)               printf("Three of a kind");
    else if (pairs == 2)          printf("Two pairs");
    else if (pairs == 1)          printf("Pair");
    else if (ace_low)             printf("Ace-low");
    else                          printf("High card");

    printf("\n\n");
}
