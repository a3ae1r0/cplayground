/* For each of following scopes, list all variables and parameter names visible in
 * that scope: 
 *
 * (a) The f function
 * (b) The g function
 * (c) The block in which e is declared
 * (d) The main function
 *
 * (a)
 * Variables:
 * - a, b, c;
 * Parameters:
 * - b;
 *
 * (b)
 * Variables:
 * - a, d, e; 
 * Parameters:
 * - none (void);
 *
 * (c)
 * Variables:
 * - a, e;
 * Parameters:
 * - none (void);
 *
 * (d)
 * Variables:
 * - a, f; 
 * Parameters:
 * - none (void);
 */

int a;

void f(int b)
{
    int c;
}

void g(void)
{
    int d;
    {
        int e;
    }
}

int main(void)
{
    int f;
}

/* Author's answer (http://knking.com/books/c2/answers/c10.html):
 * (b) a, and d are visible.
 * (c) a, d, and e are visible.
 */
