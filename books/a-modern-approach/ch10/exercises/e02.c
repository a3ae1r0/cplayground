/* For each of following scopes, list all variables and parameter names visible in
 * that scope: 
 *
 * (a) The f function
 * (b) The g function
 * (c) The block in which a and d are declared
 * (d) The main function
 *
 * (a)
 * Variables:
 * - b (declared in line 38), d, c;
 * Parameters:
 * - none;
 *
 * (b)
 * Variables:
 * - c (declared in line 42), b;
 * Parameters:
 * - a;
 *
 * (c)
 * Variables:
 * - a (variable at line 44), d, b, c;
 * Parameters:
 * - (a paremeter overwrited by variable inside the block l44)
 *
 * (d)
 * Variables:
 * - b, c (declared in line 51), d;
 * Parameters:
 * -
 */

int b, c;

void f(void)
{
    int b, d;
}
void g(int a)
{
    int c;
    {
        int a, d;
    }

}

int main (void)
{
    int c, d;
}
