/* Suppose that a program has only one function (main). How many different
 * variables name i could this program contain?
 *
 * Answer: Inside (multiple) blocks could be infinite variables.
 * Outside these blocks only one could be declared in main.
 */

#include<stdio.h>
int main(void)
{
    int i = 1;
    printf("i is %d.\n", i);
    {
        int i = 2;
        printf("i is %d.\n", i);
    }
    {
        int i = 3;
        printf("i is %d.\n", i);
    }
    {
        int i = 99999;
        printf("i is %d.\n", i);
    }
    return 0;
}

/* (after note) With C89/C90, one external variable one internal variable inside
 * main, would collide, since the declarations of variables must be in the
 * beginning of function.
 * But with C99, we could also have this external variable (with declaration of
 * a new variable i in the middle of the code). So this way, we would have: 1
 * ext. var, 1 internal var, multiple (no limit) of blocks.
 */
