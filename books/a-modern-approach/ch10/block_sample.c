#include <stdio.h>
int main (void)
{
    int a = 2, b = 5;

    printf("Block below\n");
    if (a < b) {
        int c = 3;
        printf("Now in the block!\n");
        printf("a + b + c = %d\n", a + b + c);
    }

    /
    /* dont work with c89/c90, because is "mixing declarations and code". The
     * declarations of "c" needs to be inside the block */
    /*
    int c = 3;
    if (a < b) {
        printf("Now in the block!\n");
        printf("a + b + c = %d\n", a + b + c);
    }
    */

    /* only work on C99 */
    /*
    if (a < b) {
        int c = 3;
        printf("Now in the block!\n");
        printf("a + b + c = %d\n", a + b + c);
    }
    */
    return 0;
}
