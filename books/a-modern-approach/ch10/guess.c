/* Asks user to guess a hiden number (my version) **/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define MAX_NUMBER 100

int generate_number(void);
void get_secret_number(void);
void user_interaction(void);

/* global vars to allow visibility across all functions */
int secret_number = 0, attempts = 1;

int main(void)
{
    printf("Guess the secret number between 1 and %d.\n", MAX_NUMBER);
    get_secret_number();
    return 0;
}

int generate_number(void)
{
    /* generate a random number between 1 - 100 */
    srand( (unsigned)time( NULL ) ) ;
    return ((rand() % 100) + 1);
}

void get_secret_number(void)
{
    char again;

    /* call function to generate random number. Global var #0 */
    secret_number = generate_number();
    printf("\nA new number has been chosen.\n");
    /* call function for user interaction */
    user_interaction();

    /* try again menu */
    printf("Play again? (Y/N) ");
    scanf(" %c", &again);       /* no need for use a global var */
    if (again == 'Y') {
        attempts = 1;           /* reset number of attempts */
        get_secret_number();    /* recursive. do it all over again. */
    }
}

void user_interaction(void)
{
    int guess = 0;

    printf("Enter guess: ");
    scanf("%d", &guess);

    if (guess > secret_number) {
        printf("Too high; try again.\n");
        attempts++;
        user_interaction();     /* recursive call until the values are the same */
    } else if (guess < secret_number) {
        printf("Too low; try again.\n");
        attempts++;
        user_interaction();
    } else {
        /* guess is correct. End play. */
        printf("You won in %d guesses!\n\n", attempts);
    }
}
