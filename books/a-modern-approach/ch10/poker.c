/* Classifies a poker hand (my version)
 *
 * Note: This is failed attempt. The program is incomplete.
 * The final program - from the chapter - will be poker_v2.c
 */
#include <stdio.h>

/* #define directives (constants) */
#define RANKS_MAX 13
#define SUITS_MAX 4

/* global/external variables */
char hand_cards[20] = {' '};        /* each card (rank & suit) will occupy two
                                       elements*/
/* char suits_hand[SUITS_MAX] = {' '}; */
/* char card0[RANKS_MAX][SUITS_MAX] = {' '};     /1* each card is one variable ?? *1/ */
/* char card1[RANKS_MAX][SUITS_MAX] = {' '}; */
/* char card2[RANKS_MAX][SUITS_MAX] = {' '}; */
/* char card3[RANKS_MAX][SUITS_MAX] = {' '}; */
/* char card4[RANKS_MAX][SUITS_MAX] = {' '}; */

/* prototypes */
void read_cards(void);
void analyze_hand(void);
void print_result(void);

/************************************************
 * main: calls read_cards, analyze_hand, and    *
 *       print_result repeatedly.               *
 ************************************************/
int main(void)
{
    for(;;) {
        read_cards();
        analyze_hand();
        print_result();
    }
    return 0;
}

/************************************************
 * read_cards: read hand cards from user input, *
 *             to external variables.           *
 *             Check for duplicate and bad      *
 *             cards.                           *
 ************************************************/
void read_cards(void)
{
    int i;
    char rank, suit;

    for (i = 0; i < 10; i++, i+=2) {
        printf("Enter a card: ");
        scanf(" %c%c", &hand_cards[i], &hand_cards[i+1]);
    }

        printf("Cards: ");
    for (i = 0; i < 10; i+=2) {
        printf("%c%c ", hand_cards[i], hand_cards[i+1]);
    }
        printf("\n");
}

/************************************************
 * analyze_hand: check and count cards from     *
 *               ext variables writted by       *
 *               read_cards.                    *
 *               store results in other ext vars*
 *               for benefit of print_result.   *
 ************************************************/
void analyze_hand(void)
{

}

/************************************************
 * print_results: print cards results, from     *
 *                values in ext vars from       *
 *                analyze_hand.                 *
 ************************************************/
void print_result(void)
{

}
