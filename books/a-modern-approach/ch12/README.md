---
Title: C Programming - A Modern Approach, 2nd Edition
Type: Notes
Date: 2020-11-18
---

# Chapter 12 | Pointers and Arrays

>Optimization hinders evolution.

## Table of Content
- [Chapter 12 | Pointers and Arrays](#chapter-12-pointers-and-arrays)
  - [Table of Content](#table-of-content)
  - [12.1 | Pointer Arithmetic](#121-pointer-arithmetic)
    - [Adding an Integer to a Pointer](#adding-an-integer-to-a-pointer)
    - [Subtracting an integer from a pointer](#subtracting-an-integer-from-a-pointer)
    - [Subtracting one pointer from another](#subtracting-one-pointer-from-another)
    - [Comparing Pointers](#comparing-pointers)


## 12.1 | Pointer Arithmetic
* As we saw in the Section 11.5, a pointer can point to array elements
  ```c
  int a[10], *p;
  p = &a[0];          /* p points to a[0] */
  *p = 5;             /* Access a[0] (*p) and the store the value 5 */
  ```
* Also know as address arithmetic
* Performing pointer arithmetic allows us to access others elements of an array
* C supports three - and only three - forms of pointer arithmetic:
  * [Adding an integer to a pointer](#adding-and-integer-to-a-pointer)
  * [Subtracting an integer from a pointer](#subtracting-an-integer-from-a-pointer)
  * [Subtracting one pointer from another](#subtracting-one-pointer-from-another)

### Adding and Integer to a Pointer
* Adding an integer `j` to a pointer `p` means a pointer to the element `j` places after the one the `p` currently points to
  * if `p` points to the array element a[i], then `p + j` points to `a[i+j]`
    ```c
    int a[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, *p, *q;

    p = &a[2];      /* p = 2 */
    q = p + 3;      /* q = a[3+3] --> q = a[6] --> q = 5 */
    p += 6;         /* p = a[3+6] --> p = a[9] --> p = 8 */
    ```


### Subtracting an Integer from a Pointer
* Subtracting an integer `j` from a pointer `p` means a pointer to the element `j` places before the one the `p` currently points to
  * if `p` points to the array element a[i], then `p - j` points to `a[i-j]`
    ```c
    int a[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, *p, *q;

    p = &a[8];      /* p = 8 */
    q = p - 3;      /* q = a[8-3] --> q = a[5] --> q = 5 */
    p -= 6;         /* p = a[8-6] --> p = a[2] --> p = 2 */
    ```


### Subtracting One Pointer from Another
* When subtracting one pointer from another, the result is the distance (in array elements) between the pointers
  * If `p` points to `a[i]` and `q` points to `a[j]`, then `p - q` is equal to `i - j`.
    ```c
    int a[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, *p, *q, i;

    p = &a[5];
    q = &a[1];

    i = p - q;      /* i = 4  */
    i = q - p;      /* i = -4 */
    ```
**NOTE:** Performing arithmetic on a pointer that doesn't point to an array element causes undefined behavior. Furthermore, the effect of subtracting one pointers is undefined unless both point to elements of the same array.


### Comparing Pointers
* Relational operators (`<, <=, >, >=`) and the equality operators (`==` and `!=`) can still be used
* Relational operators
  * Meaningful only when both pointers point to elements of the same array
* Equality operators
  * The outcome of comparison depends on the relative positions of the two elements in the array
* Example
```c
p = &a[5];
p = &a[1];

p <= q    /* 0 */
p >= q    /* 1 */
```
