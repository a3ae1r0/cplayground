#include <stdio.h>
#include <string.h>

int main(void)
{
    int a[] = {99}, b[];

    printf("a --> %d\n", a);
    memcpy(a, b, sizeof(a));
    printf("b --> %d\n", b);

    return 0;
}
