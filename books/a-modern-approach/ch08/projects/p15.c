#include <stdio.h>

#define MESSAGE_MAXSIZE 80

int main()
{
    char ch, message[MESSAGE_MAXSIZE] = {0};
    int i = 0, length = 0, shift;

    /* get original message */
    printf("Enter message to be encrypted: ");
    while ( (ch = getchar()) != '\n') {
        message[i] = ch;
        i++;
        length++;
    }

    /*get shift */
    printf("Enter shift amount (1-25): ");
    scanf("%d", &shift);

    /* print chiphertext */
    printf("Encrypted message: " );
    for (i = 0; i < length; i++) {

        ch = message[i];

        if ( (ch >= 'A' && ch <= 'Z')) {
            printf("%c", (((ch - 'A') + shift) % 26 + 'A'));
        } else if (ch >= 'a' && ch <= 'z') {
            printf("%c", (((ch - 'a') + shift) % 26 + 'a'));
        } else {
            printf("%c", ch);
        }

    }

    printf("\n");

    return 0;
}
