/*
 * Each student : Total and AVG
 * Each quiz    : AVG, high score and low score
 *
 * array[row][column] == grades[student][grade]
 */
#include <stdio.h>

#define N_STUDENTS 5
#define N_QUIZ 5

int main(void)
{
    int student, quiz, grades[N_STUDENTS][N_QUIZ] = {0};
    float student_total, quiz_total, quiz_score, low_quiz, high_quiz;

	/* get array elements (quiz per student) */
    for (student = 0; student < N_STUDENTS; student++) {
        printf("Enter grades for student %d: ", student + 1);
        for (quiz = 0; quiz < N_QUIZ; quiz++)
            scanf("%d", &grades[student][quiz]);
    }

    /********************************
     * Computing area
     ********************************/
    /* students */
    printf("\n\n--Students--");         /* header */
    /** total + avg score **/
    for (student = 0; student < N_STUDENTS; student++) {
        student_total = 0;
        for (quiz = 0; quiz < N_QUIZ; quiz++)
            student_total += grades[student][quiz];

        printf("\n- Student %i", student + 1);
        printf("\nTotal score\t:  %.2f", student_total);
        printf("\nAverage score\t:  %.2f", student_total / N_QUIZ);
    	printf("\n");
    }

    /* quizzes */
    printf("\n\n--Quizzes--");         /* header */
    /** total + avg score **/
    for (quiz = 0; quiz < N_QUIZ; quiz++) {
		quiz_total = 0;
		/* init low and high to 1st element of the array. Calc from there after */
		low_quiz = grades[0][0];
		high_quiz = grades[0][0];

        for (student = 0; student < N_STUDENTS; student++) {
			quiz_total += grades[student][quiz];
			quiz_score = grades[student][quiz];

			if (low_quiz > quiz_score) { low_quiz = quiz_score; }
			if (high_quiz < quiz_score) { (high_quiz = quiz_score); }
        }
        printf("\n- Quiz %i",  quiz + 1);
        printf("\nAverage score\t:  %.2f", quiz_total / N_STUDENTS);
        printf("\nLow score\t:  %.2f", low_quiz);
        printf("\nHigh score\t:  %.2f", high_quiz);
    	printf("\n");
    }

    return 0;
}
