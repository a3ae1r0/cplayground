#include <stdio.h>
#include <stdbool.h>

#define ROWS_MAX 99
#define COLUMNS_MAX 99

int main (void)
{
    int square[ROWS_MAX][COLUMNS_MAX] = {0};
    bool square_used[ROWS_MAX][COLUMNS_MAX] = {false};
    int n, i, j;
    int row, column, row_og, column_og;

    /* get n from user */
    printf("This program creates a magic square of specified size.\n"
           "The size must be an odd number between 1 and 99.\n"
           "Enter size of magic number: ");
    scanf("%d", &n);

    /* starting point */
    row = 0, column = (n / 2);
    square[row][column] = 1;
    square_used[row][column] = true;

    for (i = 2; i <= n*n; i++) {
        /* save values b4 change them. usefull when we need 
         * to restore column, and go row -1 */
        row_og = row;
        column_og= column;

        /* now start with movement */
        row--;
        column++;

        /********************* 
         * row bound limit check
         *********************/
        if (row < 0) {              /* when incrementing x + 1*/
            row = (n - 1);
        }

        /* column bound limit check */
        if (column > (n - 1)) {
            column = 0;
        }

        /********************* 
         * fill magic square
         *********************/
        if (!square_used[row][column]);
        else {
            column = column_og;         /* revert to previoustly value */
            row = row_og + 1;           /* go down by 1 */

            /* when puting a new number below, if it is last row, 
             * go back to top */
            if ( row > (n - 1)) { 
                row = row;
                row = 0;
            }
        }
       square[row][column] = i;
       square_used[row][column] = true;
    }

    /* show magic */
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("%d \t", square[i][j]);
        }
        printf("\n");
    }

    return 0;
}
