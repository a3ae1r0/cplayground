/**********************************************************
 * Modify programming project 5 from chapter 7 so that the
 * program labels its ouput:
 *
 * Enter phone number: 1-800-COL-LECT
 * In numeric form: 1-800-265-5328
 **********************************************************/
#include <stdio.h>

#define PHONE_NUMBER_MAX_SIZE 15

int main(void)
{
    char phoneNumber[PHONE_NUMBER_MAX_SIZE] = {}, ch;
    int i = 0;

    /** get user input (phone number) **/
    printf("Enter phone number: ");
    while ((ch = getchar()) != '\n' ) {
        phoneNumber[i] = ch;
        i++;

        /* assume that the phone number is no more than 15 characteres long */
        if (i >= PHONE_NUMBER_MAX_SIZE) {
            printf("\nPhone number is longer than 15 characters.\n"
                    "Please try again. MAX: 15 char.\n\n");
            return 0;          /* avoid buffer overflow */
        }
    }

    /** convert and print phone number in numeric **/
    printf("In numeric form: ");
    for (i = 0; i < PHONE_NUMBER_MAX_SIZE; i++) {
            switch (phoneNumber[i]) {
                case 'A': case 'B': case 'C':
                    putchar('2');
                    break;
                case 'D': case 'E': case 'F':
                    putchar('3');
                    break;
                case 'G': case 'H': case 'I':
                    putchar('4');
                    break;
                case 'J': case 'K': case 'L':
                    putchar('5');
                    break;
                case 'M': case 'N': case 'O':
                    putchar('6');
                    break;
                case 'P': case 'R': case 'S':
                    putchar('7');
                    break;
                case 'T': case 'U': case 'V':
                    putchar('8');
                    break;
                case 'W': case 'X': case 'Y':
                    putchar('9');
                    break;
                default:
                    putchar(phoneNumber[i]);
                    break;
            }
    }
    printf("\n");

    return 0;
}
