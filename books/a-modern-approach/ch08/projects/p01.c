#include <stdio.h>
#include <stdbool.h>

#define DIGIT_MAX 10

int main(void)
{
    int digit, digit_seen[DIGIT_MAX] = {false};
    long n;

    printf("Enter a number: ");
    scanf("%ld", &n);

    while (n > 0) {
        digit = n % 10;
        /* increment repeated number */
        digit_seen[digit]++;
        n /= 10;
    }

    printf("Repeated digit(s): ");
    for (n = 0; n < DIGIT_MAX; n++) {
        /* if number was incremented (>2 then it is repeated) */
        if (digit_seen[n] > 1)
            printf("%ld ", n);
    }
    printf("\n");

    return 0;
}
