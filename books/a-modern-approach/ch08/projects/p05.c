#include <stdio.h>

#define VALUE 100.00
#define ARRAY_SIZE 12

int main(void)
{
    int rate, years, i;
    float money[ARRAY_SIZE];

    printf("Enter interest rate: ");
    scanf("%d", &rate);
    printf("Enter number of years: ");
    scanf("%d", &years);

    for (i = 0; i < ARRAY_SIZE; i++) {
        printf("%5d%%  ", rate + i);
        money[i] = VALUE;
    }

    for (i = 1; i < ARRAY_SIZE; i++) {
        /* don't work */
        money[i-1] += (((rate + 0) / 100.0 ) * money[i-1]);
        money[0] += (((3 + 0) / 100.0 ) * 100.0);
        money[1] += (((3 + 0) / 100.0 ) * 103.0);
        printf("%i --> %7.2f \n", i, money[i-1]);
    }

    return 0;
}
