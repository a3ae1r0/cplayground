#include <stdio.h>
#include <ctype.h>

#define ALPHABET_SIZE 26

int main()
{
    int word_count[ALPHABET_SIZE] = {0};
    char ch;
    int i, anagram = 0;

    printf("Enter first word: ");
    /* char input is convertet to lower */
    while ( (ch = tolower(getchar())) != '\n') {
        /* check if is alphabetic char */
        if (isalpha(ch)) {
            /* 
             * use char as a int. So remove 97 ('a' value) to get the
             * correspondent char integer between 0 - 26
             * eg:
             * a = word_count[0]
             * b = word_count[1]
             */
            word_count[ch - 97] += 1;
        }
    }

    printf("Enter second word: ");
    while ( (ch = tolower(getchar())) != '\n') {
        if (isalpha(ch)) {
        word_count[ch - 97] -= 1;
        }
    }

    /* if the words are anagrams we should have 26 zeros. Let's check! */
    for (i = 0; i < 26; i++) {
        if (word_count[i] == 0) {
            anagram += 1;
        }
    }

    if (anagram == 26) {
        printf("The words are anagrams.\n");
    } else {
        printf("The words are not anagrams.\n");
    }

    return 0;
}
