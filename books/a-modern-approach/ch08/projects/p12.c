/**********************************************************
 * Modify programming project 6 from chapter 7: so SCRABBLE
 * values (letters value) are set in a array.
 *
 * Read the character from user input, and use the array to
 * get value of each char
 **********************************************************/
#include <stdio.h>
#include <ctype.h>

#define ALPHABET_SIZE 26

int main (void)
{
    char ch;
    int sum = 0, values[ALPHABET_SIZE] = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1,
                                          3, 1, 1, 3, 10, 1, 1, 1, 1, 1, 4, 8,
                                          4, 10};

    printf("Enter a word: ");
    while ((ch = getchar()) != '\n') {
        ch = toupper(ch);
        sum += values[ch - 65];

    }
    printf("Scrable value: %d\n", sum);
    return 0;
}
