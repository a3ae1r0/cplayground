#include <stdio.h>

#define ARRAY_SIZE 5

int main(void)
{
    int row, column, square[ARRAY_SIZE][ARRAY_SIZE] = {0}, row_sum, column_sum;

    for (row = 0; row < ARRAY_SIZE; row++) {
        printf("Enter row %d: ", row + 1);
        for (column = 0; column < ARRAY_SIZE; column++)
            scanf("%d", &square[row][column]);
    }

    /**** row results ****/
    printf("\nRow totals\t:");
    for (row = 0; row < ARRAY_SIZE; row++) {
        row_sum = 0;
        for (column = 0; column < ARRAY_SIZE; column++)
            row_sum += square[row][column];
        printf(" %d", row_sum);
    }

    /**** column results ****/
    printf("\nColumn totals\t:");
    for (column = 0; column < ARRAY_SIZE; column++) {
        column_sum = 0;
        for (row = 0; row < ARRAY_SIZE; row++)
            column_sum += square[row][column];
        printf(" %d", column_sum);
    }
    printf("\n");

    return 0;
}
