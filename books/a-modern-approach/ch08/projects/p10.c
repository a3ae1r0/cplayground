#include <stdio.h>

#define N_DEPARTURES 8

int main(void)
{
    int departure[8] = {480, 583, 679, 767, 840, 945, 1140, 1305};
    int arrival[8] = {616, 712, 811, 900, 968, 1075, 1280, 1438};
    int i, hours, minutes, time_in_minutes;
    int lowest_time = 999, closest_departure = 0, arrival_time = 0;

    printf("Enter a 24-hour time: ");
    scanf("%d:%d", &hours, &minutes);
    time_in_minutes = hours * 60 + minutes;

    for (i = 0; i < N_DEPARTURES; i++) {
        if (time_in_minutes % departure[i] < lowest_time) {
            /* calculate lowest differencial time */
            lowest_time = (time_in_minutes % departure[i]);
            /* save departure index array*/
            closest_departure = departure[i];
            /* save arrival index array*/
            arrival_time = arrival[i];
        } else
            continue;
    }

    /* departure time */
    printf("Closest departure time is %d:%.2d ", closest_departure / 60, closest_departure % 60);
    if (closest_departure < 720)
        printf("a.m.");
    else
        printf("p.m.");

    /* arriving time */
    printf(", arriving at ");
    if (arrival_time < 720)
        printf("%d:%.2d a.m.\n", arrival_time / 60, arrival_time % 60);
    else
        /* separetes AM/PM, but also calc -12 when PM */
        printf("%d:%.2d p.m.\n", (arrival_time / 60) - 12, arrival_time % 60);

    return 0;
}
