#include <stdio.h>

#define SENTENCE_MAXSIZE 72 

int main (void)
{
    char sentence[SENTENCE_MAXSIZE], ch = ' ', terminator = ' ';
    char temp[SENTENCE_MAXSIZE], new_length = 0, k = 0;
    int i, j, length = 0;

    printf("Enter a sentence: ");
    while ( (ch != '.') && (ch != '!') && (ch != '?')) {
        ch = getchar();
        sentence[length] = ch;
        length++;
    }

    /* terminating char has the index length minus 1. Saved it in a var */
    terminator = sentence[length - 1];

    ch = 'x';       /* clean ch. cannot be a space */

    /* 
     * start from end to begin of inputed sentece
     * -2 to avoid terminating char
     */
    for (i = (length - 2); i >= 0; i--) {
        
        ch = sentence[i];

        /* 
         *
         * get reversed word and saved in a new array
         * end when reach a space or the begin of original sentence
         */
        if ( (ch != ' ') && (i >= 0) ) {
            temp[k] = ch;           /* independent int used for iteration */
            new_length++;           /* useful on for loop priting later */
            k++;
        } 

        /* 
         * if is space char or we reach the begining of original
         * sentence -- then --> printf word
         */
        if ( ch == ' ' || i == 0 ) {
            for (j = new_length; j >= 0; j--) {
                printf("%c", temp[j]);
            }

            /* reset values for new word */
            new_length = 0;
            k = 0;
           
            /* 
             * i did this to clean array, but now i dont
             * know why or how this work....
             */
            for (j = 0; j < length; j++) {
                temp[j] = ' ';
            }
           
        }

    }

    printf("%c \n", terminator);

    return 0;
}
