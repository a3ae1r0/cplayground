/**********************************************************
 * Modify programming project 11 from chapter 7:
 * - The program will need to store the last name (but not
 *   the first) in a array of characters
 **********************************************************/
#include <stdio.h>

#define LASTNAME_LENGHT 20

int main(void)
{
    char firstLetter, ch, lastName[LASTNAME_LENGHT] = {' '};
    int i = 0;

    printf("Enter a first and last name: ");
    /* get 1st letter of 1st name. Not stored in a array */
    scanf(" %c", &firstLetter);

    /* discard after 1st name letter until start last name */
    while ((ch = getchar()) != ' ') {
    }

    /* populate array with char input it by user */
    while ((ch = getchar()) != '\n') {
        lastName[i] = ch;
        i++;
    }

    printf("You entered the name: ");
    for (i = 0; i < LASTNAME_LENGHT; i++) {
        printf("%c", lastName[i]);
    }
    printf(", %c.\n", firstLetter);

    return 0;
}
