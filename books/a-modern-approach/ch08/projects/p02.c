#include <stdio.h>

#define MAX_DIGIT 10

int main(void)
{
    int i, digit, digit_seen[MAX_DIGIT] = {0};
    long n;

    printf("Enter a number: ");
    scanf("%ld", &n);

    while (n > 0) {
        digit = n % 10;
        digit_seen[digit]++;
        n /= 10;
    }

    printf("Digit: \t\t");
    for (i = 0; i < MAX_DIGIT; i++)
        printf("%d ", i);

    printf("\nOccorrences: \t");
    for (n = 0; n < MAX_DIGIT; n++)
        printf("%d ", digit_seen[n]);
    printf("\n");

    return 0;
}
