#include <stdio.h>
#include <ctype.h>

#define MSG_MAXSIZE 40

int main(void)
{
    char ch, msg[MSG_MAXSIZE];
    int i, chCount = -1;        /* -1 to remove '\0' in end of the string */

    printf("Enter message: ");
    for (i = 0; ch != '\n'; i++) {
        scanf("%c", &ch);
        msg[i] = ch;
        /* string length. hack to overwrite initial array size bc of printf */
        chCount++;
    }

    /* a more compact solution found online */
    /* for (chCount = 0; (msg[chCount] = toupper(getchar())) != '\n'; chCount++) */
    /*     ; */

    printf("In B1FF-speak: ");
    for (i = 0; i < chCount; i++ ) {
        msg[i] = toupper(msg[i]);

        switch (msg[i]) {
            case 'A' : msg[i] = '4';
                       break;
            case 'B' : msg[i] = '8';
                      break;
            case 'E' : msg[i] = '3';
                       break;
            case 'I' : msg[i] = 'I';
                       break;
            case 'O' : msg[i] = '0';
                       break;
            case 'S' : msg[i] = '5';
                       break;
            default: msg[i] = msg[i];
                     break;
        }
        printf("%c", msg[i]);
    }
    printf("!!!!!!!!!!\n");

    return 0;
}
