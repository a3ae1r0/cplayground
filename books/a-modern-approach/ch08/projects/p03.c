#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    bool digit_seen[10] = {false};
    int digit, i;
    long n;

    printf("Enter multiple numbers (0 or negative numbers to terminate): ");
    scanf("%ld", &n);

    /* refactor code: get multiple numbers from user */
    while (n > 0) {
        /* reset seen number*/
        for (i = 0; i < 10; i++) {
            digit_seen[i] = false;
        }

        /* output working number to user */
        printf("number %ld ", n);

        /* maintain default code */
        while (n > 0) {
            digit = n % 10;
            if (digit_seen[digit]) {
                break;
            }
            digit_seen[digit] = true;
            n /= 10;
        }

        if (n > 0)
            printf("has repeated digit(s)\n");
        else
            printf("no repeated digit(s)\n");

        /* get another number */
        scanf("%ld", &n);
    }

    return 0;
}
