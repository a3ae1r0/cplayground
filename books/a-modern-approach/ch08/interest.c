#include <stdio.h>

#define VALUE 100.00
#define ARRAY_SIZE 5

int main(void)
{
    int rate, years, i, j;
    float money[ARRAY_SIZE];

    printf("Enter interest rate: ");
    scanf("%d", &rate);
    printf("Enter number of years: ");
    scanf("%d", &years);

    printf("Years");
    for (i = 0; i < ARRAY_SIZE; i++) {
        printf("%5d%%  ", rate + i);
        money[i] = VALUE;
    }
    printf("\n");

    for (i = 1; i <= years; i++) {
        printf("  %d  ", i);
        for (j = 0; j < ARRAY_SIZE; j++) {
            money[j] += ((rate + j) / 100.0 ) * money[j];
            printf("%7.2f ", money[j]);
        }
        printf("\n");
    }
    return 0;
}
