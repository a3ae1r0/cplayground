#include <stdio.h>

int main(void)
{
    int a[10], i;

    for(i = 1; i <= 10; i++) {
        a[i] = 0;
    }

    return 0;
}

/* OUTPUT:
 * *** stack smashing detected ***: terminated
 * [1]    12725 abort (core dumped)  ./a.out
 */
