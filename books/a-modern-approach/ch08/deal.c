/* Deals a random hand of cards */
#include <stdio.h>
#include <stdbool.h>    /* C99 only... but works on my machine with c89... */
#include <time.h>
#include <stdlib.h>

#define N_RANK 13
#define N_SUIT 4

int main(void)
{
    bool in_hand[N_RANK][N_SUIT] = {false};
    int n_cards, rank, suit;
    const char rank_code[13] = {'2', '3', '4', '5', '6', '7', '8', '9',
                           't', 'j', 'q', 'k', 'a'};
    const char suit_code[4] = {'c', 'd', 'h', 's'};

    /* use OS system time to make rand function really random */
    srand ( time(NULL));

    printf("Enter number of cards in hand: ");
    scanf("%d", &n_cards);

    printf("Your hand:");
    while (n_cards > 0) {
        rank = rand() % N_RANK;
        suit = rand() % N_SUIT;

        if (!in_hand[rank][suit]) {
            in_hand[rank][suit] = true;
            printf(" %c%c", rank_code[rank], suit_code[suit]);
            n_cards--;
        }
    }
    printf("\n");

    return 0;
}
