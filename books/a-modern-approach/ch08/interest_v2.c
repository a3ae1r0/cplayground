#include <stdio.h>

#define INITIAL_BALANCE 100.00
#define ARRAY_SIZE 5
#define NUM_RATES ((int) (sizeof(value) / sizeof(value[0])))

int main (void)
{
    int i, low_rate, num_years, year;
    double value[ARRAY_SIZE];

    printf("Enter interest rate: ");
    scanf("%dn", &low_rate);
    printf("Enter number of years: ");
    scanf("%d", &num_years);

    printf("\nYears");
    for (i = 0; i < NUM_RATES; i++) {
        printf("%6d%%", low_rate + i);
        value[i] = INITIAL_BALANCE;
    }
    printf("\n");

    for (year = 1; year <= num_years; year++) {
        printf("%d    ", year);
        for (i = 0; i < NUM_RATES; i++) {
            value[i] += (low_rate + i) / 100.0 * value[i];
            printf("%7.2f ", value[i]);
        }
        printf("\n");
    }

    return 0;
}
