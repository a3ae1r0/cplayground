/* Reverses a series of numbers */
#include <stdio.h>

#define MAX_NUMBERS 10

int main(void)
{
    int i, n[MAX_NUMBERS];

    printf("Enter %d numbers: ", MAX_NUMBERS);
    for (i = 0; i < MAX_NUMBERS; i++) {
        scanf("%d", &n[i]);
    }

    printf("In reverse order: ");
    for (i = MAX_NUMBERS - 1; i >= 0; i--) {
        printf("%d ", n[i]);
    }

    printf("\n");

    return 0;
}
