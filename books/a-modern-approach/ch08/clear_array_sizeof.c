#include <stdio.h>

#define SIZE ((int) (sizeof(a) / sizeof(a[0])))

int main(void)
{
    int a[5] = {1, 2, 3, 4, 5};
    int i;

    printf("%d\n", a[3]);

    /* clear array values */
    /* for (i = 0; i < (int) (sizeof(a) / sizeof(a[0])); i++) { */
    for (i = 0; i < SIZE; i++) {
        a[i] = 99;
    }
    printf("%d\n", a[3]);

    return 0;
}
