---
Name: C Programming: A Modern Approach
Edition: 2nd Edition
Author: K. N. King
Publisher: W. W. Norton & Company; 
Year: April 19, 2008
Type: Programming
---

Book notes, exercises and projects.
