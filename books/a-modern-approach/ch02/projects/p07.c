/* Project 7, p34
 * read input from user (bill) and then shows how to pay that amount using the
 * smallest number of $20, $10, $5, $1
 */
#include <stdio.h>

int main(void)
{

    int twenty_bill, ten_bill, five_bill, one_bill, bill, left;
    left = 0;

    printf("Enter a dollar bill.: ");
    scanf("%d", &bill);

    /* $20 */
    twenty_bill = bill / 20;
    printf("$20 bills: %d\n", twenty_bill);

    /* find what is left */
    left = bill - (20 * twenty_bill);

    /* $10 */
    ten_bill = left / 10;
    printf("$10 bills: %d\n", ten_bill);
    
    /* find what is left */
    left = left - (10 * ten_bill);
    
    /* $5 */
    five_bill = left / 5;
    printf(" $5 bills: %d\n", five_bill);
    
    /* find what is left */
    left = left - (5 * five_bill);
    
    /* $1 */
    one_bill = left / 1;
    printf(" $1 bills: %d\n", one_bill);

    return 0;
}
