/* Project4, p43
 * Write a program that asks the user to enter a dollars-and-cents amount, then
 * displays the amount with 5% tax added:
 *
 * Enter an amount: 100.00/
 * With tax added:  $105.00
 */
#include <stdio.h>

#define TAX 0.05f

int main(void)
{
    float before_tax, after_tax;

    printf("Enter an amount : ");
    scanf("%f", &before_tax);

    after_tax = (before_tax * TAX) + before_tax ;
    printf("With tax added  : $%.2f\n", after_tax);

    return 0;
}
