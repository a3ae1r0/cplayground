#include <stdio.h>

# define PI_VALUE 3.14f
# define SCALE_FACTOR (4.0f / 3.0f ) * PI_VALUE

int main(void)
{
    int radius;
    float volume;

    printf("Please Sir, enter a sphere radius : ");
    scanf("%d", &radius);

    /* formula : v = 4/3πr3 */
    volume = SCALE_FACTOR * radius * radius * radius;
    printf("Volume (user input) : %.3fm3\n", volume);

    return 0;
}
