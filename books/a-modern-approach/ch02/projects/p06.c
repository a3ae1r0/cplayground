/* Project6, p34
 * read x from user then calculate:
 * ((((3x+2)x-5)x-1)x+7)x-6
 */
#include <stdio.h>

int main(void)
{
    int x;

    printf("Insert value of x : ");
    scanf("%d", &x);
    printf("((((3x+2)x-5)x-1)x+7)x-6 = %d", 
    ((((3 * x + 2) * x - 5) * x - 1) * x + 7) * x - 6);

    return 0;
}
