/* Project 8, p35
 * Write a program the calculates the remaining balance on a loan after the 
 * first, second, and third monthly payments.
 */
#include <stdio.h>

int main(void)
{
    float rate, payment, balance;

    printf("Enter amount of loan  : ");
    scanf("%f", &balance);
    printf("Enter interest rate   : ");
    scanf("%f", &rate);
    printf("Enter monthly payment : ");
    scanf("%f", &payment);

    /* monthly balance = loan - payment + ((interested_rate/12) x balance) */
    balance = balance - payment + (((rate/12) / 100) * balance);
    printf("Balance remaining after first payment  : $%.2f\n", balance);
    balance = balance - payment + (((rate/12) / 100) * balance);
    printf("Balance remaining after second payment : $%.2f\n", balance);
    balance = balance - payment + (((rate/12) / 100) * balance);
    printf("Balance remaining after third payment  : $%.2f\n", balance);
    
    return 0;
}
