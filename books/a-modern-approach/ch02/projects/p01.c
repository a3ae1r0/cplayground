/* Programming Projects, Ex. 1, page 34
Display:
           *
          *
         *
    *   *
     * *
      *
*/
#include <stdio.h>

int main(void)
{
    printf("       *\n");
    printf("      * \n");
    printf("     *  \n");
    printf("*   *   \n");
    printf(" * *    \n");
    printf("  *     \n");

    return 0;
}
