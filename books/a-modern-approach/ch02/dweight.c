/*  Computes the dimensional weight of a 12" x 10" x 8"box */
#include <stdio.h>

int main (void)
{
    int HEIGHT, LENGTH, WIDTH, VOLUME, WEIGHT;
/*    int HEIGHT = 8, LENGTH = 12, WIDTH = 10, VOLUME, WEIGHT; */
/*    int HEIGHT, LENGTH, WIDTH = 10, VOLUME, WEIGHT; */
    
    HEIGHT = 8;
    LENGTH = 12;
    WIDTH  = 10;
    VOLUME = HEIGHT * LENGTH * WIDTH;                           /* statement */
    WEIGHT = (VOLUME + 165) / 166;

    printf("Dimensions : %d x %d x %d\n", HEIGHT, LENGTH, WIDTH); 
    printf("Volume (cubic inches) : %d\n", VOLUME);             /* statement */
/*    printf("Volume (cubic inches) : %d\n", HEIGHT * LENGTH * WIDTH);*/
    printf("Dimensional weight (pounds) : %d\n", WEIGHT); 

    return 0;
}
