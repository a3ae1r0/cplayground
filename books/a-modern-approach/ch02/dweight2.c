/* Computes the dimensional weight of a box from a input provder by the user */
#include <stdio.h>

int main(void)
{
    int HEIGHT, LENGTH, WIDTH, VOLUME, WEIGHT;

    printf("Enter height of box : ");
    scanf("%d", &HEIGHT);
    printf("Enter lenght of box: ");
    scanf("%d", &LENGTH);
    printf("Enter width of box: ");
    scanf("%d", &WIDTH);

    VOLUME = HEIGHT * LENGTH * WIDTH;
    WEIGHT = (VOLUME + 165) / 166;

    printf("Volume (cubic inches) : %d\n", VOLUME);
    printf("Dimensional weight (pounds): %d\n", WEIGHT);

    return 0;
}
