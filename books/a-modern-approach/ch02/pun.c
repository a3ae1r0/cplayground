/* include standard I/O library */
#include <stdio.h>

/* "main" program. void = no arguments [main function] */
int main(void)
{
    /* printf is a function from I/O library [function call] */
    printf("To C, or not to C: that is the question.\n");   
    printf("\n");
    printf("Brevity is the sour fo wit.\n  --Shakespeare\n");

    /* returns the value 0 to OS to indicate that the program terminates. 
     * [statement] */
    return 0;
}

/*===========================================
directives

int main(void)
{
    statements
}

# --> directive

<stdio> --> header
    stdio.h provides fuction ability to perfom input 
    and ouput operations

function
    a series of statmentes that have been grouped
    together and given a name.

statement
    command to be executed when the program runs.
    Each statement end with a ;
==============================================*/
