/* Converts a Fahrenheit tempature to Celsius */
#include <stdio.h>

#define FREEZING_PT 32.0f
/* if 5 and 9 was a integer, the result would have been truncated (0) */
#define SCALE_FACTOR (5.0f / 9.0f)

int main(void)
{
    float FAHRENHEIT, CELSIUS;

    printf("Enter Fahrenheit temperature : ");
    scanf("%f", &FAHRENHEIT);
    
    /* calculations */
    CELSIUS = (FAHRENHEIT - FREEZING_PT) * SCALE_FACTOR;
    printf("Celsius equivalent is : %.1f\n", CELSIUS);

    return 0;
}
