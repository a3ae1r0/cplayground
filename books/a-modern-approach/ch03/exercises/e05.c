/* CH03, Ex5, p49
 * Suppose that we call scanf as follows:
 * scanf("%f%d%f", &x, &i, &y)
 *
 * If the user enters
 * 12.3 45.6 789
 *
 * what will be the values of x, i and y after the call? (Assume that x a nd y
 * are float variables and i is ant in variable.)
 *
 */
#include <stdio.h>

int main (void)
{
    int i;
    float x, y;

    scanf("%f%d%f", &x, &i, &y);
    printf("%f\t%d\t%f", x, i, y);

    return 0;
}

/* Output:
 *
 *
 * Anwser:
 * x --> since is a float, scanf will read the number unter find the space,
 * so 12.3 will be value saved to the variable.
 * scanf will put the rest back in the buffer: 45.6 789
 *
 * i --> since is a integer, scanf will read until find the ., since a integer
 * dont have decimal part. A value for the var i will be 45.
 * will be left .6 789
 *
 * y --> since is float, scanf will read until find a not suitable a ilegal
 * character for a float, like a space. The value of y will be 0.6
 *
 * Out of this scanf will be 789. This value only will be cath in the next
 * scanf call

 */
