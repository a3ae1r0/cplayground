/* For each of the following pairs of scanf format strings, indicate wether or
 * not the two strings are equivalent. If they're not, show how they can be
 * distinguished.
 */
#include <stdio.h>

int main (void)
{
    int a, b0, b1, b2;
    float c, d0, d1;

    /* (a) example */
    printf("(a)\n");

    scanf("%d", &a);
    printf("%d\n", a);

    scanf(" %d", &a);
    printf("%d\n", a);

    /* (b) example */
    printf("(b)\n");
    scanf("%d-%d-%d", &b0, &b1, &b2);
    printf("%d %d %d\n", b0, b1, b2);

    scanf("%d -%d -%d", &b0, &b1, &b2);
    printf("%d %d %d\n", b0, b1, b2);

    /* (c) example */
    printf("(c)\n");
    scanf("%f", &c);
    printf("%f\n", c);

    scanf("%f ", &c);
    printf("%f\n", c);

    /* (d) example */
    printf("(d)\b");
    scanf("%f,%f", &d0, &d1);
    printf("%f %f\n", d0, d1);

    scanf("%f, %f", &d0, &d1);
    printf("%f %f\n", d0, d1);
}

/* Answers:
 * (a) Equivalent
 * (b) Equivalent
 * (c) Not equivalent. The second options will be hanging for a user input.
 * (d) Equivalent
 *
 * Good answer:
 * (a), (b), (d) white-space character in format string matches ANY number of
 *  white-space characters in the input string (including none).
 * (c) The 2nd format string causes scanf to search for an additional non-blank
 * character after the number.
 */
