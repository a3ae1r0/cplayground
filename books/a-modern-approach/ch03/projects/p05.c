/* CH03, Project 5, p50
 * Write a program that asks the user to enter the numbers from 1 ato 16
 * (in any order) and then displays the numbers in a 4 by 4 arrangement,
 * followed by the sums of the rows, columns, and diagonals.
 */
#include <stdio.h>

int main (void)
{

    int numbers[16];
    int row_sums0, row_sums1, row_sums2, row_sums3;
    int column_sums0, column_sums1, column_sums2, column_sums3;
    int diagonal_sums0, diagonal_sums1;

    printf("Enter ther numbers from 1 to 6 in any order:");
    /* get input from user and storing it in an array*/
    for (int i = 0; i < 16; ++i) {
        scanf("%d", &numbers[i]);
    }

    /* printing first 4 elements of an array */
    for (int i = 0; i < 4; ++i) {
        printf("%2d  ", numbers[i]);
    }
    printf("\n");

    /* printing next 4 elements of an array */
    for (int i = 4; i < 8; ++i) {
        printf("%2d  ", numbers[i]);
    }
    printf("\n");

    /* printing next 4 elements of an array */
    for (int i = 8; i < 12; ++i) {
        printf("%2d  ", numbers[i]);
    }
    printf("\n");

    /* printing last 4 elements of an array */
    for (int i = 12; i < 16; ++i) {
        printf("%2d  ", numbers[i]);
    } printf("\n");

    /* arithmetic operators with arrays*/
    row_sums0 = numbers[0] + numbers[1] + numbers[2] + numbers[3];
    row_sums1 = numbers[4] + numbers[5] + numbers[6] + numbers[7];
    row_sums2 = numbers[8] + numbers[9] + numbers[10] + numbers[11];
    row_sums3 = numbers[12] + numbers[13] + numbers[14] + numbers[15];
    printf("Row sums      : %d %d %d %d\n", row_sums0, row_sums1,
                                            row_sums2, row_sums3);
    column_sums0 = numbers[0] + numbers[4] + numbers[8] + numbers[12];
    column_sums1 = numbers[1] + numbers[5] + numbers[9] + numbers[13];
    column_sums2 = numbers[2] + numbers[6] + numbers[10] + numbers[14];
    column_sums3 = numbers[3] + numbers[7] + numbers[11] + numbers[15];
    printf("Column sums   : %d %d %d %d\n", column_sums0, column_sums1,
                                            column_sums2, column_sums3);
    diagonal_sums0 = numbers[0] + numbers[5] + numbers[10] + numbers[15];
    diagonal_sums1 = numbers[3] + numbers[6] + numbers[9] + numbers[12];
    printf("Diagonal sums : %d %d\n", diagonal_sums0, diagonal_sums1);

    return 0;
}
