/* CH03, Project 3, p50
 * Write a program that breaks down an ISBN (Internal Standard Book Number)
 * entered by the user.
 */
#include <stdio.h>

int main(void)
{
    int gsi_ean, group_identifier, publisher, item_title, check_digit;

    printf("Enter ISB ........: ");
    scanf("%d-%d-%d-%d-%d", &gsi_ean, &group_identifier, &publisher,
                            &item_title, &check_digit);

    /* all together now in one print call */
      printf("GS1 prefix: %d\n"
              "Group Identifier : %d\n"
              "Publisher code: %d\n"
              "Item number: %d\n"
              "Check digit: %d\n",
            gsi_ean, group_identifier, publisher, item_title, check_digit);

    return 0;
}
