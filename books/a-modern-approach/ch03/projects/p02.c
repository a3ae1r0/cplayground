/* CH03, Project 2, p50
 * Write a program that formats product information entered by the user.
 */
#include <stdio.h>

int main(void)
{
    int item, month, day, year;
    float price;

    printf("Enter item number : ");
    scanf("%d", &item);
    printf("Enter unit price  : ");
    scanf("%f", &price);
    printf("Enter purchase date (mm/dd/yyyy) : ");
    scanf("%d/%d/%d", &month, &day, &year);

    printf("Item\tUnit\tPurchase\n");
    printf("\tPrice\tDate\n");
    printf("%d\t$%7.2f\t%.2d/%.2d/%.4d", item, price, month, day, year);

    return 0;
}
