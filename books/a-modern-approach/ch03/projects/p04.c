/* CH03, Project 4, p50
 * Write a program that prompts the user to enter a telephone number in the
 * form (xxx) xxx-xxxx and the displays the number in the form xxx.xxx.xxx
 */
#include <stdio.h>

int main(void)
{
    int group0, group1, group2;

    printf("Enter phone number [(xxx) xxx-xxxx] : ");
    scanf("(%d) %d-%d", &group0, &group1, &group2);
    printf("You entered : %3d.%3d.%4d", group0, group1, group2);

    return 0;
}
