/* CH03, Project 1, p50
 * Write a program that accepts a date from the user in the form mm/dd/yyyy
 * and the displays it in the from yyyymmdd:
 *
 * Enter a data (mm/dd/yyyy) : 2/17/2011
 * You entered the date 20110217
 */
#include <stdio.h>

int main(void)
{
    int day, month, year;

    printf("Enter a data (mm/dd/yyyy) : ");
    scanf("%2d/%2d/%4d", &month, &day, &year);
    printf("You entered the date      : %.4d%.2d%.2d", year, month, day);

    return 0;
}
