/* Craps Game */
#include <stdio.h>
#include <stdlib.h>
/* #include <time.h> */
#include <stdbool.h>

int roll_dice(void);
bool play_game(void);

int main(void)
{
    char again = 'y';
    int result, wins = 0, losses = 0;

    while((again == 'y' || again == 'Y')) {
        result = play_game();
        printf("\nPlay again? ");
        scanf(" %c", &again);

        /* tracking results */
        if(result == true)
            wins++;
        else
            losses++;
    }

    printf("\nWins: %d    Losses: %d\n", wins, losses);
    return 0;
}

int roll_dice(void)
{
    int i, dice_sum = 0;

    /* line below comment out because it was generating the same number over again */
    /*  (bc of time?) */
    /* srand( (unsigned)time( NULL ) ) ; */
    for (i = 0; i < 2; i++)
        dice_sum += ( (rand() % 6) + 1);    /* +1 to roll between 1-6 */

    return dice_sum;
}

bool play_game(void)
{
    int n_roll = 0, i, pointer;

    /* infinite loop without test condition. The body loop will take care of
    termination */
    for(i = 0; ; i++) {
        n_roll = roll_dice();
        printf("You rolled: %d\n", n_roll);

        /*** 1st roll scenarios ***/
        if(i == 0) {
            if(n_roll == 7 || n_roll == 11) {
                printf("You win!\n");
                return true;
            }
            else if(n_roll == 2 || n_roll == 3 || n_roll == 12) {
                printf("You lose!\n");
                return false;
            } else {
                /*** Output pointer ***/
                pointer = n_roll;
                printf("Your pointer is %d\n", pointer);
            }
        } else {
            if(n_roll == pointer) {
                printf("You win!\n");
                return true;
            } else if (n_roll == 7){
                printf("You lose!\n");
                return false;
            }
        }
    }

    /* Default. Fix GCC C89 warn: "control reaches end of non-void function" */
    return false;
}
