/***********************************************************************
 * Modify Programming Project 9 from Chapter 8:
 * - add functions:
 *   . void generate_random_walk(char walk[10][10]);
 *   . void print_array(char walk[10][10]);
 *
 * - functions calls:
 *   . main call generate_random_walk
 *   . main call print_array
 **********************************************************************/
#include <stdio.h>
#include <stdlib.h>     /* srand */
#include <time.h>       /* time */
#include <stdbool.h>

#define WARD_SIZE 10
#define ALPHABET_SIZE 26

void generate_random_walk(char walk[WARD_SIZE][WARD_SIZE]);
void print_array(char walk[WARD_SIZE][WARD_SIZE]);

/*          Y           X   */
char ward[WARD_SIZE][WARD_SIZE];

int main(void)
{
    generate_random_walk(ward);
    print_array(ward);
    return 0;
}

void generate_random_walk(char walk[WARD_SIZE][WARD_SIZE])
{
    bool ward_used[WARD_SIZE][WARD_SIZE] = {false};
    char alpha, ward_current[WARD_SIZE][WARD_SIZE];
    int row, column, random, x = 0, y = 0;
   
    /* init initial square with dots */
    for (row = 0; row < WARD_SIZE; row++) {
        for (column = 0; column < WARD_SIZE; column++) {
            ward[row][column] = '.';            
        }
    }
    
    srand ((unsigned) time(NULL));
   
    /* walk starting point. 'A' @ [0][0] */
    walk[y][x] = 'A';            
    ward_used[y][x] = true;

    /* a loop to "walk"
     * from "." to alphabet
     */
    for (alpha = 'B'; alpha <= 'Z'; alpha++) {

        random =  rand() % 4; 
       
        /* first rule: don't go outside of the ward */
        if ((random == 0 && y == 0) || (random == 1 && y == 9)
                || (random == 2 && x % 10 == 0) || (random == 3 && x % 10 == 9)){
            alpha--;    /* end here, decrementer alpha, so it can try another random */
        } else {
            switch (random) {
                case 0: y--;            /* change the coordenate accordingly */
                        if (!ward_used[y][x]) {         /* coordenate not in use */
                            walk[y][x] = alpha;         /* assign letter (loop) to coordenate */ 
                            ward_used[y][x] = true;     /* mark coordenat as true */
                        } else {
                            /* revert increments if coordenates were already used b4*/
                            y++;
                            alpha--;
                            break;
                        }
                        break;
                case 1: y++;
                        if ( !ward_used[y][x] ) {
                            walk[y][x] = alpha; 
                            ward_used[y][x] = true;
                        } else {
                            y--;
                            alpha--;
                            break;
                        }
                        break;
                case 2: x--;
                        if (!ward_used[y][x]) {
                            walk[y][x] = alpha; 
                            ward_used[y][x] = true;
                        } else {
                            x++;
                            alpha--;
                            break;
                        }
                        break;
                case 3: x++;
                        if (!ward_used[y][x]) {
                            walk[y][x] = alpha; 
                            ward_used[y][x] = true;
                        } else {
                            x--;
                            alpha--;
                            break;
                        }
                        break;
            } /* end switch*/
        }   /* end else (normal) */

        /* test after each iteration if you are not in a rabbit hole (no place to go)*/ 
        if ((ward_used[y+1][x] && ward_used[y-1][x] && ward_used[y][x+1] && ward_used[y][x-1]) ||   /* all sides in use*/
                ((((!ward_used[y+1][x] && y ==  9) && ward_used[y-1][x]) && ward_used[y][x+1]) && ward_used[y][x-1]) ||   /* all sides used & can't go up */   
                (((ward_used[y+1][x] && (!ward_used[y-1][x]) && y ==  0) && ward_used[y][x+1]) && ward_used[y][x-1]) ||   /* all sides used & can't go down */ 
                ((((ward_used[y+1][x] && ward_used[y-1][x]) && (!ward_used[y][x+1]) && x ==  9)) && ward_used[y][x-1]) ||   /* all sides used & cant't go right */
                ((((ward_used[y+1][x] && ward_used[y-1][x]) && ward_used[y][x+1]) && (!ward_used[y][x-1]) && x ==  0))) {  /* all sides used & can't go left */
            printf("\nDude, no more spacing available. Bailing out now!!!!\n\n");
            break; /* end loop. still print ward */
        }
    } /* end for loop */
}

void print_array(char walk[WARD_SIZE][WARD_SIZE])
{
    int row, column;

    for (row = 0; row < WARD_SIZE; row++) {
        for (column = 0; column < WARD_SIZE; column++) {
            printf(" %c", ward[row][column]);
        }
        printf("\n");
    }
}
