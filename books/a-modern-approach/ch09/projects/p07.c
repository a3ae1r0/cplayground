/******************************************************************************
 *  Improve power program from Section 9.6. Write a recursive function that
 *  computes x^n:
 *  - if n is even
 *    x^n = x^((n/2)^2)
 *  - if n is odd
 *    x ^ n = x * x^n-1
 *  - recursion ends when n = 0, in which case the function returns 1
 *
 *  - Ask the user values for x and n
 *  - call functin poew to compute x^n
 *  - display returned value
 ******************************************************************************/
#include <stdio.h>

int power(int x, int n, int odd_or_even);

int main(void)
{
    int x, n;

    printf("Insert value of 'x': ");
    scanf("%d", &x);
    printf("Insert value of 'n': ");
    scanf("%d", &n);

    printf("The result of %d ^ %d :: %d\n", x, n, power(x, n, n));
    return 0;
}

int power(int x, int n, int odd_or_even)
{
    if (odd_or_even % 2 == 0) {
        if (n == 0)
            return 1;
        else {
            /* x^n = (x^n/2)^2 <=> x^n = x^(n/2 * 2) <=> x^n = x^n */
            return power(x, (n/2)*2, odd_or_even);
        }
    } else {
        if (n == 0)
            return 1;
        else
            return x * power(x, n - 1, odd_or_even);
    }
}

/*
 * A correct solution from @github.com/fordea.
 * Where:
 * - Simplifies the condition when n == 0
 * - Implements the right function - that I was thinking, but didn't do it
 *   correctly - for when n is even
 * 
 * int power(int x, int n)
 * {
 *     if (n == 0) return 1;
 * 
 *     if (n % 2 == 0) {
 *         int i = power(x, n / 2);
 *         return i * i;
 *     } else {
 *         return x * power(x, n - 1);
 *     }
 * }
 */
