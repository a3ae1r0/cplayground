/***********************************************************************
 * Modify Programming Project 5 from Chapter 5:
 * - uses a funtion to compute the amount of income tax
 * - args: amount taxable income
 * - return: tax due
 ***********************************************************************/
#include <stdio.h>

float calc_tax(float taxable_inc);

int main(void)
{
    float income;

    printf("Enter the amount of taxable income: ");
    scanf("%f", &income);

    printf("Tax due: %.2f %\n", calc_tax(income));
    return 0;
}

float calc_tax(float taxable_inc)
{
    float tax;

    if (taxable_inc < 750.00f)
        tax = taxable_inc * 0.01f;
    else if (taxable_inc < 2250.00f)
        tax = ((taxable_inc - 750) * 0.02f) + 7.50f;
    else if (taxable_inc < 3750.00f)
        tax = ((taxable_inc - 2250)* 0.03f) + 37.50f;
    else if (taxable_inc < 5250.00f)
        tax = ((taxable_inc - 3750) * 0.04f) + 82.50f;
    else if (taxable_inc < 7000.00f)
        tax = ((taxable_inc - 5250) * 0.05f) + 142.50f;
    else
        tax = ((taxable_inc - 7000) * 0.06f) + 230.00f;

    return tax;
}
