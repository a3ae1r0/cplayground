/******************************************************************************
 * Write a function that computes the value of the polynomial:
 * 3x^5 + 2x^4 - 5x^3 - x^2 + 7x - 6
 *
 * How?
 * - program ask the user to enter a value for x
 * - then calls a function to compute the value fo the polynomial
 * - and then display the value return by the function
 ******************************************************************************/
#include <stdio.h>
#include <math.h>

int cal_polynomial(int x);

int main(void)
{
    int x;

    printf("Enter the value of 'x': ");
    scanf("%d", &x);

    printf("Result of the polynomial :: %d\n", cal_polynomial(x));
    return 0;
}

int cal_polynomial(int x)
{
    return (3 * pow(x,5) + 2 * pow(x,4) - 5 * pow(x,3) - pow(x,2) + 7 * x - 6);
}
