/***********************************************************************
 * Modify Programming Project 16 from Chapter 8:
 * - add functions:
 *   . void read_word(int counts[26]);
 *     . read word form user input, and return to main
 *   . bool equal_array(int counts1[26], int counts2[26]):
 *     . compare the two arrays. return true (anagram) or false
 *
 * - functions calls:
 *   . main call read_word (2x)
 *     . main have 2 arrays. Update each one with function return value
 *   . main call equal_array
 *     . printf if is or not a anagram with function return false
 **********************************************************************/
#include <stdio.h>
#include <ctype.h>
#include <stdbool.h> 

#define ALPHABET_SIZE 26
#define CHAR_A_DECIMAL 97

void read_word(int counts[ALPHABET_SIZE]);
bool equal_array(int counts1[ALPHABET_SIZE], int counts2[ALPHABET_SIZE]);

int main(void)
{
    int counts1[ALPHABET_SIZE] = {0}, counts2[ALPHABET_SIZE] = {0};

    printf("Enter first word: ");
    read_word(counts1);
    printf("Enter second word: ");
    read_word(counts2);

    if ((equal_array(counts1, counts2)) == true)
        printf("The words are anagrams.\n");
    else 
        printf("The words are not anagrams.\n");

    return 0;
}

void read_word(int counts[ALPHABET_SIZE])
{
    char ch;

    while ( (ch = tolower(getchar())) != '\n') {
        if (isalpha(ch))
            counts[ch - CHAR_A_DECIMAL] += 1;
    }
}

bool equal_array(int counts1[ALPHABET_SIZE], int counts2[ALPHABET_SIZE])
{
    int i, j, anagram = 0;

    for (i = 0, j = 0; i < ALPHABET_SIZE; i++, j++) {
            if (counts1[i] - counts2[j] == 0)
                anagram += 1;
    }
    
    if (anagram == ALPHABET_SIZE)
        return true;
    return false;
}
