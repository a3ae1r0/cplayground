/******************************************************************************
 * Modify Programming Project 17 from Chapter 8:
 * - add functions:
 *   . void create_magic_square(int n, char magic_square[n][n]);
 *     . fill the array with numbers 1, 2, ...., n^2
 *   . void print_magic_square(int n, char magic_square[n][n]);
 *     . print array
 *
 * - functions calls:
 *   . main call create_magic_square
 *     . main get n from user, and pass it (n x n) to function.
 *   . main call print_magic_square
 *     . call function to print array/square
 *
 * NOTE:
 * - the compiler (GCC) allows variable length array, but C89/C90 don't. In this
 *   project, we (me) are complying with this standard. So we use '99' as length
 *   (like p17@ch08).
 ******************************************************************************/
#include <stdio.h>
#include <stdbool.h>

#define MAX_ARRAY_LENGTH 99

void create_magic_square(int n, char magic_square[MAX_ARRAY_LENGTH][MAX_ARRAY_LENGTH]);
void print_magic_square(int n, char magic_square[MAX_ARRAY_LENGTH][MAX_ARRAY_LENGTH]);

int main (void)
{
    int n;
    char square[MAX_ARRAY_LENGTH][MAX_ARRAY_LENGTH] = {0};

    printf("This program creates a magic square of specified size.\n"
           "The size must be an odd number between 1 and 99.\n"
           "Enter size of magic number: ");
    scanf("%d", &n);

    create_magic_square(n, square);
    print_magic_square(n, square);
    return 0;
}

void create_magic_square(int n, char magic_square[MAX_ARRAY_LENGTH][MAX_ARRAY_LENGTH])
{
    bool square_used[MAX_ARRAY_LENGTH][MAX_ARRAY_LENGTH] = {false};
    int i, row, column, row_og, column_og;

    /* starting point */
    row = 0, column = (n / 2);
    magic_square[row][column] = 1;
    square_used[row][column] = true;

    for (i = 2; i <= n*n; i++) {
        /* save values b4 change them. usefull when we need 
         * to restore column, and go row -1 */
        row_og = row;
        column_og= column;

        /* now start with movement */
        row--;
        column++;

        /************************** 
         * row bound limit check
         *************************/
        /* when incrementing x + 1 */
        if (row < 0)            
            row = (n - 1);

        /* column bound limit check */
        if (column > (n - 1))
            column = 0;

        /************************** 
         * fill magic square
         *************************/
        if (!square_used[row][column]);
        else {
            column = column_og;         /* revert to previoustly value */
            row = row_og + 1;           /* go down by 1 */

            /* when puting a new number below, if it is last row, 
             * go back to top */
            if ( row > (n - 1)) { 
                row = row;
                row = 0;
            }
        }
       magic_square[row][column] = i;
       square_used[row][column] = true;
    }
}

void print_magic_square(int n, char magic_square[MAX_ARRAY_LENGTH][MAX_ARRAY_LENGTH])
{
    int i, j;

    /* show magic */
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("%d \t", magic_square[i][j]);
        }
        printf("\n");
    }
}
