#include <stdio.h>

#define ARRAY_MAXSIZE 20
/*
 * two length parameters:
 * - tmp_length: working value. Used in recursion (decrement)
 * - original_length: original size of array. Used in the final print
 */
void selection_sort(int a[], int tmp_length, int original_length);

int main(void)
{
    int i, length, array[ARRAY_MAXSIZE];

    printf("How many integers (length): ");
    scanf("%d", &length);

    printf("Enter a array of integers: ");
    for (i = 0; i < length; i++)
        scanf("%d", &array[i]);

    selection_sort(array, length, length);
    return 0;
}

void selection_sort(int a[], int tmp_length, int original_length)
{
    int i, largest = 0, temp = 0, temp_i = 0;

    if (tmp_length != 0) {
        for (i = 0; i < tmp_length; i++) {
            if (a[i] > largest) {
                largest = a[i];
                /* save element original position to allow the swap */
                temp_i = i;
            }
         }
        
        /* swap values */ 
        temp = a[tmp_length-1];         /* save value of last element */
        a[tmp_length-1] = largest;      /* move largest to last position */
        a[temp_i] = temp;               /* restore previous last element */

        /* recursive call to keep sorting array  */
        selection_sort(a, tmp_length-1, original_length);
     } else {
         for (i = 0; i < original_length; i++)
             printf("%d ", a[i]);
         printf("\n");
    }
}
