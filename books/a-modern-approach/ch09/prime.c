/* my failed attempt (bad) */
#include <stdio.h>
#include <math.h>

int is_prime (int n);

int main(void)
{
    int n;

    printf("Enter a number: ");
    scanf("%d", &n);

    printf("%d", is_prime(n));
    
    return 0;
}

int is_prime(int n)
{
    int i, prime = 0;

    for(i = 2; i <= sqrt(n); i++){
        prime = ((n / i) == 0) ? 0 : 1;
    }

    return prime;
}
