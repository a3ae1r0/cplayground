/***********************************************************************
 * Rewrite fact functin so that it's no longer recursive
 *
 * original fact:
 * int fact(int n)
 * {
 *     if (n <= 1)
 *         return 1;
 *     else
 *         return n * fact(n - 1);
 * }
 **********************************************************************/
#include <stdio.h>

int fact(int n);

int main(void)
{
    int n;
    scanf("%d", &n);
    printf("Factorial of %d is %d\n", n, fact(n));
    return 0;
}

int fact(int n)
{
    int i, fact;
    for (i = 1, fact = 1; i <= n; fact *= i, i++);
    return fact;
}
