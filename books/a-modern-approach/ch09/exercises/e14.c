/*
 * Find and fix the function 'has_zero' error
 */
#include <stdio.h>
#include <stdbool.h>

bool has_zero(int a[], int n);

int main(void)
{
    int length = 9, array[9] = {1, 1, 3, 3, 5, 6, 7, 8, 0};
    ...
}

bool has_zero(int a[], int n)
{
    int i;
    for (i = 0; i < n; i++)
        if (a[i] == 0)
            return true;
/*        else
            return false;
*/
    return false;       /* fix #e14*/
}


/*
 * Answer:
 * The program should only return false (default value) in the end
 * of the function, if this does not happen, the loop will not find 
 * zeros after the first element.
 *
 */
