/***********************************************************************
 * Condense the fact function the same way of power function
 *
 * original fact:
 * int fact(int n)
 * {
 *     if (n <= 1)
 *         return 1;
 *     else
 *         return n * fact(n - 1);
 * }
 *
 * condensed power:
 *
 * int power(int x, int n)
 * {
 *     return n == 0 ? 1 : x * power(x, n - 1);
 * }
 **********************************************************************/

...

int fact(int n)
{
    return n <= 1 ? 1 : n * fact(n - 1);
}
