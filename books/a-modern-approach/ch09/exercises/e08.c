/*
 * A valid prototype for a function that returns nothing and has one 
 * double parameter:
 * (a) void f(double x);
 * (b) void f(double);
 * (c) void f(x);
 * (d) f(double x);
 *
 * Answer:
 * (a) and (b) are valid. (c) and (d) not.
 * - The data type (double) must be specify (c).
 * - The return type is required (d)
 * - Parameter name and void is nice to have, but is not required
 *
 *
 * (c) output:
 * e08.c:18:1: warning: parameter names (without types) in function declaration
 *    18 | void f(x);
 *       | ^~~~
 *
 * (d) output
 * e08.c:25:1: warning: data definition has no type or storage class
 *    25 | f(double x);
 *       | ^
 * e08.c:25:1: warning: type defaults to ‘int’ in declaration of ‘f’ [-Wimplicit-int]
 * e08.c:35:1: warning: return type defaults to ‘int’ [-Wimplicit-int]
 *    35 | f(double x)
 *       | ^
 * 
 */
#include <stdio.h>

void f(double x);
/*
void f(double);
void f(x);
f(double x);
*/

int main(void)
{
    f(3.3445);

    return 0;
}

void f(double x)
{
    printf("Value of x: %lf\n", x);
}
