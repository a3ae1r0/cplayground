/* 
 * (...) For example, digit(829, 1) returns 9, digit(829, 2) returns 2,
 * and digit(829, 3) returns 8.
 * if k > number of digits in n, return 0 (?? typo? is not "return 1"?)
 */
#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE ((int) (sizeof(n) / sizeof(n[0])))

int digit(int n[], int k, int len);

int main(void)
{
    int n[], k;
    
    ...

    digit(n, k, ARRAY_SIZE);

    return 0;
}

int digit(int n[], int k, int len)
{
    int i, digit = (n[len - k]);

    if ( digit <= len) {
        /* multiline printf a more verbose output */
        printf("The %dth digit of the number ", k); 
        for(i = 0; i < len; i++)
            printf("%d", n[i]);
        printf(" is %d.\n", digit );

        return 0;
    } else {
        printf("\nThe digit you want is bigger than the actual number. "
               "Try running the program with other(s) value(s).\n");

        exit(EXIT_FAILURE);
    }
}
