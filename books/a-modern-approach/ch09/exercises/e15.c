/***********************************************************************
 * Rewrite function to use just one return statement:
 *
 * if (x <= y)
 *     if (y <= z) return y;
 *     else if (x <= z) return z;
 *     else return x;
 * if (z <= y) return y;
 * if (x <= z) return x;
 * return z;
 **********************************************************************/
#include <stdio.h>
double median(double x, double y, double z);

int main(void)
{
    ...
}

double median(double x, double y, double z)
{
    double median = z;

    if (x <= y){
        if (y <= z)
            median = y;
        else if (x >= z)
            median = x;
    } else if (z <= y)
        median = y;
    else if (x <= z)
        median = x;

    return median;
}


/*
 * Author answer:
 *
 * double median(double x, double y, double z)
 * {
 *   double result;
 * 
 *   if (x <= y)
 *     if (y <= z) result = y;
 *     else if (x <= z) result = z;
 *     else result = x;
 *   else {
 *     if (z <= y) result = y;
 *     else if (x <= z) result = x;
 *     else result = z;
 *   }
 * 
 *   return result;
 * }
 *
 * source: http://knking.com/books/c2/answers/c9.html
 */
