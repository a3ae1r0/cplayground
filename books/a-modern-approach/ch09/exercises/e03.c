#include <stdio.h>

int gcd(int a, int b);

int main()
{
    ...
}

int gcd(int a, int b)
{
    int q = 0, r = 0;

    while (b != 0) {
         q = a / b;
         r = a % b;
         a = b;
         b = r;
    }

    return a;
}
