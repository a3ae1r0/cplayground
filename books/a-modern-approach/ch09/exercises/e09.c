/* 
 * what will be the output of the following program? (09)?
 *
 * answer:
 * i == 1
 * j == 2
 *
 * Because:
 * - swap function has the type return type "void", so this
 * function don't return the new values in the end.
 * - (in addition) the printf is printing i and j after the swap 
 *   function call and the new value were not saved, so the printf 
 *   will print the values assign in the beginning of main
 */

#include <stdio.h>

void swap(int a, int b);

int main(void)
{
    int i = 1, j = 2;

    swap(i, j);
    printf("i = %d, j = %d\n", i, j);
    return 0;
}

void swap(int a, int b)
{
    int temp = a;
    a = b;
    b = temp;
}

/*
 * OUTPUT:
 * $ gcc e09.c && ./a.out 
 * i = 1, j = 2
 */
