/***********************************************************************
 * Write a recursive version of the gcd function (exercise 3 [e03.c])
 *
 * Tip:
 * - If n is not 0, return m
 *   - otherwise call gcc recursively, passing n as 1st arg and m % n as
 *   2nd arg
 **********************************************************************/
#include <stdio.h>

int gcd(int a, int b);

int main()
{
    int n1, n2;
    printf("Enter n1: ");
    scanf("%d", &n1);
    printf("Enter n2: ");
    scanf("%d", &n2);

    printf("The GCD of %d and %d is %d\n", n1, n2, gcd(n1, n2));
    return 0;
}

int gcd(int a, int b)
{
    if (b != 0)
        gcd(b, a % b);
    else
        return a;
}
