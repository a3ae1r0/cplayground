/*
 * function f:
 * int f(int a, int b) { ... }
 *
 * Which statements are legal? (i --> int, x --> double)
 * (a) i = f(83, 12);
 * (b) x = f(83, 12);
 * (c) i = f(3.15, 9.28);
 * (d) x = f(3.15, 9.28);
 * (d) f(83, 12);
 *
 * ---
 * Answer:
 * All...?
 * The compiler performs casting.
 * 
 */
#include <stdio.h>

int f(int a, int b)
{
    printf("This %d and this %d.\n", a, b);
    return 0;
}

int main(void)
{
    int i;
    double x;

    i = f(83, 12);
    x = f(83, 12);
    i = f(3.15, 9.28);
    x = f(3.15, 9.28);
    f(83, 12);

    return 0;
}

/*
 * Output: 
 * This 83 and this 12.
 * This 83 and this 12.
 * This 3 and this 9.
 * This 3 and this 9.
 * This 83 and this 12.
 */
