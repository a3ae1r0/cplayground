/*
 * Write function:
 * float compute_GPA(char grades[], int n);
 *
 * Notes:
 * - "grades" array --> A, B, C D or F (upper-case or lower-case)
 * - n is the lenght of the array
 * - function return --> average of grades
 * - values: A = 4, B = 3, C = 2, D = 1, and F = 0
 */

#include <stdio.h>
#include <ctype.h>

#define N_GRADES 5
#define MAX_STUDENT_GRADES 10

float compute_GPA(char grades[], int n);

int main(void)
{
    char ch, student_grades[MAX_STUDENT_GRADES];
    int i, length = 0;

    printf("Enter number of grades: ");
    scanf("%d", &length);
    printf("Enter grades (separated by a space): ");
    for (i = 0; i < length; i++) {
        /* in two steps to allow to already convert to upper-case */
        scanf(" %c", &ch);
        student_grades[i] = toupper(ch);
    }

    printf("GPA is: %.2f\n", compute_GPA(student_grades, length));

    return 0;
}

float compute_GPA(char grades[], int n)
{
    /* grades decimal value */
    int i, grades_value[N_GRADES] = {4, 3, 2, 1, 0};
    float gpa = 0.00;

    for (i = 0; i < n; i++) {
        /* exception bc we don't have a 'E'. Remove -1 to 'F' index */
        if ( (grades[i] % 65) == 5) {
            gpa += grades_value[grades[i] % 65 - 1];
            continue;
        }
        /* take advantage the compiler treats char as int */
        gpa += grades_value[grades[i] % 65];
    }

    return gpa / n;
}
