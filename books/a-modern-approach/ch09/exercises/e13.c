/*
 * chess material points
 *
 * function:
 * int evaluate_position(char[8][8)
 *
 * pieces:
 * - upper-case --> white
 * - lower-case --> black
 *
 * function return:
 * - value of white and black pieces
 * - difference in material w/ evaluation
 */
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>

#define BOARD_DIMENSIONS 8

void print_board(void);
void get_pieces(void);
int evaluate_position(char board[BOARD_DIMENSIONS][BOARD_DIMENSIONS]);
int get_points(int piece);

int main(void)
{
    print_board();

    printf("\nEnter both players pieces in chess notation "
           "separated by spaces.\n"
           "(E.g Ke5 for white, and Bc4 for black). "
           "Enter 0 to quit:\n");
    get_pieces();       /* function get/save both players pieces */
    return 0;
}

void print_board(void)
{
    printf("\n     Board coordinates\n");
    printf("1[ ][ ][ ][ ][ ][ ][ ][ ]\n");
    printf("2[ ][ ][ ][ ][ ][ ][ ][ ]\n");
    printf("3[ ][ ][ ][ ][ ][ ][ ][ ]\n");
    printf("4[ ][ ][ ][ ][ ][ ][ ][ ]\n");
    printf("5[ ][ ][ ][ ][ ][ ][ ][ ]\n");
    printf("6[ ][ ][ ][ ][ ][ ][ ][ ]\n");
    printf("7[ ][ ][ ][ ][ ][ ][ ][ ]\n");
    printf("8[ ][ ][ ][ ][ ][ ][ ][ ]\n");
    printf("  a  b  c  d  e  f  g  h\n");
}

void get_pieces(void)
{
    char board[BOARD_DIMENSIONS][BOARD_DIMENSIONS] = {0};
    unsigned char file, piece;      /* allow use in array */
    int rank;

    for(;;){
        /* 1st input in a different scanf to allow user to quit */
        scanf(" %c", &piece);
        if (piece == '0')
            break;
        scanf(" %c %d", &file, &rank);

        /* handle 'file' input */
        file = tolower(file);       /* enforce lower case */
        file = file % 97;           /* fix int value of char (array) */

        /* save input to array */
        board[rank-1][file] = piece;
    }

    evaluate_position(board);
}

int evaluate_position(char board[BOARD_DIMENSIONS][BOARD_DIMENSIONS])
{
    int i, j, eval_white = 0, eval_black = 0;

    for (i = 0; i < BOARD_DIMENSIONS; i++) {
        for (j = 0; j < BOARD_DIMENSIONS; j++) {
            /* check first is square is in use */
            if(isupper(board[i][j]))
                eval_white += get_points(board[i][j]);
            else
                eval_black += get_points(board[i][j]);
        }
    }
    printf("White material evalation: %d\n", eval_white);
    printf("Black material evaluation: %d\n", eval_black);
    printf("Material difference --> %.2f\n", (float) (eval_white - eval_black));
    return 0;
}

int get_points(int piece)
{
    int points = 0;
    switch(piece) {
        case 'Q': case 'q':
            points = 9;
            break;
        case 'R': case 'r':
            points = 5;
            break;
        case 'B': case 'b': case 'N': case 'n':
            points = 3;
            break;
        case 'P': case 'p':
            points = 1;
            break;
    }

    return points;
}
