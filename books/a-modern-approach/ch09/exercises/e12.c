/* 
 * write function:
 * double inner_product (double a[], double b[], int n)
 *
 * function returns:
 * a[0] * b[0] + a[1] * b[1] + ... + a[n-1] * b[n-1]
 */
#include <stdio.h>

#define ARRAY_MAX_SIZE 10

double inner_product(double a[], double b[], int n);

int main(void)
{
    ... 

    printf("The inner product result = %.2f\n", inner_product(a, b, n));

    return 0;
}

double inner_product(double a[], double b[], int n)
{
    double result;
    int i;

    for (i = 0; i < n; i++)
        result += a[i] * b[i];

    return result;
}
