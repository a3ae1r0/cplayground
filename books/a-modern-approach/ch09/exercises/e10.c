/*
 * Write functions that return:
 * (a) The largest element in a
 * (b) The average of all elements in a
 * (c) The number of positive elements in a
 *
 * Assume:
 * - "a" and "n" are parametes
 * - a is an array of int
 * - b is the length of the array
 */

#include <stdio.h>

#define ...

int biggest(int a[], int n);
double avg(int a[], int n);
int positive(int a[], int n);

int main(void)
{
    ...
    
    printf("The largest element in the array: %d\n", biggest(b, n));
    printf("The average of all elements in the array: %.3g\n", avg(b, n));
    printf("The number of positive elements in the array: %d\n", positive(b, n));

    return 0;
}

int biggest(int a[], int n)
{
    int largest = 0, i;
    for (i = 0; i < n; i++)
        if(a[i] > largest)
            largest = a[i];

    return largest;
}

double avg(int a[], int n)
{
    double average = 0;
    int i;
    for (i = 0; i < n; i++)
        average += a[i];

    return average / n;
}

int positive(int a[], int n)
{
    int n_positive = 0, i;
    for (i = 0; i < n; i++)
        if(a[i] > 0)
            n_positive++;
        
    return n_positive;
}
