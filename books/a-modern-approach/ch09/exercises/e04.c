#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h> 

int day_of_year(int month, int day, int year);

int main (void)
{
    ...

    if (day_of_year(month,day,year) == 1) {
        printf("The year inserted is not a leap year. "
                "Please try again another date.\n");
        return 1;
    ...
}

int day_of_year(int month, int day, int year)
{
    int days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int i, daysYear = 0, leapYear = false;

    /* add days up to the previous month */
    for (i = 0; i < month - 1; i++) {
        daysYear += days[i];
    }

    /****************
    * Leap Year
    ****************/
    /* check if is leap year. Needs to fulfill these requirements: */
    if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))
        leapYear = true;

    /* next actions after check */
    if (leapYear == true) {
        if (month > 2)
            daysYear++;         /* add extra day */
    } else {
       /* is not a leap year and user entered 29/02??? */
       if (month == 2 && day == 29)
           return 1;            /* let main handle this */
    }
    
    return daysYear + day;
}

/*
 * Author' solution: http://knking.com/books/c2/answers/c9.html
 *
 * Relevant points:
 * - No array size 
 * - Different for loop to count days before the current month
 * - A clean leap year expression. It's the same, but simpler to read 
 *   (merged)
 */
