/* count number of digits in n (positve integer) */
#include <stdio.h>

int num_digits(long n);

int main(void)
{
    ...
}

int num_digits(long n)
{
    int count = 0;
    
    while (n != 0) {
        n = n / 10;
        count++;
    }

    return count;
}
