---
Title: C Programming - A Modern Approach, 2nd Edition
Type: Notes
Date: 2020-03-29
---

# Chapter 09 | Functions

## 9.3 | Arguments
### Variable-Length Array Parameters (C99)
* **Function definition**
  * The value of the first parameter (`n`) specifies the length of the second parameter (`a`).
  ```c
  int sum_array(int n, int a[n])
  {
    ...
  }
  ```

* **Parameter * notation**
  * Possible because **parameter names are optional** in function declarations
  * \* provides a clue that the length of the array is related to **parameters that come earlier** in the list
  ```c
  int sum_array(int n, int a[*]);
  OR
  int sum_array(int, int[*]);
  ```

* **Parameter with expressions**
  ```c
  int concatenate(int m, int n, int a[m], int b[n], int c[m+n])
  {
    ...
  }
  ```

### Using `static` in Array Paramater Declarations (C99)
* Using `static` in this way **has no effect** on the behavior of the program
* Main advantage: may allow the compiler to **generate faster instructions** when accessing the array
  * Since the size is the same, the compiler can arrange to prefetch these elements in memory before are actually needed
* On multi-dimensional arrays, `static`  can be used **only in the first dimension** (e.g. rows)
* Declaration example:
  ```c
  int sum_array(int a[static 3], int n)
  {
    ...
  }
  ```

### Compound Literals (C99)
* **Compound literal:** an unnamed array created "on the fly" where we specify which elements it contains
* Function call + compound literal:
```c
total = sum_array((int []){3, 0, 3, 4, 1}, 5);
```
* When not specify, the lenght of the array is determined by the **number of elements in the literal**
* Caracteristics:
  * **Arbitrary expressions**
  ```c
  total = sum_array((int []){2 * 1, i + j, j * k}, 3);
  ```
  * **"Read-only"**
  ```c
  total = sum_array((const int []){5, 4});
  ```

## 9.4 | The `return` Statement
* (Possible) Expressions:
```c
return 0;
return status;
return;    /* return in void fuction. OK, but not needed */
return n >= 0 ? n : 0;
```
* Return **type mismatch**
  * type of expression in **return statement** != **function's return** type --> expression **implicit converted to the return type**
    * Example:  
    ```c
    /*
     * function --> (declare to) return an int
     * return statement --> (contains) double expression
     *
     * (final) value of the expression --> (converted) int
     */
    ```
* If a non-void functions **fails to execute a return statement**, the behavior of the program is **undefined** (if it attempts to use the value return by the function)
```c
/* compiler possible warning */
control reaches end of non-void function
```

## 9.5 | Program Termination
* Define `main` 
```c
/* common */
int main(void)
{
  ...
}

/* older C programs. Taking advantage that main defaults to int 
 * ommiting the return type is ilegal in C99
 * 
 * Ommitting word void is a matter of style. But is best to explicit
 */
main()
{
  ...
}
```
* The value return by `main` is a status code that allows the OS to check if the program **terminated successfully (0)**

### The `exit` function
* Library `<stdlib.h>`
* Usage:
```c
exit(0)                   /* normal termination */
exit(EXIT_SUCCESS)        /* normal termination (0)*/
exit(EXIT_FAILURE)        /* abnormal termination (1) */
```
* `return` vs `exit`
  * `return`: termination of the program only when it appers in the `main` function
  * `exit`: termination of the program regardless of which function calls it
  * `exit` is common exclusively use, to make it easier to locate all exit points

## 9.6 | Recursion
* A function is **recursive** if it calls itself
  * Examples:
  ```c
  /* example 1 */
  int fact(int n)
  {
    if (n <= 1)
      return 1;
    else
      return n * fact(n -1);
  }

  /* example 2 */
  int power(int x, int n)
  {
    return n == 0 ? 1 : x * power(x, n - 1);
  }
  ```
* **Termination condition** is needed to prevent **infinite recursion**

### The Quicksort Algorithm
* **Divide-and-conquer**: algorithm technique/paradigm where a large problem is divided into smaller pieces that are then tackled by the same algorithm
* **Quicksort** algorithm is one classic examples
* See [`qsort.c`](qsort.c)

# Q & A
* K&R style for defining functions (*obsolescent*):
```c
double average(a, b)
double a, b;
{
  return (a + b) / 2;
}
```
* Function names not followed by parentheses
  * Compiler treats as a **pointer** to the function
```c
print_pun;
```
* Parameters names in a function prototype **don't have to match** names in the function definition
* Function declarations **w/o parameters information**
  * Empty parentheses **doesn't means the function has no parameters** (just omits this information)
  * Considered obsolescent, but still allowed
  ```c
  double average();        /* only informs the return type of function */
  ```
* Function declaration can be **inside the body** of another function
  * But only that function can call this inside function
  ```c
  int main(void)
  {
    double average(double a, double b);
    ...
  }
  ```
* Compiler **ignores specify length** for one-dimensional array parameter
  ```c
  double inner_product(doube v[3], double w[3]);
  ```
* Left unspecified the first dimensional in a array, and not the others
  * The calculation of **one-dimensional array**, **don't depend of the size** of the array
  * For **multidimensional array**, the compiler **needs the size of the row (column)** to calculate values

