/* Sorts an array of integers using Quicksort algorith */

#include <stdio.h>

#define N 10

void quicksort(int a[], int low, int high);
int split(int a[], int low, int high);

int main(void)
{
    int a[N], i;

    printf("Enter %d numbers to be sorted: ", N);
    for (i = 0; i < N; i++)
        scanf("%d", &a[i]);

    quicksort(a, 0, N - 1);

    for (i = 0; i < N; i++)
        printf("%d ", a[i]);
    printf("\n");

    return 0;
}

void quicksort(int a[], int low, int high)
{
    int middle;

    if (low >= high) return;
    middle = split(a, low, high);
    quicksort(a, low, middle - 1);
    quicksort(a, middle + 1, high);
}

int split(int a[], int low, int high)
{
    int part_element = a[low];

    for (;;) {
        while (low < high && part_element <= a[high])
            high--;
        if (low >= high) break;
        a[low++] = a[high];

        while (low < high && a[low] <= part_element)
            low++;
        if (low >= high) break;
        a[high--] = a[low];
    }

    a[high] = part_element;
    return high;
}

/*
 * Ways to improve program's performance (p208):
 *
 * - Improving the partitioning algorithm
 * Partitioning element could be the median of the 1st element, the middle
 * element, and the last element
 * Partitioning process could be sped up, in particular by removing the two 
 * low < high tests
 *
 * - Using a different method to sort small arrays
 *
 * - Making Quicksort nonrecursive
 * Although Quicksort is a recursive algorithm, it could be more efficient if
 * the recursion is removed
 */
