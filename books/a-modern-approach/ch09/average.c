#include <stdio.h>

float average (float x, float y);

int main (void)
{
    float n1, n2, n3;

    printf("Enter three numbers: ");
    scanf("%f %f %f", &n1, &n2, &n3);

    printf("Average of %g and %g: %g\n", n1, n2, average(n1, n2));
    printf("Average of %g and %g: %g\n", n2, n3, average(n2, n3));
    printf("Average of %g and %g: %g\n", n1, n3, average(n1, n3));

    return 0;
}

float average (float x, float y)
{
    return (x + y) / 2;
}
