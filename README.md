# C Playground

## What?
[**Warm-up:**](https://gitlab.com/a3ae1r0/cplayground/-/tree/master/warm-up) small exercises just to practice and test some code  
[**Projects:**](https://gitlab.com/a3ae1r0/cplayground/-/tree/master/projects) slightly more "complex" programs  
[**Books:**](https://gitlab.com/a3ae1r0/cplayground/-/tree/master/books) notes, exercises and projects from C books
