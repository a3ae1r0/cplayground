#include <stdio.h>
#include <stdlib.h>
#include "include/handlers.h"

/************************************************************
 * userMenu: print menu with options at user disposal
 ***********************************************************/
void userMenu(requirements passwdRequirements)
{
    printf("Welcome to The Password Manager\n\n");
    printf("(1) Password length       [%d]\n", passwdRequirements.length);
    printf("(2) Upper-case letters    [%s]\n",
           (passwdRequirements.uppercase ? "true" : "false"));
    printf("(3) Lower-case letters    [%s]\n",
           (passwdRequirements.lowercase ? "true" : "false"));
    printf("(4) Numeric numbers       [%s]\n",
           (passwdRequirements.numeric ? "true": "false"));
    printf("(5) Special characters    [%s]\n\n",
           (passwdRequirements.specialchars) ? "true" : "false");
    printf("(0) Generate password\n");
    printf("(9) Run this menu again\n");
    printf("(q) Quit\n");
}


/************************************************************
 * userOptions: get user input for options that the program
 *              will then act uppon - e.g. generate password
 ***********************************************************/
void userOptions(entry *en, requirements passwdRequirements)
{
    char option, *password = calloc(sizeof(char), passwdRequirements.length+1);

    userMenu(passwdRequirements);

    while (option != 'q') {
        printf("\nYour option: ");
        scanf(" %c", &option);

        switch(option) {
            case '1': printf("Insert password length: ");
                      scanf("%d", &passwdRequirements.length);
                      break;
            case '2': (passwdRequirements.uppercase == true)
                        ? (passwdRequirements.uppercase = false)
                        : (passwdRequirements.uppercase = true);
                      printf("Successfully toggled use of upper-case letters\n");
                      break;
            case '3': (passwdRequirements.lowercase == true)
                        ? (passwdRequirements.lowercase = false)
                        : (passwdRequirements.lowercase = true);
                      printf("Successfully toggled use of lower-case letters\n");
                      break;
            case '4': (passwdRequirements.numeric == true)
                        ? (passwdRequirements.numeric = false)
                        : (passwdRequirements.numeric = true);
                      printf("Successfully toggled use of numeric numbers\n");
                      break;
            case '5': (passwdRequirements.specialchars == true)
                        ? (passwdRequirements.specialchars = false)
                        : (passwdRequirements.specialchars = true);
                      printf("Sucessfully toggled use of special characters\n");
                      break;
            case '0': genPasswd(password, passwdRequirements);
                      en->password = password;
                      printf("Your password: %s\n", en->password);
                      break;
            case '9': userMenu(passwdRequirements);
                      break;
            case 'q': exit(0);
        }
    }

    free(password);
}
