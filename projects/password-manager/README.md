# The Password Manager
A very simple password manager

## Features
* Generate random password from user input requirements

## TODO
* Accept full entry from user (name, user, password, url, description)
* Write entries to file (plaintext)
* Add encryption to "db" file
* Application lock (master password to access?)
* Copy user/password to OS clipboard
