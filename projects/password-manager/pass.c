#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "include/handlers.h"

/************************************************************
 * genPasswd: generate password based on requirements from
 *            user input
 ***********************************************************/
void genPasswd(char *pass, requirements passRequirements)
{
    int arrReq[4], length = 0, randReq;
    char ch;

    strcpy(pass, "");

    /* Save requirements as array indexes rand operations */
    arrReq[0] = passRequirements.uppercase;
    arrReq[1] = passRequirements.lowercase;
    arrReq[2] = passRequirements.numeric;
    arrReq[3] = passRequirements.specialchars;

    srand((unsigned) time(NULL));

    while (length < passRequirements.length) {
        do {
            randReq = rand() % 4;
        } while (arrReq[randReq] != 1);

        switch (randReq) {
            case 0: ch = 'A' + (rand() % 26);
                    break;
            case 1: ch = 'a' + (rand() % 26);
                    break;
            case 2: ch = '0' + (rand() % 10);
                    break;
            case 3: ch = '!' + (rand() % 15);
                    break;
        }

        strncat(pass, &ch, 1);
        length++;
    }
}
