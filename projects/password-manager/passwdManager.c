/******************************************************************************
 * PASSWORD MANAGER
 *
 * A password manager open-source that is not in the "cloud"
 *
 * VERSION : v1.0
 * DATE    : 2021
 *****************************************************************************/
#include "include/handlers.h"

int main(void)
{
    requirements passwdRequirements;
    entry newEntry = { "", "", "", "" };

    /* Get password requirements from user input */
    userOptions(&newEntry, passwdRequirements);

    return 0;
}
