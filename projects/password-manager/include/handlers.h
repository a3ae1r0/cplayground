#include <stdbool.h>

/**
 ** Requirements for password creation
 **/
typedef struct {
    int length;
    bool uppercase;
    bool lowercase;
    bool numeric;
    bool specialchars;
} requirements;

/**
 ** New password entry
 **/
typedef struct {
    char *name;
    char *username;
    char *password;
    char *url;
} entry;

/**
 ** Menu and user input
 **/
void userMenu(requirements passwdRequirements);
void userOptions(entry *en, requirements passwdRequirements);

/**
 ** Password operations
 **/
void genPasswd(char *pass, requirements passRequirements);
