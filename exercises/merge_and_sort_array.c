/*
 * Exercise from https://www.w3resource.com/c-programming-exercises
 * /array/c-array-exercise-7.php 
 */
#include <stdio.h>
#include <stdbool.h>

#define ARRAY_MAXSIZE 30

int main (void)
{
    int array[ARRAY_MAXSIZE];
    bool keep_track[ARRAY_MAXSIZE] = {false};       /* array to remove already seen */
    int i, j, length, temp_length, largest = 0, largest_index = 0;

    printf("\nNumber of elements to be stored in the 1st array: ");
    scanf("%d", &length);
    printf("Input %d elements in the array: ", length);
    for (i = 0; i < length; i++)
        scanf("%d", &array[i]);

    printf("\nNumber of elements to be stored in the 2nd array: ");
    scanf("%d", &temp_length);
    printf("Input %d elements in the array: ", temp_length);
    for (i = 0; i < temp_length; i++)
        /* append values to 1st array */
        scanf("%d", &array[length + i]);

    length += temp_length;      /* append new values to length */

    /* 1st iteration makes sure we treat all digits (length) */
    i = length;
    while (i - 1 >= 0) {
        largest = 0;

        for (j = 0; j < length; j++) {
            if (keep_track[j] == false) {
                if (array[j] > largest) {
                    largest = array[j];
                    /* useful to saved real (final ) largest value */
                    largest_index = j;
                }
            }
        }
        printf("%d ", largest);
        /* remove element for next iterations */
        keep_track[largest_index] = true;
        i--;        /* one less digit to check if it is the largest */
    }
    printf("\n");

    return 0;
}
