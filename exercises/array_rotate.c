/*
 * Given an array arr[] of size N and D index, rotate the array by the D index.
 * arr[] = {1, 2, 3, 4, 5} 
 * D = 2 
 * Output: 
 * 3 4 5 1 2 
 */
#include <stdio.h>

#define ARRAY_MAXSIZE 20
#define ARR1_SIZE ((int) (sizeof(arr1) / sizeof(arr1[0])))
#define ARR2_SIZE ((int) (sizeof(arr2) / sizeof(arr2[0])))

int get_array(void);

int main(void)
{
   /* int arr[ARRAY_MAXSIZE] = 0;*/
    get_array();

    return 0;
}

int get_array(void)
{
    char ch = ' ', arr1[ARRAY_MAXSIZE] = {'0'}, arr2[ARRAY_MAXSIZE] = {'0'};
    int i=0;

    printf("Insert array n1: ");
    for (i = 0; ch != '\n'; i++) {
        scanf("%c", &ch);
        arr1[i] = ch;
    }

    printf("Insert array n2: ");
    for (i = 0; ch != '\n'; i++) {
        scanf("%c", &ch);
        arr2[i] = ch;
    }


    for (i = 0; i < sizeof(ARR1_SIZE); i++)
        printf("%c", arr1[i]);
    for (i = 0; i < sizeof(ARR2_SIZE); i++)
        printf("%c", arr2[i]);
    return 0;
}
