/******************************************************************************
 * Given the triangle of consecutive odd numbers:
 *              1
 *           3     5
 *        7     9    11
 *    13    15    17    19
 * 21    23    25    27    29
 * ...
 * 
 * Calculate the sum of the numbers in the nth row of this triangle (starting
 * at index 1) e.g.: (Input --> Output)
 * 1 -->  1
 * 2 --> 3 + 5 = 8
 *
 * Kata: https://www.codewars.com/kata/55fd2d567d94ac3bc9000064
 *****************************************************************************/
#include <inttypes.h>
#include <stdio.h>

uint64_t rowSumOddNumbers(uint32_t n);

int main (void)
{
    printf("%" PRIu64 "\n", rowSumOddNumbers(1));
    printf("%" PRIu64 "\n", rowSumOddNumbers(2));
    printf("%" PRIu64 "\n", rowSumOddNumbers(5));
    printf("%" PRIu64 "\n", rowSumOddNumbers(42));
    printf("%" PRIu64 "\n", rowSumOddNumbers(374451));
    return 0;
}

/***********************************************************
 * "Custom" algorithm:                                     *
 * 1/ Find 1st number of the nth row (using a pattern)     *
 * 2/ Sum n odd numbers that are on the row                *
 * Alternatively, you can use (found by trial and error...)*
 * return (n * n * n );                                    *
 **********************************************************/
uint64_t rowSumOddNumbers(uint32_t n)
{
    uint64_t i, j, current_number = 2*(n-1), sum = 1;

    for (i = 0; i < n; i++, current_number += 2) {
        /* First element/number of the row */
        if (i == 0) {
            for (j = 0; j < n-1; j++, current_number -= 2)
                sum += current_number;
            current_number = sum;           /* Save current row number */
        } else {
            /* Sum the remaining row elements (odd numbers) */
            sum += current_number;
        }
    }

    return sum;
}
