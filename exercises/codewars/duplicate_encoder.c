/******************************************************************************
 * The goal of this exercise is to convert a string to a new string where each
 * character in the new string is "(" if that character appears only once in
 * the original string, or ")" if that character appears more than once in the
 * original string. Ignore capitalization when determining if a character is a
 * duplicate.
 *
 * Examples
 * "din"      =>  "((("
 * "recede"   =>  "()()()"
 * "Success"  =>  ")())())"
 * "(( @"     =>  "))((" 
 *
 * Notes
 * Assertion messages may be unclear about what they display in some languages.
 * If you read "...It Should encode XXX", the "XXX" is the expected result,
 * not the input!
 *
 * Kata: 54b42f9314d9229fd6000d9c
 *****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

char *DuplicateEncoder(char *string);

int main(void)
{
    /* Sample Tests */
    printf("%s\n", DuplicateEncoder("recede"));
    printf("%s\n", DuplicateEncoder("Prespecialized"));
    printf("%s\n", DuplicateEncoder("(( @"));
    printf("%s\n", DuplicateEncoder("   ()(   "));
    printf("%s\n", DuplicateEncoder("Xe8Xd&+Q2b"));
    return 0;
}

char *DuplicateEncoder(char *string)
{
    int i, ascii_values[256] = {0}, str_len = strlen(string);
    char *str_return = calloc(str_len + 1, sizeof(char));

    /* Count number of appearences into array */
    for (i = 0; i < str_len; i++)
        ascii_values[toupper(string[i])]++;

    /* Create new string */
    for (i = 0; i < str_len; i++) {
        if (ascii_values[toupper(string[i])] == 1)
            str_return[i] = '(';
        else
            str_return[i] = ')';
    }

    return str_return;
}
