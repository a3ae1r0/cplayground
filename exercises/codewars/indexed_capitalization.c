/******************************************************************************
 * Given a string and an array of integers representing indices, capitalize all
 * letters at the given indices.
 *
 * For example:
 *   capitalize("abcdef",[1,2,5]) = "aBCdeF"
 *    capitalize("abcdef",[1,2,5,100]) = "aBCdeF". There is no index 100.
 *
 * The input will be a lowercase string with no spaces and an array of digits.
 *
 * Kata: https://www.codewars.com/kata/59cfc09a86a6fdf6df0000f1
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>           /* Answer required */
#include <string.h>           /* Answer required */
#include <ctype.h>            /* Answer required */

char *capitalize(const char *str_in, size_t z, size_t indices[z]);

int main(void)
{
    /* const char *string = "abcdef"; */
    /* size_t indices[3] = {1, 2, 5}; */
    /* printf("%s\n", capitalize(string, 3, indices)); */

    /* const char *string = "abcdef"; */
    /* size_t indices[4] = {1, 2, 5, 100}; */
    /* printf("%s\n", capitalize(string, 4, indices)); */

    const char *string = "codewars";
    size_t indices[4] = {1, 3, 5, 50};
    printf("%s\n", capitalize(string, 4, indices));

    /* const char *string = "aBXKpMlvCQBW"; */
    /* size_t indices[2] = {10, 36}; */
    /* printf("%s\n", capitalize(string, 2, indices));      /1* aBXKPMlvCQBW *1/ */

    return 0;
}

char *capitalize(const char *str_in, size_t z, size_t indices[z])
{
    size_t len = strlen(str_in);
    char *str_out = malloc(sizeof(char) * len + 1);

    strcpy(str_out, str_in);
    for (size_t i = 0; i < z; i++)
        /* Index checking required to avoid getting wrong expected test values,
         * because of the use of toupper() and memory violation */
        if (indices[i] < len)
            str_out[indices[i]] = toupper(str_out[indices[i]]);

    return str_out;
}
