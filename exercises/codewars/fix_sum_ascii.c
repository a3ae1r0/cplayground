/******************************************************************************
 * The given function sum_ascii is supposed to find the sum of ASCII values of
 * all characters in the given string.
 * However, it has a very subtle problem in the code, so it does not pass the
 * tests.
 *
 * Analyze the code carefully, and fix it to pass all tests.
 *
 * Kata: 59cf03496bddd2aa17000015
 *****************************************************************************/
#include <stdio.h>

/*** SAMPLE TESTS ***/
char str1[] = "X";

/*** PROTOTYPES***/
int sum_ascii(char string[]);

int main(void)
{
    printf("%d\n", sum_ascii(str1));
    return 0;
}

/*** SOLUTION START ***/
int sum_ascii(char string[])
{
    int sum = 0, i;

    /* Problem: When passing an array to a function, this function receives a
     *          pointer to the value, so the knowledge of the value of the
     *          sizeof is lost.
     *          Two option: (i) calculate sizeof before the function call, and
     *          pass it as an additional argument. Or (ii) use the conditions
     *          below in the for loop, to stop when reaching the end of the
     *          array.
     * Fix: fix_sum_ascii.c:14:28: warning: ‘sizeof’ on array function parameter
     *    ‘string’ will return size of ‘char *’ [-Wsizeof-array-argument] */
    /* for (int i = 0; i < sizeof(string) / sizeof(string[0]); ++i) */
    for (i = 0; string[i] != '\0'; ++i)
    {
        sum += string[i];
    }
    return sum;
}
