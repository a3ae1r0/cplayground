/******************************************************************************
 * Compare two strings by comparing the sum of their values (ASCII character
 * code).
 *   For comparing treat all letters as UpperCase
 *   null/NULL/Nil/None should be treated as empty strings
 *   If the string contains other characters than letters, treat the whole
 * string as it would be empty
 *
 * Your method should return true, if the strings are equal and false if they
 * are not equal.
 *
 * Examples:
 * "AD", "BC"  -> equal
 * "AD", "DD"  -> not equal
 * "gf", "FG"  -> equal
 * "zz1", ""   -> equal (both are considered empty)
 * "ZzZz", "ffPFF" -> equal
 * "kl", "lz"  -> not equal
 * null, ""    -> equal
 *
 * Kata: https://www.codewars.com/kata/576bb3c4b1abc497ec000065
 *****************************************************************************/
#include <stdio.h>
/* Required for answer */
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

int calc_values(const char *s);
bool compare(const char *s1, const char *s2);

int main(void)
{
    printf("%s\n", compare("AD",   "BC") ? "true" : "false");       /*  true */
    printf("%s\n", compare("AD",   "DD") ? "true" : "false");       /* false */
    printf("%s\n", compare("gf",   "FG") ? "true" : "false");       /*  true */
    printf("%s\n", compare("Ad",   "DD") ? "true" : "false");       /* false */
    printf("%s\n", compare("zz1",  "") ? "true" : "false");         /*  true */
    printf("%s\n", compare("ZzZz", "ffPFF") ? "true" : "false");    /*  true */
    printf("%s\n", compare("kl",   "lz") ? "true" : "false");       /* false */
    printf("%s\n", compare(NULL,   "") ? "true" : "false");         /*  true */
    printf("%s\n", compare(NULL,   "ABC") ? "true" : "false");      /* false */
    printf("%s\n", compare("",     "ABC") ? "true" : "false");      /* false */
    printf("%s\n", compare("!!",   "7476") ? "true" : "false");     /*  true */
    printf("%s\n", compare("##",   "1176") ? "true" : "false");     /*  true */

    return 0;
}

int calc_values(const char *s)
{
    size_t i, len_str;
    int sum_str = 0;

    if (s == NULL)
        return 0;

    for (i = 0, len_str = strlen(s); i < len_str; i++) {
        if (isalpha(s[i]) == 0)
            return 0;
        sum_str += toupper(s[i]);
    }

    return sum_str;
}

bool compare(const char *s1, const char *s2)
{
    return calc_values(s1) == calc_values(s2);
}
