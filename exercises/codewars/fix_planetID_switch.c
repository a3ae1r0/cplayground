/*
 * Caution: This kata does not currently have any known supported versions for
 * C.
 * It may not be completable due to dependencies on out-dated
 * libraries/language versions.
 *
 * The function is not returning the correct values. Can you figure out why?
 *
 * Example (Input --> Output):
 * 3 --> "Earth"
 *
 * Kata: 515e188a311df01cba000003
 */
#include <stdio.h>

const char * getPlanetName(int id);

int main(void)
{
    /* FIX: Wrong value returned: 3 --> Neptune */
    printf("3 --> %s\n", getPlanetName(3));
    printf("7 --> %s\n", getPlanetName(7));

    return 0;
}


/*** SOLUTION GOES HERE ***/
const char * getPlanetName(int id)
{
    const char *name = "";          /* Bonus: Add var init (C89/C90) */
    switch (id)
    {
    case 1: name = "Mercury";
            break;
    case 2: name = "Venus";
            break;
    case 3: name = "Earth";
            break;
    case 4: name = "Mars";
            break;
    case 5: name = "Jupiter";
            break;
    case 6: name = "Saturn";
            break;
    case 7: name = "Uranus";
            break;
    case 8: name = "Neptune";
            break;
    }
    return name;
}
