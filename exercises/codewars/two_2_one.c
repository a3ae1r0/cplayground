/******************************************************************************
 * Take 2 strings s1 and s2 including only letters from a to z.
 * Return a new sorted string, the longest possible, containing distinct letters
 * - each taken only once - coming from s1 or s2.
 *
 * Examples:
 * a = "xyaabbbccccdefww"
 * b = "xxxxyyyyabklmopq"
 * longest(a, b) -> "abcdefklmopqwxy"
 *
 * a = "abcdefghijklmnopqrstuvwxyz"
 * longest(a, a) -> "abcdefghijklmnopqrstuvwxyz"
 *
 * Kata: 5656b6906de340bd1b0000ac
 *****************************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

/*** PROTOTYPES ***/
char* longest(char* s1, char* s2);

/*** MACROS ***/
#define ALPHABET_SIZE 26

int main(void)
{
    /*** SAMPLE TESTS ***/
    char a0[] = "xyaabbbccccdefww";
    char b0[] = "xxxxyyyyabklmopq";
    char a1[] = "abcdefghijklmnopqrstuvwxyz";
    char a2[] = "mmzzyyioll";
    char b2[] = "nnhhwii";

    printf("%s\n", longest(a0, b0));
    printf("%s\n", longest(a1, a1));
    printf("%s\n", longest(a2, b2));

    return 0;
}

char* longest(char* s1, char* s2)
{
    int ch, char_exist[ALPHABET_SIZE] = {0};
    char *str_return = calloc(sizeof(char), ALPHABET_SIZE + 1), letter;

    /* set char to true if exist in the string s1 */
    for (ch = 0; s1[ch] != '\0'; ++ch)
        char_exist[ (s1[ch] % 'a') ] += 1;

    /* now set true chars that exist in s2 */
    for (ch = 0; s2[ch] != '\0'; ++ch)
        /* char_exist[ (s2[ch] % 'a') ] = true; */
        char_exist[ (s2[ch] % 'a') ] += 1;

    /* print chars that have a true value */
    for (ch = 0, letter = 'a'; ch < ALPHABET_SIZE; ch++, letter++) {
        /* if (char_exist[ch] == true) */
        if (char_exist[ch] >= 1)
            strncat(str_return, &letter, 1);   /* Append alphabet letter to str */
    }

    return str_return;
}
