/******************************************************************************
 * Complete the function/method so that it returns the url with anything after
 * the anchor (#) removed.
 *
 * Examples
 * "www.codewars.com#about" --> "www.codewars.com"
 * "www.codewars.com?page=1" -->"www.codewars.com?page=1"
 *
 * Kata: https://www.codewars.com/kata/51f2b4448cadf20ed0000386
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>             /* Required */
#include <string.h>             /* Required */

char *remove_url_anchor(const char *url_in);

int main(void)
{
    printf("%s\n", remove_url_anchor("www.codewars.com#about"));
    printf("%s\n", remove_url_anchor("www.codewars.com?page=1"));
    printf("%s\n", remove_url_anchor("http://www.tomato.gov#fragment"));
    return 0;
}

char *remove_url_anchor(const char *url_in)
{
    int i;
    char *url_out = malloc(sizeof(char) * 0);

    for (i = 0; url_in[i] != '\0'; i++) {
        if (url_in[i] == '#')
            break;
        url_out    = realloc(url_out, sizeof(char) + (i+1));
        url_out[i] = url_in[i];
    }
    url_out[i] = '\0';
    return url_out;
}
