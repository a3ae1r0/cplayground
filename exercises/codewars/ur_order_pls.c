/******************************************************************************
 * Your task is to sort a given string. Each word in the string will contain a
 * single number. This number is the position the word should have in the
 * result.
 *
 * Note: Numbers can be from 1 to 9. So 1 will be the first word (not 0).
 *
 * If the input string is empty, return an empty string. The words in the input
 * String will only contain valid consecutive numbers.
 * Examples
 *
 * "is2 Thi1s T4est 3a"  -->  "Thi1s is2 3a T4est"
 * "4of Fo1r pe6ople g3ood th5e the2"  -->  "Fo1r the2 g3ood 4of th5e pe6ople"
 * ""  -->  ""
 *
 * Kata: https://www.codewars.com/kata/55c45be3b2079eccff00010f
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>                     /* Required */

char *order_words (char *ordered, const char *words);
extern char* strdup(const char*);       /* Workaround for Ansi C compilation */

int main (void)
{
    char *ordered = malloc(sizeof(char) * strlen("is2 Thi1s T4est 3a")+1);

    printf("%s\n", order_words(ordered, "is2 Thi1s T4est 3a"));
    printf("%s\n", order_words(ordered, ""));

    return 0;
}

/**********************************************************
 * order_words: split each work of the input string (token)
 *              and save in an array. Iterate over array to
 *              order words and concatenate to final string
 *********************************************************/
char *order_words (char *ordered, const char *words)
{
    /* ordered is pre-allocated and has enough room for a trailing space
     * character ' ' but dont forget to trim it !
     * write to ordered and return it 
     **/
    char *w = strtok(strdup(words), " "), str_tokens[9][25];
    int i, j, cnt = 0;
    *ordered = '\0';

    while (w != NULL) {
        strcpy(str_tokens[cnt], w);
        w = strtok(NULL, " ");
        cnt++;
    }

    for (i = '1'; i < '1'+cnt; i++) {
        for (j = 0; j < cnt; j++)
            if (strchr(str_tokens[j], i)) {
                strcat(ordered, str_tokens[j]);
                strcat(ordered, " ");
            }
    }
    ordered[strlen(words)] = '\0';

    return ordered;
}
