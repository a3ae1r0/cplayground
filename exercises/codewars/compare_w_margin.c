/**********************************************************
 * Kata: 56453a12fcee9a6c4700009c
 **********************************************************/
#include <stdio.h>
#include <stdlib.h>                     /* required for abs function */

int close_compare(int a, int b, int margin);

int main(void)
{
    int a, b, margin, result;
    char margin_op;                    /* to allow y or n from user */

    printf("Enter value of _a_: ");
    scanf("%d", &a);
    printf("Enter value of _b_: ");
    scanf("%d", &b);

    /* discard new line char from previous input */
    getchar();

    /* input margin (optional) */
    printf("\n[optional] Enter margin (y/n)? ");
    margin_op = getchar();
    if (margin_op == 'y') {
        printf("Enter value of margin: ");
        scanf("%d", &margin);
    } else {
        /* if margin is not given, treat it as zero. */
        margin = 0;
    }

    /* save to return value in a new variable to allow treat the output below */
    result = close_compare(a, b, margin);
    if(result == 0)
        printf("\na is close to b\n");
    else if(result == -1)
        printf("\na is less than b\n");
    else if(result == 1) {
        printf("\na is greater than b\n");
    } /* brackets to avoid C89/90 warning: -Werror=misleading-indentation*/

	return 0;
}


/**********************************************************
 * close_compare: accepts 3 parameters:
 *                _ a, b, and an margin (optional)
 *                consider:
 *                _ a is "close to" b IF margin >= b - a
 *                (distance)
 *                function returns:
 *                _  0 : when a is CLOSE to b
 *                otherwise:
 *                _ -1 : when a is LESS than b
 *                _  1 : when a is GREATER than b
 **********************************************************/
int close_compare(int a, int b, int margin)
{
    if(margin >= abs(b - a))
        return 0;
    else if (a < b)
        return -1;
    else
        return 1;
}
