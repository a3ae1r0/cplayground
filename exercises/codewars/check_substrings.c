/******************************************************************************
 * Given two arrays of strings a1 and a2 return a sorted array r in
 * lexicographical order of the strings of a1 which are substrings of strings
 * of a2.
 *
 * Example 1:
 * a1 = ["arp", "live", "strong"]
 * a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
 * returns ["arp", "live", "strong"]
 *
 * Example 2:
 * a1 = ["tarp", "mice", "bull"]
 * a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
 * returns []
 *
 * Notes:
 *   Arrays are written in "general" notation. See "Your Test Cases" for
 * examples in your language.
 *   In Shell bash a1 and a2 are strings. The return is a string where words
 * are separated by commas.
 *   Beware: r must be without duplicates.
 *
 * Kata: 550554fd08b86f84fe000a58
 *****************************************************************************/
#include <stdio.h>
/* Required */
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

char** inArray(char* array1[], int sz1, char* array2[], int sz2, int* lg);
/* bool strIsDuplicated(char* array[], int sz, char* str); */
int strCompare(const void* a, const void* b);

int main(void)
{
    /* ** TEST 1 **   Expected [ "arp", "live", "strong" ] */
    /* char* arr1[3] = { "arp", "live", "strong" }; */
    /* char* arr2[5] = { "lively", "alive", "harp", "sharp", "armstrong" }; */

    /* ** TEST 2 **   Expected [ "cod", "code, "ewar", "wars" ] */
    char* arr1[7] = { "cod", "code", "wars", "ewar", "pillow", "bed", "phht" };
    char* arr2[8] = { "lively", "alive", "harp", "sharp", "armstrong", "codewars", "cod", "code" };

    /* ** TEST 3 **   Expected: [ ] */
    /* char* arr1[3] = { "tarp", "mice", "bull" }; */
    /* char* arr2[5] = { "lively", "alive", "harp", "sharp", "armstrong" }; */

    /* ** TEST 4 **   Expected: [ ] */
    /* char* arr1[5] = { "cod", "code", "wars", "ewar", "ar" }; */
    /* char* arr2[1] = { "pr" }; */

    char** arr3;
    int i, size_arr3;

    /* arr3 = inArray(arr1, 3, arr2, 5, &size_arr3);       /1* Test 1 *1/ */
    arr3 = inArray(arr1, 7, arr2, 8, &size_arr3);       /* Test 2 */
    /* arr3 = inArray(arr1, 3, arr2, 5, &size_arr3);       /1* Test 3 *1/ */
    /* arr3 = inArray(arr1, 5, arr2, 1, &size_arr3);       /1* Test 4 *1/ */

    for (i = 0; i < size_arr3; i++)
        printf("%s ", arr3[i]);
    printf("\n");

    free(arr3);

    return 0;
}

char** inArray(char* array1[], int sz1, char* array2[], int sz2, int* lg)
{
    char** array3 = malloc(sizeof(char) * 0);
    int i, j, i_array3 = 0;

    for (i = 0; i < sz1; i ++) {
        for (j = 0; j < sz2; j++) {
            if (strstr(array2[j], array1[i])) {
                /* if (strIsDuplicated(array3, i_array3, array1[j]) == false) { */
                    array3 = realloc(array3, sizeof(char *) * (i_array3 + 1));
                    array3[i_array3] = array1[i];
                    i_array3++;
                    break;      /* Stop to avoid duplications in the new array */
                /* } */
            }
        }
    }
    *lg = i_array3;
    qsort(array3, *lg, sizeof(const char*), strCompare);

    return array3;
}


/* NOTE: Not needed anymore after change the nested loop order
 *       where the sub/strings are searched.
 */
/* bool strIsDuplicated(char* array[], int sz, char* str) */
/* { */
/*     int i; */
/*     for (i = 0; i < sz; i++) */
/*         if (strcmp(array[i], str) == 0) */
/*             return true; */
/*     return false; */
/* } */

/*
 * Using: https://stackoverflow.com/a/47959703
 */
int strCompare(const void* a, const void* b)
{
    return strcmp(*(const char**)a, *(const char**)b);
}
