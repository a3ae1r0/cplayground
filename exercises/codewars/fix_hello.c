/******************************************************************************
 * The code provided has a method hello which is supposed to show only those
 * attributes which have been explicitly set. Furthermore, it is supposed to
 * say them in the same order they were set.
 * But it's not working properly.
 *
 * Notes
 * There are 3 attributes
 *     name
 *     age
 *     sex ('M' or 'F')
 *
 * When the same attribute is assigned multiple times the hello method shows
 * it only once. If this happens the order depends on the first assignment of
 * that attribute, but the value is from the last assignment.
 *
 * Examples
 *     Hello.
 *     Hello. My name is Bob. I am 27. I am male.
 *     Hello. I am 27. I am male. My name is Bob.
 *     Hello. My name is Alice. I am female.
 *     Hello. My name is Batman.
 *
 * Task
 * Fix the code so we can all go home early.
 *
 * Kata: https://www.codewars.com/kata/5b0a80ce84a30f4762000069
 *****************************************************************************/
#include <stdio.h>          /* sprintf() */
#include <stdlib.h>         /* calloc()  */
#include <string.h>         /* strlen()  */

typedef struct
{
    char *name;
    int   age;
    char  sex;
} Dinglemouse;

Dinglemouse *createDinglemouse();
void destroyDinglemouse(Dinglemouse *dm);
void setAge(Dinglemouse *dm, int age);
void setSex(Dinglemouse *dm, char sex);
void setName(Dinglemouse *dm, const char *name);
char *hello(Dinglemouse *dm);

/* TEST PURPOSES ONLY START */
int main(void)
{
    /*
     * Expected:
     * Hello. I am 27. I am male. My name is Bob.
     **/
    Dinglemouse *dm = createDinglemouse();
    setAge(dm, 27);
    setSex(dm, 'M');
    setName(dm, "Bob");
    printf("%s\n", hello(dm));
    /***************************/

    return 0;
}
/* TEST PURPOSES ONLY END */

Dinglemouse *createDinglemouse()
{
    return calloc(1, sizeof(Dinglemouse));
}

void destroyDinglemouse(Dinglemouse *dm)
{
    // Any memory leaks?
    free(dm);
}

void setAge(Dinglemouse *dm, int age)
{
    dm->age = age;
}

void setSex(Dinglemouse *dm, char sex)
{
    dm->sex = sex;
}

void setName(Dinglemouse *dm, const char *name)
{
    dm->name = strdup(name);
}

char *hello(Dinglemouse *dm)
{
    const char format[] = {"Hello. My name is %s. I am %d. I am %s."};
    char *message = malloc(sizeof(format) + strlen(dm->name) + 16);
    sprintf(message, format, dm->name, dm->age, dm->sex == 'M' ? "male" : "female");
    return message;
}
