/******************************************************************************
 * In this kata you are required to, given a string, replace every letter with
 * its position in the alphabet.
 *
 * If anything in the text isn't a letter, ignore it and don't return it.
 * "a" = 1, "b" = 2, etc.
 *
 * Example
 * alphabet_position("The sunset sets at twelve o' clock.")
 *
 * Should return:
 * "20 8 5 19 21 14 19 5 20 19 5 20 19 1 20 20 23 5 12 22 5 15 3 12 15 3 11"
 * ( as a string )
 *
 * kata: https://www.codewars.com/kata/546f922b54af40e1e90001da
 * kyu: 6
 *****************************************************************************/
#include <stdlib.h>         /* Solution */
#include <ctype.h>          /* Solution */
#include <string.h>         /* Solution */
#include <stdio.h>          /* Solution */

char *alphabet_position(const char *text);

int main(void) {
    char *str, *res, *ans;

    /*** Test number 1 ***/
    str = "The sunset sets at twelve o' clock.";
    res = alphabet_position(str);
    ans = "20 8 5 19 21 14 19 5 20 19 5 20 19 1 20 20 23 5 12 22 5 15 3 12 15 3 11";
    printf("Test 1 ... ");
    if(strcmp(res, ans) == 0)
        printf("[OK]\n");
    else
        printf("[NOK]\n");

    /*** Test number 2 ***/
    str = "The narwhal bacons at midnight.";
    res = alphabet_position(str);
    ans = "20 8 5 14 1 18 23 8 1 12 2 1 3 15 14 19 1 20 13 9 4 14 9 7 8 20";
    printf("Test 2 ... ");
    if(strcmp(res, ans) == 0)
        printf("[OK]\n");
    else
        printf("[NOK]\n");

    /*** Test number 3 ***/
    str = "0";
    res = alphabet_position(str);
    ans = "";
    printf("Test 3 ... ");
    if(strcmp(res, ans) == 0)
        printf("[OK]\n");
    else
        printf("[NOK]\n");

    /*** Test number 4 ***/
    str = "123456789";
    res = alphabet_position(str);
    ans = "";
    printf("Test 4 ... ");
    if(strcmp(res, ans) == 0)
        printf("[OK]\n");
    else
        printf("[NOK]\n");

    return 0;
}

// returned string has to be dynamically allocated and will be freed by the caller
char *alphabet_position(const char *text)
{
    char letter[4], *out = calloc(1, sizeof(char));

    while (*text != '\0') {
        if(isalpha(*text) != 0) {
            snprintf(letter, 4, "%d ", (toupper(*text) % 64));
            out = realloc(out, sizeof(char) * (strlen(out) + strlen(letter) + 1));
            strncat(out, letter, 3);
        }
        text++;
    }
    out[strlen(out)-1] = '\0';      /* Also remove trailing whitespace */

    return out;
}
