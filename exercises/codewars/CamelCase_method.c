/******************************************************************************
 * Write simple .camelCase method (camel_case function in PHP, CamelCase in C#
 * or camelCase in Java) for strings. All words must have their first letter
 * capitalized without spaces.
 *
 * For instance:
 * camel_case("hello case");        // => "HelloCase"
 * camel_case("camel case word");   // => "CamelCaseWord"
 *
 * kata: https://www.codewars.com/kata/587731fda577b3d1b0001196
 * kyu: 6
 *****************************************************************************/
#include <stdio.h>          /* For printing tests only */
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

char *camel_case(const char *s);

int main(void)
{
    /*** TESTS ***/
    char *s;

    if(strcmp(s = camel_case("test case"), "TestCase") == 0)
       printf("Test 1........................[OK]\n");
    else
       printf("Test 1........................[NOK]\n");
    free(s);

    if(strcmp(s = camel_case("camel case method"), "CamelCaseMethod") == 0)
       printf("Test 2........................[OK]\n");
    else
       printf("Test 2........................[NOK]\n");
    free(s);

    if(strcmp(s = camel_case("say hello "), "SayHello") == 0)
       printf("Test 3........................[OK]\n");
    else
       printf("Test 3........................[NOK]\n");
    free(s);

    if(strcmp(s = camel_case(" camel case word"), "CamelCaseWord") == 0)
       printf("Test 4........................[OK]\n");
    else
       printf("Test 4........................[NOK]\n");
    free(s);

    if(strcmp(s = camel_case(""), "") == 0)
       printf("Test 5........................[OK]\n");
    else
       printf("Test 5........................[NOK]\n");
    free(s);

    if(strcmp(s = camel_case("  nx  cp u qg"), "NxCpUQg") == 0)
       printf("Test 6........................[OK]\n");
    else
       printf("Test 6........................[NOK]\n");
    free(s);

    return 0;
}

/*** SOLUTION START ***/
char *camel_case(const char *s)
{
    int len = strlen(s);
    char *camelcase_out = calloc(len+1, sizeof(char));
    char *token, *str = calloc(len+1, sizeof(char));
    const char delim[2] = " ";

    strcpy(str, s);
    token = strtok(str, delim);

    while(token != NULL) {
        token[0] = toupper(token[0]);
        strncat(camelcase_out, token, strlen(token));
        token = strtok(NULL, delim);
    }
    free(str);

    return camelcase_out;
}
