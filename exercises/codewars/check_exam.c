/******************************************************************************
 * The first input array is the key to the correct answers to an exam, like
 * ["a", "a", "b", "d"]. The second one contains a student's submitted answers.
 *
 * The two arrays are not empty and are the same length. Return the score for
 * this array of answers, giving +4 for each correct answer, -1 for each
 * incorrect answer, and +0 for each blank answer, represented as an empty
 * string (in C the space character is used).
 *
 * If the score < 0, return 0.
 *
 * For example:
 * checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"]) → 6
 * checkExam(["a", "a", "c", "b"], ["a", "a", "b",  ""]) → 7
 * checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"]) → 16
 * checkExam(["b", "c", "b", "a"], ["",  "a", "a", "c"]) → 0
 *
 * kate: https://www.codewars.com/kata/5a3dd29055519e23ec000074
 * kyu: 7
 *****************************************************************************/
#include <stdio.h>
#include <stddef.h>     /* Required for solution */

int check_exam(size_t n, const char answers[n], const char student[n]);

int main(void)
{
    const char answers0[4] = {'a', 'a', 'b', 'b'};
    const char student0[4] = {'a', 'c', 'b', 'd'};
    if(check_exam(4, answers0, student0) == 6)
        printf("Test 1 ... [OK]\n");
    else
        printf("Test 1 ... [NOK]\n");

    const char answers1[4] = {'a', 'a', 'c', 'b'};
    const char student1[4] = {'a', 'a', 'b', ' '};
    if(check_exam(4, answers1, student1) == 7)
        printf("Test 2 ... [OK]\n");
    else
        printf("Test 2 ... [NOK]\n");

    const char answers2[4] = {'a', 'a', 'b', 'c'};
    const char student2[4] = {'a', 'a', 'b', 'c'};
    if(check_exam(4, answers2, student2) == 16)
        printf("Test 3 ... [OK]\n");
    else
        printf("Test 3 ... [NOK]\n");

    const char answers3[4] = {'b', 'c', 'b', 'a'};
    const char student3[4] = {' ', 'a', 'a', 'c'};
    if(check_exam(4, answers3, student3) == 0)
        printf("Test 4 ... [OK]\n");
    else
        printf("Test 4 ... [NOK]\n");

    const char answers4[18] = {'c', 'd', 'b', 'a', 'a', 'c', 'a', 'b', 'a',
                               'd', 'b', 'a', 'd', 'a', 'a', 'c', 'c', 'c'};
    const char student4[18] = {'a', 'd', 'd', 'a', ' ', ' ', 'b', 'd', 'c',
                               ' ', 'b', ' ', 'a', 'b', ' ', 'd', 'b', 'a'};
    if(check_exam(18, answers4, student4) == 2)
        printf("Test 5 ... [OK]\n");
    else
        printf("Test 5 ... [NOK]\n");

    return 0;
}

/* SOLUTION START */
int check_exam(size_t n, const char answers[n], const char student[n])
{
    size_t test;
    int score = 0;

    for(test = 0; test < n; test++) {
        if(student[test] == answers[test])
            score += 4;
        else if(student[test] == ' ')
            ;
        else
            score--;
    }

    return (score < 0 ? score = 0 : score);
}
