/******************************************************************************
 * Fix a bug in provided method which should execute a passed action n
 * (possibly hundreds) times. The method is timing out and not completing in
 * time)
 *
 * Kata: https://www.codewars.com/kata/5b2b4836b6989d207700005b
 *****************************************************************************/
#include <stdio.h>
#include <stdatomic.h>        /* atomic_inn */
#include <unistd.h>           /* sleep() */

void thread();
void execute (void (*action)(), int nTimes);

static atomic_int counter;

int main(void)
{
    execute(&thread, 20);
    return 0;
}

void thread()
{
    fputs(".", stdout);
    sleep(1);
    counter += 1;
}

void execute (void (*action)(), int nTimes)
{
    for (volatile int i = 0; i < nTimes; ++i) {
        action();
        fflush(stdout);
    }
}
