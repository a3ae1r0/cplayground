/******************************************************************************
 * In the following 6 digit number:
 * 283910
 * 91 is the greatest sequence of 2 consecutive digits.
 *
 * In the following 10 digit number:
 * 1234567890
 * 67890 is the greatest sequence of 5 consecutive digits.
 *
 * Complete the solution so that it returns the greatest sequence of five
 * consecutive digits found within the number given. The number will be passed
 * in as a string of only digits. It should return a five digit integer.
 * The number passed may be as large as 1000 digits.
 *
 * Adapted from ProjectEuler.net
 *
 * Kata: https://www.codewars.com/kata/51675d17e0c1bed195000001
 *****************************************************************************/
#include <stdio.h>
/* Required */
#include <stdlib.h>
#include <string.h>

int largest_five_digits(const char *digits);

int main(void)
{
    printf("%d\n", largest_five_digits("283910"));
    printf("%d\n", largest_five_digits("1234567890"));
    printf("%d\n", largest_five_digits("731674765"));
    printf("%d\n", largest_five_digits("1234567898765"));
    return 0;
}

int largest_five_digits(const char *digits)
{
    int i, j, len = strlen(digits), num, largest = 0;
    char str[6];

    for (i = 0; i <= len-5; i++) {
        for (j = 0; j < 5; j++)
            str[j] = digits[i+j];
        str[5] = '\0';

        num = atoi(str);
        if (num > largest)
            largest = num;
    }
    return largest;
}
