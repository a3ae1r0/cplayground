/*
 * Complete the square sum function so that it squares each number passed into
 * it and then sums the results together.
 *
 * For example, for [1, 2, 2] it should return 9 because 1^2 + 2^2 + 2^2 = 9.
 *
 * Kata: 515e271a311df0350d00000f
 **/
#include <stddef.h>         /* size_t */
#include <stdio.h>

#define ARRAY_SIZE ((int) (sizeof(values) / sizeof(values[0])))

int square_sum(const int *values, size_t count);

int main(void)
{
    /* const int values[] = {1, 2, 2}; */
    const int values[] = {0, 3, 4, 5};
    printf("sum: %d\n", square_sum(values, ARRAY_SIZE));

    return 0;
}

int square_sum(const int *values, size_t count)
{
    int sum = 0;
    size_t num;             /* To allow comparison w/ count (C89/C90) */

    for (num=0; num<count; num++)
        sum += values[num] * values[num];

    return sum;
}
