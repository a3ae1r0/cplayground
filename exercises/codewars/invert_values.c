/******************************************************************************
 * Given a set of numbers, return the additive inverse of each. Each positive
 * becomes negatives, and the negatives become positives.
 *
 * invert([1,2,3,4,5]) == [-1,-2,-3,-4,-5]
 * invert([1,-2,3,-4,5]) == [-1,2,-3,4,-5]
 * invert([]) == []
 *
 * Notes:
 * - All values are greater than INT_MIN
 * - The input should be modified, not returned.
 *****************************************************************************/
#include <stdio.h>
#include <stddef.h>             /* size_of */

void invert(int *values, size_t values_size);

int main(void)
{
    /* SAMPLE TESTS START */
    int i, values0[] = {1, 2, 3, 4, 5},
           values1[] = {1, -2, 3, -4, 5},
           values2[] = {0};

    /* Test 0 */
    invert(values0, 5);
    for (i=0; i<5; i++)
        printf("%d ", values0[i]);
    printf("\n");

    /* Test 1 */
    invert(values1, 5);
    for (i=0; i<5; i++)
        printf("%d ", values1[i]);
    printf("\n");

    /* Test 2 */
    invert(values2, 0);
    for (i=0; i<0; i++)
        printf("%d ", values2[i]);
    printf("\n");
    /* SAMPLE TESTS END */

    return 0;
}


/*** SOLUTION START ***/
void invert(int *values, size_t values_size)
{
    int i;
    for (i=0; i<(int) values_size; i++)
        values[i] *= -1;
}
