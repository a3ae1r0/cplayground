/******************************************************************************
 * Note: This kata is inspired by Convert a Number to a String!. Try that one
 * too.
 *
 * Description
 * We need a function that can transform a string into a number. What ways of
 * achieving this do you know?
 *
 * Note: Don't worry, all inputs will be strings, and every string is a
 * perfectly valid representation of an integral number.
 *
 * Examples
 * "1234" --> 1234
 * "605"  --> 605
 * "1405" --> 1405
 * "-7" --> -7
 *
 * Kata: 544675c6f971f7399a000e79
 *****************************************************************************/
#include <stdio.h>
#include <stdbool.h>

/*** MACROS ***/
#define CHAR_TO_INT 48      /* Decimal difference between char '0' and int 0 */

/*** PROTOTYPES ***/
int string_to_number(const char *src);

int main(void)
{
    char src[] = "1234";
    printf("%d\n", string_to_number(src));
    printf("%d\n", string_to_number("605"));
    printf("%d\n", string_to_number("1405"));
    printf("%d\n", string_to_number("-7"));

    return 0;
}

/************************************************************
 * string_to_number: use a for loop run across all string
 *                   array and remove the decimal value of
 *                   char '0' and the int 0
 *                   (RATIO_CONVERSION_INT).
 *                   This subtraction for every char will
 *                   convert a string to a integer.
 ************************************************************/
/*** SOLUTION START ***/
int string_to_number(const char *src)
{
    /* negative: track if digit (src) is negative. number: converted str */
    int ch, negative = 0, number = 0;

    /* Digit (src) is a negative number */
    if (src[0] == '-')
        negative = 1;

    /* Convert chars to int */
    for (ch = negative; src[ch] != '\0'; ++ch) {
        number *= 10;                       /* Increase number of digits (int) */
        number += src[ch] - CHAR_TO_INT;    /* Append digit (char) to number (int) */
    }

    if (negative == 1)
        return number - (2*number);     /* Is negative --> mirror number */
    return number;
}
