/*
 * Return the number (count) of vowels in the given string.
 *
 * We will consider a, e, i, o, u as vowels for this Kata (but not y).
 *
 * The input string will only consist of lower case letters and/or spaces.
 *
 * Kata: 54ff3102c1bad923760001f3
 */
#include <stddef.h>
#include <stdio.h>

size_t get_count(const char *s);

int main(void)
{
    printf("abracadabra --> %ld\n", get_count("abracadabra"));
    printf("vowel --> %ld\n", get_count("vowel"));
    return 0;
}

/*** SOLUTION ***/
size_t get_count(const char *s)
{
    int i, sum = 0;

    for (i=0; s[i] != '\0'; i++) {
        if (s[i] == 'a' || s[i] == 'e' || s[i] == 'i' || s[i] == 'o' ||
            s[i] == 'u')
            sum += 1;
    }

    return sum;
}
