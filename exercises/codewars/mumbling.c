/******************************************************************************
 * This time no story, no theory. The examples below show you how to write
 * function accum:
 *
 * Examples:
 * accum("abcd") -> "A-Bb-Ccc-Dddd"
 * accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
 * accum("cwAt") -> "C-Ww-Aaa-Tttt"
 *
 * The parameter of accum is a string which includes only letters from a..z
 * and A..Z.
 *
 * Kata: 5667e8f4e3f572a8f2000039
 *****************************************************************************/
#include <stdio.h>
/* Required for solution */
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

/** PROTOTYPES **/
char *accum(const char *source);

int main(void)
{
    /* SAMPLE */
    char src_str[100] = "ZpglnRxqenU";
    printf("%s\n", accum(src_str));

    return 0;
}

/**********************************************************
 * accum: generate and print mumbling from source string:
 *        accum("cwAt") -> "C-Ww-Aaa-Tttt"
 **********************************************************/
char *accum(const char *source)
{
    int letter, i, n_mumbling, len = strlen(source) + 1;
    char * str_return = malloc(sizeof(char) * (len * (len + 1) / 2 + len));
    char ch, div = '-';

    for (letter = 0, n_mumbling = 1; source[letter] != '\0';
         letter++, n_mumbling++) {
        for (i = 0; i < n_mumbling; i++) {
            ch = !i ? toupper(source[letter]) : tolower(source[letter]);
            strncat(str_return, &ch, 1);
        }

        if ((unsigned) letter < strlen(source) - 1)         /* No end hifen */
            strncat(str_return, &div, 1);
    }

    return str_return;
}
