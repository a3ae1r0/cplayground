/******************************************************************************
 * Complete the method which accepts an array of integers, and returns one of
 * the following:
 * - "yes, ascending" - if the numbers in the array are sorted in an
 *                      ascending order
 * - "yes, descending" - if the numbers in the array are sorted in a
 *                      descending order
 * - "no" - otherwise
 *
 * You can assume the array will always be valid, and there will always be one
 * correct answer.
 *
 * Kata: 580a4734d6df748060000045
 *****************************************************************************/
#include <stdio.h>

/*** PROTOTYPES ***/
char* isSortedAndHow(int* array, int arrayLength);

int main(void)
{
    int array1[] = {1, 2};
    int array2[] = {15, 7, 3, -8};
    int array3[] = {4, 2, 30};
    /* Additional random tests with consecutive repeated numbers. Should return
    "yes, ascending" */
    int array4[] = {9481, 19483, 19485, 19487, 19489, 19492, 19494, 19496,
                    19501, 19501, 19502, 19502, 19506, 19508, 19511, 19515,
                    19516, 19516, 19518, 19519, 19520, 19523, 19530, 19537,
                    19541, 19548, 19548, 19548, 19548, 19549, 19554, 19554,
                    19554, 19557, 19558, 19561, 19562, 19562, 19568, 19573,
                    19574, 19574, 19575, 19580, 19581, 19586, 19586, 19590,
                    19594, 19600, 19603, 19608, 19609, 19611, 19612, 19612,
                    19616, 19616, 19618, 19619, 19628, 19630, 19637, 19641,
                    19641, 19643, 19646, 19647, 19647, 19647, 19649, 19651,
                    19653, 19654, 19656, 19656, 19657, 19659, 19660, 19661,
                    19661, 19670, 19675, 19677, 19677, 19679, 19683, 19683,
                    19684, 19686, 19692, 19696, 19703, 19704, 19704, 19705,
                    19711, 19712, 19720, 19720, 19724, 19727, 19730, 19730,
                    19731, 19732, 19734, 19734, 19735, 19740, 19741, 19743,
                    19747, 19753, 19756, 19756, 19757, 19760, 19760, 19766,
                    19768, 19776, 19777, 19778, 19779, 19783, 19785, 19786,
                    19787, 19792, 19793, 19796, 19796, 19800, 19801, 19801,
                    19801, 19804, 19807, 19808, 19811, 19812, 19814, 19815,
                    19819, 19820, 19821, 19821, 19834, 19835, 19836, 19836,
                    19836, 19837, 19840, 19844, 19851, 19851, 19855, 19861,
                    19864, 19865, 19866, 19869, 19875, 19876, 19877, 19882,
                    19887, 19888, 19890, 19890, 19896, 19899, 19904, 19906,
                    19909, 19910, 19912, 19913, 19914, 19915, 19916, 19918,
                    19919, 19923, 19925, 19930, 19932, 19940, 19948, 19950,
                    19952, 19952, 19954, 19959, 19960, 19962, 19963, 19966,
                    19969, 19970, 19971, 19971, 19975, 19979, 19980, 19981,
                    19982, 19984, 19985, 19995};

    printf(isSortedAndHow(array1, ((int) (sizeof(array1) / sizeof(array1[0])))));
    printf("\n");
    printf(isSortedAndHow(array2, ((int) (sizeof(array2) / sizeof(array2[0])))));
    printf("\n");
    printf(isSortedAndHow(array3, ((int) (sizeof(array3) / sizeof(array3[0])))));
    printf("\n");
    printf(isSortedAndHow(array4, ((int) (sizeof(array4) / sizeof(array4[0])))));
    printf("\n");
    return 0;
}

char* isSortedAndHow(int* array, int arrayLength)
{
    int ch, asc = 0, desc = 0, consec_rep_numbers = 0;

    /* Loop through array, and check if all values are in ascending or 
     * descending order */
    for (ch = 0; ch < arrayLength - 1; ch++) {
        if (array[ch] < array[ch+1])
            asc++;
        else if (array[ch] > array[ch+1])
            desc++;
        /* Consecutive repeated numbers (equal) should be ignore. Count them
         * so they can be "discarded" next */
        else
            consec_rep_numbers++;
    }

    /* Count number of ascending and descending values plus the number of
     * consecutive numbers. Prepare final string based on that result value */
    if (asc + consec_rep_numbers == arrayLength - 1)
        return "yes, ascending";
    else if (desc + consec_rep_numbers == arrayLength - 1)
        return "yes, descending";
    else
        return "no";
}
