/******************************************************************************
 * Jamie is a programmer, and James' girlfriend. She likes diamonds, and wants
 * a diamond string from James. Since James doesn't know how to make this
 * happen, he needs your help.
 *
 * Task
 * You need to return a string that looks like a diamond shape when printed on
 * the screen, using asterisk (*) characters. Trailing spaces should be
 * removed, and every line must be terminated with a newline character (\n).
 *
 * Return null/nil/None/... if the input is an even number or negative, as it is
 * not possible to print a diamond of even or negative size. Examples
 *
 * A size 3 diamond:
 *   *
 *  ***
 *   *
 * ...which would appear as a string of " *\n***\n *\n"
 *
 * A size 5 diamond:
 *    *
 *   ***
 *  *****
 *   ***
 *    *
 * ...that is:
 * "  *\n ***\n*****\n ***\n  *\n"
 *
 * kata: https://www.codewars.com/kata/5503013e34137eeeaa001648
 * kyu: 6
 *****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char *diamond (int n);

int main(void)
{
    printf("%s\n", diamond(3));
    printf("%s\n", diamond(5));
    printf("%s\n", diamond(2));

    return 0;
}

char *diamond (int n)
{
    char *diamond_out = calloc(n*n, sizeof(char));
    int height, width, padding;

    if(diamond_out == NULL) {
        printf("Pointer assignment failed.\n");
        exit(1);
    }

    if(n > 0 && n % 2 == 1) {
        for(height = 0; height < n; height++) {
            /* Padding: distance from middle of diamont to current row */
            padding = abs((n/2) - height);

            for(width = 0; width < padding; width++)
                strncat(diamond_out, " ", 1);

            /* Add '*'s to remaining space in row after space padding */
            for(width = 0; width < n - (2 * padding); width++)
                strncat(diamond_out, "*", 1);

            strncat(diamond_out, "\n", 1);
        }
    } else {
        diamond_out = NULL;
    }

    return diamond_out;
}
