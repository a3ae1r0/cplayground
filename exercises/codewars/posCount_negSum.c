/******************************************************************************
 * Given an array of integers.
 *
 * Return an array, where the first element is the count of positives numbers
 * and the second element is sum of negative numbers. 0 is neither positive
 * nor negative.
 * If the input array is empty or null, return an empty array.
 *
 * Example
 * For input [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15],
 * you should return [10, -65].
 *
 * Kata: 576bb71bbbcf0951d5000044
 ******************************************************************************/
#include <stdio.h>
#include <stddef.h>

void count_positives_sum_negatives(int *values, size_t count,
                                   int *positivesCount, int *negativesSum);

int main(void)
{
    /* int posCountReceived = 0, negSumReceived = 0; */
    int posCountReceived, negSumReceived;
    int values[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15 };

    count_positives_sum_negatives(values, sizeof(values)/sizeof(values[0]),
                                   &posCountReceived, &negSumReceived);
    printf("Positives Count: %d\n  Negatives Sum: %d\n", posCountReceived,
                                                         negSumReceived);
    return 0;
}

/* Solutions Starts */
void count_positives_sum_negatives(int *values, size_t count,
                                   int *positivesCount, int *negativesSum) 
{
    size_t i;                               /* Same signedness of count */
    int pos_count = 0, neg_sum = 0;         /* Improve performance and error */

    for (i = 0; i < count; i++) {
        if (values[i] > 0)
            pos_count++;                /* Store positives sum in memory */
        else
            neg_sum += values[i];       /* Store negatives sum in memory */
    }

    *positivesCount = pos_count;
    *negativesSum   = neg_sum;
}
