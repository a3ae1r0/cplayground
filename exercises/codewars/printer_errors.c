/******************************************************************************
 * Write a function printer_error, which:
 * - receives : as argument (string)
 *   - length greater or equal to one
 *   - contains only letters from ato z.
 * - returns  : rate of the printer (string)
 *   - Good control string color codes: from a to m
 *   - Bad control string color codes: from n to z
 *   - rate =  (number of errors) / (length of the control string)
 *   - don't reduce this fraction to a simpler expression.
 *
 * Examples:
 * s="aaabbbbhaijjjm"
 * printer_error(s) => "0/14"
 *
 * s="aaaxbbbbyyhwawiwjjjwwm"
 * printer_error(s) => "8/22"
 *
 * Kata: 56541980fa08ab47a0000040
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

/*** PROTOTYPES ***/
char* printer_error(char *s);

int main(void)
{
    /* SAMPLE TESTS */
    char str0[] = "aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz";
    char str1[] = "kkkwwwaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz";
    char str2[] =
        "kkkwwwaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyzuuuuu";
    printf("1. %s\n", printer_error(str0));
    printf("2. %s\n", printer_error(str1));
    printf("3. %s\n", printer_error(str2));

    return 0;
}

/*** SOLUTION ***/
char* printer_error(char *s)
{
    int i, error = 0, len = 0, n_char_value = 110;
    char * str_return = malloc(sizeof(char) * 40);

    for (i = 0; s[i] != '\0'; ++i) {
        /* Get errors: Compare int values of chars. If is bigger than the int
         *             value of n (110), then is a error */
        if (s[i] >= n_char_value)
            error++;
        len++;
    }

    /* Format error and len (int) into a string */
    sprintf(str_return, "%d/%d", error, len);

    return str_return;
}
