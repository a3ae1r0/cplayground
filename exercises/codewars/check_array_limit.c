 /*****************************************************************************
 * You will be given an array and a limit value. You must check that all values
 * in the array are below or equal to the limit value. 
 *
 * If they are, return true. Else, return false.
 *
 * You can assume all values in the array are numbers.
 *
 * Kata: 57cc981a58da9e302a000214
 *****************************************************************************/
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

/*** MACROS ***/
#define ARRAY_SIZE ((int) (sizeof(arr0) / sizeof(arr0[0])))

/*** PROTOTYPES ***/
bool small_enough(int *arr, size_t length, int limit);

int main(void)
{
    /*** SAMPLE TESTS ***/
    int arr0[] = {66, 101}, limit0 = 200;
    int arr1[] = {78, 117, 110, 99, 104, 117, 107, 115}, limit1 = 100;

    printf("%d\n", small_enough(arr0, ARRAY_SIZE, limit0));
    printf("%d\n", small_enough(arr1, ARRAY_SIZE, limit1));
    /*** SAMPLE TESTS ***/

    return 0;
}

/*** SOLUTION START ***/
bool small_enough(int *arr, size_t length, int limit)
{
    int i;
    for (i = 0; i < length; i++) {
        if (arr[i] > limit)
            return false;
    }

    return true;
}
/*** SOLUTION END ***/
