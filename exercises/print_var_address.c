/*
 * Computer Systems I | CSC 2400 - Fall 2013
 *
 * Find out (add code to print out) the address of the variable x in foo1, and
 * the variable y in foo2. What do you notice? Can you explain this?
 *
 * Src: http://www.csc.villanova.edu/~mdamian/Past/csc2400fa13/assign/plab.pdf
 */
#include <stdio.h>

void foo1(int xval)
{
    int x;
    x = xval;

    /* print the address and value of x here */
    printf("Address of x -- %p\n", &x);
}

void foo2(int dummy)
{
    int y;

    /* print the address and value of y here */
    printf("Address of y -- %p\n", &y);
}

int main()
{
    foo1(7);
    foo2(11);
    return 0;
}


/* Answer:
 * The x and y variable have the same address. This happens because, since both
 * variables are in different functions, meaning they have different scopes,
 * the compiler reuse the same address.
 */
